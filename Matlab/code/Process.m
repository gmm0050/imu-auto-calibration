
%User Set Params

dsf = 10;
k = 5;
v = 5;
validationmethod = 1;
valoffset = 5;
gradmethod = 'Gradient';

%Load in Data from .mat File

%Rough Train

    %Inputs
    
    Time_rough_train = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run5/Run6_v20_a1_dv0_150_rough.mat', 'Time');
    
    delta_rough_train = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run5/Run6_v20_a1_dv0_150_rough.mat', 'Steer_SW');
    vtheta_rough_train = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run5/Run6_v20_a1_dv0_150_rough.mat', 'AVx_S2');
    vphi_rough_train = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run5/Run6_v20_a1_dv0_150_rough.mat', 'AVy_S2');
    vpsi_rough_train = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run5/Run6_v20_a1_dv0_150_rough.mat', 'AVz_S2');
    vx_rough_train = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run5/Run6_v20_a1_dv0_150_rough.mat', 'Vx_S2');
    vy_rough_train = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run5/Run6_v20_a1_dv0_150_rough.mat', 'Vy_S2');
    vz_rough_train = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run5/Run6_v20_a1_dv0_150_rough.mat', 'Vz_S2');
    
    Inputs_rough_train = [delta_rough_train.Steer_SW, vtheta_rough_train.AVx_S2, vphi_rough_train.AVy_S2, vpsi_rough_train.AVz_S2, vx_rough_train.Vx_S2, vy_rough_train.Vy_S2, vz_rough_train.Vz_S2];
    
    %Targets
    
    ax_rough_train = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run5/Run6_v20_a1_dv0_150.mat', 'Ax_S2');
    ay_rough_train = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run5/Run6_v20_a1_dv0_150.mat', 'Ay_S2');
    az_rough_train = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run5/Run6_v20_a1_dv0_150.mat', 'Az_S2');
    
    Targets_rough_train = [ax_rough_train.Ax_S2, ay_rough_train.Ay_S2, az_rough_train.Az_S2];
    
%Rough Test
    
    %Inputs 
    
    Time_rough_test = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run5/Run6_v20_a1_dv0_150.mat', 'Time');
    
    delta_rough_test = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run5/Run6_v20_a1_dv0_150.mat', 'Steer_SW');
    vtheta_rough_test = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run5/Run6_v20_a1_dv0_150.mat', 'AVx_S2');
    vphi_rough_test = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run5/Run6_v20_a1_dv0_150.mat', 'AVy_S2');
    vpsi_rough_test = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run5/Run6_v20_a1_dv0_150.mat', 'AVz_S2');
    vx_rough_test = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run5/Run6_v20_a1_dv0_150.mat', 'Vx_S2');
    vy_rough_test = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run5/Run6_v20_a1_dv0_150.mat', 'Vy_S2');
    vz_rough_test = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run5/Run6_v20_a1_dv0_150.mat', 'Vz_S2');
    
    Inputs_rough_test = [delta_rough_test.Steer_SW, vtheta_rough_test.AVx_S2, vphi_rough_test.AVy_S2, vpsi_rough_test.AVz_S2, vx_rough_test.Vx_S2, vy_rough_test.Vy_S2, vz_rough_test.Vz_S2];
    
    %Targets
    
    ax_rough_test = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run5/Run6_v20_a1_dv0_150.mat', 'Ax_S2');
    ay_rough_test = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run5/Run6_v20_a1_dv0_150.mat', 'Ay_S2');
    az_rough_test = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run5/Run6_v20_a1_dv0_150.mat', 'Az_S2');
    
    Targets_rough_test = [ax_rough_test.Ax_S2, ay_rough_test.Ay_S2, az_rough_test.Az_S2];
    
%Step Steer Train
    
    %Inputs
    
    Time_step_train1 = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run5/Run5_v20_a1_d65.mat', 'Time');
    Time_step_train2 = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run5/Run5_v20_a1_d68.mat', 'Time');
    Time_step_train3 = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run5/Run5_v20_a1_d70.mat', 'Time');
    Time_step_train4 = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run5/Run5_v20_a1_d71.mat', 'Time');
    tend1 = Time_step_train1.Time(end);
    tend2 = Time_step_train1.Time(end);
    tend3 = Time_step_train3.Time(end);
    Time_step_train = [Time_step_train1.Time; (Time_step_train2.Time + tend1); (Time_step_train3.Time + 2*tend2); (Time_step_train4.Time + 3*tend3)];
    
    delta_step_train1 = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run5/Run5_v20_a1_d65.mat', 'Steer_SW');
    delta_step_train2 = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run5/Run5_v20_a1_d68.mat', 'Steer_SW');
    delta_step_train3 = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run5/Run5_v20_a1_d70.mat', 'Steer_SW');
    delta_step_train4 = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run5/Run5_v20_a1_d71.mat', 'Steer_SW');
    delta_step_train = [delta_step_train1.Steer_SW; delta_step_train2.Steer_SW; delta_step_train3.Steer_SW; delta_step_train4.Steer_SW];
    
    vtheta_step_train1 = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run5/Run5_v20_a1_d65.mat', 'AVx_S2');
    vtheta_step_train2 = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run5/Run5_v20_a1_d68.mat', 'AVx_S2');
    vtheta_step_train3 = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run5/Run5_v20_a1_d70.mat', 'AVx_S2');
    vtheta_step_train4 = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run5/Run5_v20_a1_d71.mat', 'AVx_S2');
    vtheta_step_train = [vtheta_step_train1.AVx_S2; vtheta_step_train2.AVx_S2; vtheta_step_train3.AVx_S2; vtheta_step_train4.AVx_S2];
    
    vphi_step_train1 = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run5/Run5_v20_a1_d65.mat', 'AVy_S2');
    vphi_step_train2 = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run5/Run5_v20_a1_d68.mat', 'AVy_S2');
    vphi_step_train3 = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run5/Run5_v20_a1_d70.mat', 'AVy_S2');
    vphi_step_train4 = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run5/Run5_v20_a1_d71.mat', 'AVy_S2');
    vphi_step_train = [vphi_step_train1.AVy_S2; vphi_step_train2.AVy_S2; vphi_step_train3.AVy_S2; vphi_step_train4.AVy_S2];
    
    vpsi_step_train1 = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run5/Run5_v20_a1_d65.mat', 'AVz_S2');
    vpsi_step_train2 = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run5/Run5_v20_a1_d68.mat', 'AVz_S2');
    vpsi_step_train3 = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run5/Run5_v20_a1_d70.mat', 'AVz_S2');
    vpsi_step_train4 = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run5/Run5_v20_a1_d71.mat', 'AVz_S2');
    vpsi_step_train = [vpsi_step_train1.AVz_S2; vpsi_step_train2.AVz_S2; vpsi_step_train3.AVz_S2; vpsi_step_train4.AVz_S2];
    
    vx_step_train1 = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run5/Run5_v20_a1_d65.mat', 'Vx_S2');
    vx_step_train2 = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run5/Run5_v20_a1_d68.mat', 'Vx_S2');
    vx_step_train3 = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run5/Run5_v20_a1_d70.mat', 'Vx_S2');
    vx_step_train4 = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run5/Run5_v20_a1_d71.mat', 'Vx_S2');
    vx_step_train = [vx_step_train1.Vx_S2; vx_step_train2.Vx_S2; vx_step_train3.Vx_S2; vx_step_train4.Vx_S2];
    
    vy_step_train1 = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run5/Run5_v20_a1_d65.mat', 'Vy_S2');
    vy_step_train2 = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run5/Run5_v20_a1_d68.mat', 'Vy_S2');
    vy_step_train3 = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run5/Run5_v20_a1_d70.mat', 'Vy_S2');
    vy_step_train4 = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run5/Run5_v20_a1_d71.mat', 'Vy_S2');
    vy_step_train = [vy_step_train1.Vy_S2; vy_step_train2.Vy_S2; vy_step_train3.Vy_S2; vy_step_train4.Vy_S2];
    
    vz_step_train1 = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run5/Run5_v20_a1_d65.mat', 'Vz_S2');
    vz_step_train2 = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run5/Run5_v20_a1_d68.mat', 'Vz_S2');
    vz_step_train3 = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run5/Run5_v20_a1_d70.mat', 'Vz_S2');
    vz_step_train4 = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run5/Run5_v20_a1_d71.mat', 'Vz_S2');
    vz_step_train = [vz_step_train1.Vz_S2; vz_step_train2.Vz_S2; vz_step_train3.Vz_S2; vz_step_train4.Vz_S2];
    
    Inputs_step_train = [delta_step_train, vtheta_step_train, vphi_step_train, vpsi_step_train, vx_step_train, vy_step_train, vz_step_train];
    
    %Targets
    
    ax_step_train1 = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run5/Run5_v20_a1_d65.mat', 'Ax_S2');
    ax_step_train2 = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run5/Run5_v20_a1_d68.mat', 'Ax_S2');
    ax_step_train3 = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run5/Run5_v20_a1_d70.mat', 'Ax_S2');
    ax_step_train4 = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run5/Run5_v20_a1_d71.mat', 'Ax_S2');
    ax_step_train = [ax_step_train1.Ax_S2; ax_step_train2.Ax_S2; ax_step_train3.Ax_S2; ax_step_train4.Ax_S2];
    
    ay_step_train1 = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run5/Run5_v20_a1_d65.mat', 'Ay_S2');
    ay_step_train2 = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run5/Run5_v20_a1_d68.mat', 'Ay_S2');
    ay_step_train3 = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run5/Run5_v20_a1_d70.mat', 'Ay_S2');
    ay_step_train4 = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run5/Run5_v20_a1_d71.mat', 'Ay_S2');
    ay_step_train = [ay_step_train1.Ay_S2; ay_step_train2.Ay_S2; ay_step_train3.Ay_S2; ay_step_train4.Ay_S2];
    
    az_step_train1 = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run5/Run5_v20_a1_d65.mat', 'Az_S2');
    az_step_train2 = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run5/Run5_v20_a1_d68.mat', 'Az_S2');
    az_step_train3 = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run5/Run5_v20_a1_d70.mat', 'Az_S2');
    az_step_train4 = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run5/Run5_v20_a1_d71.mat', 'Az_S2');
    az_step_train = [az_step_train1.Az_S2; az_step_train2.Az_S2; az_step_train3.Az_S2; az_step_train4.Az_S2];
    
    Targets_step_train = [ax_step_train, ay_step_train, az_step_train];
    
%Step Steer Test

    %Inputs
    
    Time_step_test = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run5/Run5_v20_a1_d69.mat', 'Time');
    
    delta_step_test = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run5/Run5_v20_a1_d69.mat', 'Steer_SW');
    vtheta_step_test = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run5/Run5_v20_a1_d69.mat', 'AVx_S2');
    vphi_step_test = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run5/Run5_v20_a1_d69.mat', 'AVy_S2');
    vpsi_step_test = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run5/Run5_v20_a1_d69.mat', 'AVz_S2');
    vx_step_test = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run5/Run5_v20_a1_d69.mat', 'Vx_S2');
    vy_step_test = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run5/Run5_v20_a1_d69.mat', 'Vy_S2');
    vz_step_test = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run5/Run5_v20_a1_d69.mat', 'Vz_S2');
    
    Inputs_step_test = [delta_step_test.Steer_SW, vtheta_step_test.AVx_S2, vphi_step_test.AVy_S2, vpsi_step_test.AVz_S2, vx_step_test.Vx_S2, vy_step_test.Vy_S2, vz_step_test.Vz_S2];
    
    %Targets
    
    ax_step_test = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run5/Run5_v20_a1_d69.mat', 'Ax_S2');
    ay_step_test = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run5/Run5_v20_a1_d69.mat', 'Ay_S2');
    az_step_test = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run5/Run5_v20_a1_d69.mat', 'Az_S2');
    
    Targets_step_test = [ax_step_test.Ax_S2, ay_step_test.Ay_S2, az_step_test.Az_S2];

%Step w/ Noise Train
    
    %Noise Calculations
    
    dnf = 1;
    vnf = 0.05;
    avnf = 0.1;
    anf = 0.01;
    
    delta_noise_train = randn(size(delta_step_train)) * dnf;
    vx_noise_train = randn(size(vx_step_train)) * vnf;
    vy_noise_train = randn(size(vy_step_train)) * vnf;
    vz_noise_train = randn(size(vz_step_train)) * vnf;
    vtheta_noise_train = randn(size(vtheta_step_train)) * avnf;
    vphi_noise_train = randn(size(vphi_step_train)) * avnf;
    vpsi_noise_train = randn(size(vpsi_step_train)) * avnf;
    
    ax_noise_train = randn(size(ax_step_train)) * anf;
    ay_noise_train = randn(size(ay_step_train)) * anf;
    az_noise_train = randn(size(az_step_train)) * anf;
    
    
    delta_noise_test = randn(size(delta_step_test.Steer_SW)) * dnf;
    vx_noise_test = randn(size(vx_step_test.Vx_S2)) * vnf;
    vy_noise_test = randn(size(vy_step_test.Vy_S2)) * vnf;
    vz_noise_test = randn(size(vz_step_test.Vz_S2)) * vnf;
    vtheta_noise_test = randn(size(vtheta_step_test.AVx_S2)) * avnf;
    vphi_noise_test = randn(size(vphi_step_test.AVy_S2)) * avnf;
    vpsi_noise_test = randn(size(vpsi_step_test.AVz_S2)) * avnf;
    
    ax_noise_test = randn(size(ax_step_test.Ax_S2)) * anf;
    ay_noise_test = randn(size(ay_step_test.Ay_S2)) * anf;
    az_noise_test = randn(size(az_step_test.Az_S2)) * anf;
    
    %Train Inputs
    
    delta_stepnoise_train = delta_step_train + delta_noise_train;
    vx_stepnoise_train = vx_step_train + vx_noise_train;
    vy_stepnoise_train = vy_step_train + vy_noise_train;
    vz_stepnoise_train = vz_step_train + vz_noise_train;
    vtheta_stepnoise_train = vtheta_step_train + vtheta_noise_train;
    vphi_stepnoise_train = vphi_step_train + vphi_noise_train;
    vpsi_stepnoise_train = vpsi_step_train + vpsi_noise_train;
    
    Inputs_stepnoise_train = [delta_stepnoise_train, vx_stepnoise_train, vy_stepnoise_train, vz_stepnoise_train, vtheta_stepnoise_train, vphi_stepnoise_train, vpsi_stepnoise_train];
    
    Time_stepnoise_train = Time_step_train;
    
    %Train Targets
    
    ax_stepnoise_train = ax_step_train + ax_noise_train;
    ay_stepnoise_train = ay_step_train + ay_noise_train;
    az_stepnoise_train = az_step_train + az_noise_train;
    
    Targets_stepnoise_train = [ax_stepnoise_train, ay_stepnoise_train, az_stepnoise_train];
    
    %Test Inputs
    
    delta_stepnoise_test = delta_step_test.Steer_SW + delta_noise_test;
    vx_stepnoise_test = vx_step_test.Vx_S2 + vx_noise_test;
    vy_stepnoise_test = vy_step_test.Vy_S2 + vy_noise_test;
    vz_stepnoise_test = vz_step_test.Vz_S2 + vz_noise_test;
    vtheta_stepnoise_test = vtheta_step_test.AVx_S2 + vtheta_noise_test;
    vphi_stepnoise_test = vphi_step_test.AVy_S2 + vphi_noise_test;
    vpsi_stepnoise_test = vpsi_step_test.AVz_S2 + vpsi_noise_test;
    
    Inputs_stepnoise_test = [delta_stepnoise_test, vx_stepnoise_test, vy_stepnoise_test, vz_stepnoise_test, vtheta_stepnoise_test, vphi_stepnoise_test, vpsi_stepnoise_test];
    
    Time_stepnoise_test = Time_step_test;
    
    %Test Targets
    
    ax_stepnoise_test = ax_step_test.Ax_S2 + ax_noise_test;
    ay_stepnoise_test = ay_step_test.Ay_S2 + ay_noise_test;
    az_stepnoise_test = az_step_test.Az_S2 + az_noise_test;
    
    Targets_stepnoise_test = [ax_stepnoise_test, ay_stepnoise_test, az_stepnoise_test];
    
%Velocities
    
    %Train 
    
    %Inputs
    
    Time_vel_train1 = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run7/R7_v20_a1_d68.mat', 'Time');
    Time_vel_train2 = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run7/R7_v30_a1_d68.mat', 'Time');
    Time_vel_train3 = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run7/R7_v50_a1_d68.mat', 'Time');
    Time_vel_train4 = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run7/R7_v60_a1_d68.mat', 'Time');
    tend1 = Time_vel_train1.Time(end);
    tend2 = Time_vel_train1.Time(end);
    tend3 = Time_vel_train3.Time(end);
    Time_vel_train = [Time_vel_train1.Time; (Time_vel_train2.Time + tend1); (Time_vel_train3.Time + 2*tend2); (Time_vel_train4.Time + 3*tend3)]; %%Add these together?
    
    delta_vel_train1 = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run7/R7_v20_a1_d68.mat', 'Steer_SW');
    delta_vel_train2 = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run7/R7_v30_a1_d68.mat', 'Steer_SW');
    delta_vel_train3 = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run7/R7_v50_a1_d68.mat', 'Steer_SW');
    delta_vel_train4 = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run7/R7_v60_a1_d68.mat', 'Steer_SW');
    delta_vel_train = [delta_vel_train1.Steer_SW; delta_vel_train2.Steer_SW; delta_vel_train3.Steer_SW; delta_vel_train4.Steer_SW];
    
    vtheta_vel_train1 = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run7/R7_v20_a1_d68.mat', 'AVx_S2');
    vtheta_vel_train2 = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run7/R7_v30_a1_d68.mat', 'AVx_S2');
    vtheta_vel_train3 = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run7/R7_v50_a1_d68.mat', 'AVx_S2');
    vtheta_vel_train4 = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run7/R7_v60_a1_d68.mat', 'AVx_S2');
    vtheta_vel_train = [vtheta_vel_train1.AVx_S2; vtheta_vel_train2.AVx_S2; vtheta_vel_train3.AVx_S2; vtheta_vel_train4.AVx_S2];
    
    vphi_vel_train1 = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run7/R7_v20_a1_d68.mat', 'AVy_S2');
    vphi_vel_train2 = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run7/R7_v30_a1_d68.mat', 'AVy_S2');
    vphi_vel_train3 = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run7/R7_v50_a1_d68.mat', 'AVy_S2');
    vphi_vel_train4 = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run7/R7_v60_a1_d68.mat', 'AVy_S2');
    vphi_vel_train = [vphi_vel_train1.AVy_S2; vphi_vel_train2.AVy_S2; vphi_vel_train3.AVy_S2; vphi_vel_train4.AVy_S2];
    
    vpsi_vel_train1 = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run7/R7_v20_a1_d68.mat', 'AVz_S2');
    vpsi_vel_train2 = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run7/R7_v30_a1_d68.mat', 'AVz_S2');
    vpsi_vel_train3 = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run7/R7_v50_a1_d68.mat', 'AVz_S2');
    vpsi_vel_train4 = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run7/R7_v60_a1_d68.mat', 'AVz_S2');
    vpsi_vel_train = [vpsi_vel_train1.AVz_S2; vpsi_vel_train2.AVz_S2; vpsi_vel_train3.AVz_S2; vpsi_vel_train4.AVz_S2];
    
    vx_vel_train1 = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run7/R7_v20_a1_d68.mat', 'Vx_S2');
    vx_vel_train2 = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run7/R7_v30_a1_d68.mat', 'Vx_S2');
    vx_vel_train3 = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run7/R7_v50_a1_d68.mat', 'Vx_S2');
    vx_vel_train4 = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run7/R7_v60_a1_d68.mat', 'Vx_S2');
    vx_vel_train = [vx_vel_train1.Vx_S2; vx_vel_train2.Vx_S2; vx_vel_train3.Vx_S2; vx_vel_train4.Vx_S2];
    
    vy_vel_train1 = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run7/R7_v20_a1_d68.mat', 'Vy_S2');
    vy_vel_train2 = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run7/R7_v30_a1_d68.mat', 'Vy_S2');
    vy_vel_train3 = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run7/R7_v50_a1_d68.mat', 'Vy_S2');
    vy_vel_train4 = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run7/R7_v60_a1_d68.mat', 'Vy_S2');
    vy_vel_train = [vy_vel_train1.Vy_S2; vy_vel_train2.Vy_S2; vy_vel_train3.Vy_S2; vy_vel_train4.Vy_S2];
    
    vz_vel_train1 = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run7/R7_v20_a1_d68.mat', 'Vz_S2');
    vz_vel_train2 = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run7/R7_v30_a1_d68.mat', 'Vz_S2');
    vz_vel_train3 = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run7/R7_v50_a1_d68.mat', 'Vz_S2');
    vz_vel_train4 = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run7/R7_v60_a1_d68.mat', 'Vz_S2');
    vz_vel_train = [vz_vel_train1.Vz_S2; vz_vel_train2.Vz_S2; vz_vel_train3.Vz_S2; vz_vel_train4.Vz_S2];
    
    Inputs_vel_train = [delta_vel_train, vtheta_vel_train, vphi_vel_train, vpsi_vel_train, vx_vel_train, vy_vel_train, vz_vel_train];
    
    %Targets
    
    ax_vel_train1 = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run7/R7_v20_a1_d68.mat', 'Ax_S2');
    ax_vel_train2 = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run7/R7_v30_a1_d68.mat', 'Ax_S2');
    ax_vel_train3 = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run7/R7_v50_a1_d68.mat', 'Ax_S2');
    ax_vel_train4 = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run7/R7_v60_a1_d68.mat', 'Ax_S2');
    ax_vel_train = [ax_vel_train1.Ax_S2; ax_vel_train2.Ax_S2; ax_vel_train3.Ax_S2; ax_vel_train4.Ax_S2];
    
    ay_vel_train1 = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run7/R7_v20_a1_d68.mat', 'Ay_S2');
    ay_vel_train2 = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run7/R7_v30_a1_d68.mat', 'Ay_S2');
    ay_vel_train3 = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run7/R7_v50_a1_d68.mat', 'Ay_S2');
    ay_vel_train4 = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run7/R7_v60_a1_d68.mat', 'Ay_S2');
    ay_vel_train = [ay_vel_train1.Ay_S2; ay_vel_train2.Ay_S2; ay_vel_train3.Ay_S2; ay_vel_train4.Ay_S2];
    
    az_vel_train1 = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run7/R7_v20_a1_d68.mat', 'Az_S2');
    az_vel_train2 = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run7/R7_v30_a1_d68.mat', 'Az_S2');
    az_vel_train3 = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run7/R7_v50_a1_d68.mat', 'Az_S2');
    az_vel_train4 = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run7/R7_v60_a1_d68.mat', 'Az_S2');
    az_vel_train = [az_vel_train1.Az_S2; az_vel_train2.Az_S2; az_vel_train3.Az_S2; az_vel_train4.Az_S2];
    
    Targets_vel_train = [ax_vel_train, ay_vel_train, az_vel_train];
    
    
    %Test
    
    %Inputs
    
    Time_vel_test1 = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run7/R7_v10_a1_d68.mat', 'Time');
    Time_vel_test2 = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run7/R7_v40_a1_d68.mat', 'Time');
    Time_vel_test3 = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run7/R7_v70_a1_d68.mat', 'Time');
    Time_vel_test_10 = Time_vel_test1.Time; 
    Time_vel_test = Time_vel_test2.Time; 
    Time_vel_test_70 = Time_vel_test3.Time; 
    
    delta_vel_test1 = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run7/R7_v10_a1_d68.mat', 'Steer_SW');
    delta_vel_test2 = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run7/R7_v40_a1_d68.mat', 'Steer_SW');
    delta_vel_test3 = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run7/R7_v70_a1_d68.mat', 'Steer_SW');
    delta_vel_test_10 = delta_vel_test1.Steer_SW;
    delta_vel_test_40 = delta_vel_test2.Steer_SW;
    delta_vel_test_70 = delta_vel_test3.Steer_SW;
    
    vtheta_vel_test1 = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run7/R7_v10_a1_d68.mat', 'AVx_S2');
    vtheta_vel_test2 = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run7/R7_v40_a1_d68.mat', 'AVx_S2');
    vtheta_vel_test3 = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run7/R7_v70_a1_d68.mat', 'AVx_S2');
    vtheta_vel_test_10 = vtheta_vel_test1.AVx_S2;
    vtheta_vel_test_40 = vtheta_vel_test2.AVx_S2;
    vtheta_vel_test_70 = vtheta_vel_test3.AVx_S2;
    
    vphi_vel_test1 = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run7/R7_v10_a1_d68.mat', 'AVy_S2');
    vphi_vel_test2 = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run7/R7_v40_a1_d68.mat', 'AVy_S2');
    vphi_vel_test3 = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run7/R7_v70_a1_d68.mat', 'AVy_S2');
    vphi_vel_test_10 = vphi_vel_test1.AVy_S2; 
    vphi_vel_test_40 = vphi_vel_test2.AVy_S2; 
    vphi_vel_test_70 = vphi_vel_test3.AVy_S2; 
    
    vpsi_vel_test1 = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run7/R7_v10_a1_d68.mat', 'AVz_S2');
    vpsi_vel_test2 = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run7/R7_v40_a1_d68.mat', 'AVz_S2');
    vpsi_vel_test3 = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run7/R7_v70_a1_d68.mat', 'AVz_S2');
    vpsi_vel_test_10 = vpsi_vel_test1.AVz_S2;
    vpsi_vel_test_40 = vpsi_vel_test2.AVz_S2;
    vpsi_vel_test_70 = vpsi_vel_test3.AVz_S2;
    
    vx_vel_test1 = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run7/R7_v10_a1_d68.mat', 'Vx_S2');
    vx_vel_test2 = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run7/R7_v40_a1_d68.mat', 'Vx_S2');
    vx_vel_test3 = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run7/R7_v70_a1_d68.mat', 'Vx_S2');
    vx_vel_test_10 = vx_vel_test1.Vx_S2; 
    vx_vel_test_40 = vx_vel_test2.Vx_S2;
    vx_vel_test_70 = vx_vel_test3.Vx_S2;
    
    vy_vel_test1 = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run7/R7_v10_a1_d68.mat', 'Vy_S2');
    vy_vel_test2 = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run7/R7_v40_a1_d68.mat', 'Vy_S2');
    vy_vel_test3 = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run7/R7_v70_a1_d68.mat', 'Vy_S2');
    vy_vel_test_10 = vy_vel_test1.Vy_S2;
    vy_vel_test_40 = vy_vel_test2.Vy_S2;
    vy_vel_test_70 = vy_vel_test3.Vy_S2;
    
    vz_vel_test1 = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run7/R7_v10_a1_d68.mat', 'Vz_S2');
    vz_vel_test2 = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run7/R7_v40_a1_d68.mat', 'Vz_S2');
    vz_vel_test3 = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run7/R7_v70_a1_d68.mat', 'Vz_S2');
    vz_vel_test_10 = vz_vel_test1.Vz_S2; 
    vz_vel_test_40 = vz_vel_test2.Vz_S2; 
    vz_vel_test_70 = vz_vel_test3.Vz_S2;
    
    Inputs_vel_test_10 = [delta_vel_test_10, vtheta_vel_test_10, vphi_vel_test_10, vpsi_vel_test_10, vx_vel_test_10, vy_vel_test_10, vz_vel_test_10];
    Inputs_vel_test = [delta_vel_test_40, vtheta_vel_test_40, vphi_vel_test_40, vpsi_vel_test_40, vx_vel_test_40, vy_vel_test_40, vz_vel_test_40];
    Inputs_vel_test_70 = [delta_vel_test_70, vtheta_vel_test_70, vphi_vel_test_70, vpsi_vel_test_70, vx_vel_test_70, vy_vel_test_70, vz_vel_test_70];
    
    %Targets
    
    ax_vel_test1 = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run7/R7_v10_a1_d68.mat', 'Ax_S2');
    ax_vel_test2 = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run7/R7_v40_a1_d68.mat', 'Ax_S2');
    ax_vel_test3 = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run7/R7_v70_a1_d68.mat', 'Ax_S2');
    ax_vel_test_10 = ax_vel_test1.Ax_S2;
    ax_vel_test_40 = ax_vel_test2.Ax_S2;
    ax_vel_test_70 = ax_vel_test3.Ax_S2;
    
    ay_vel_test1 = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run7/R7_v10_a1_d68.mat', 'Ay_S2');
    ay_vel_test2 = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run7/R7_v40_a1_d68.mat', 'Ay_S2');
    ay_vel_test3 = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run7/R7_v70_a1_d68.mat', 'Ay_S2');
    ay_vel_test_10 = ay_vel_test1.Ay_S2;
    ay_vel_test_40 = ay_vel_test2.Ay_S2;
    ay_vel_test_70 = ay_vel_test3.Ay_S2; 
    
    az_vel_test1 = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run7/R7_v10_a1_d68.mat', 'Az_S2');
    az_vel_test2 = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run7/R7_v40_a1_d68.mat', 'Az_S2');
    az_vel_test3 = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run7/R7_v70_a1_d68.mat', 'Az_S2');
    az_vel_test_10 = az_vel_test1.Az_S2; 
    az_vel_test_40 = az_vel_test2.Az_S2; 
    az_vel_test_70 = az_vel_test3.Az_S2; 
    
    Targets_vel_test_10 = [ax_vel_test_10, ay_vel_test_10, az_vel_test_10];
    Targets_vel_test = [ax_vel_test_40, ay_vel_test_40, az_vel_test_40];
    Targets_vel_test_70 = [ax_vel_test_70, ay_vel_test_70, az_vel_test_70];
    
%Step + Velocity Difference
    
    
%Experimental
    
    in_train_0 = 1;
    in_train_f = 401;
    t_train_0 = 1;
    t_train_f = 401;
    in_test_0 = 401;
    in_test_f = 601;
    t_test_0 = 401;
    t_test_f = 601;

    %Loading From Workspace
    
    time_exp = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run7/EXP/t2/figure8.mat', 'time');
    
    delta_exp = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run7/EXP/t2/figure8.mat', 'delta');
    vx_exp = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run7/EXP/t2/figure8.mat', 'vx');
    vy_exp = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run7/EXP/t2/figure8.mat', 'vy');
    vz_exp = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run7/EXP/t2/figure8.mat', 'vz');
    vtheta_exp = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run7/EXP/t2/figure8.mat', 'vtheta');
    vphi_exp = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run7/EXP/t2/figure8.mat', 'vphi');
    vpsi_exp = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run7/EXP/t2/figure8.mat', 'vpsi');
    
    ax_exp = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run7/EXP/t2/figure8.mat', 'ax');
    ay_exp = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run7/EXP/t2/figure8.mat', 'ay');
    az_exp = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run7/EXP/t2/figure8.mat', 'az');
    
    %Train
    
    delcf = 180/pi;
    avcf = 180/pi;
    velcf = 3.6;
    acf = 1/9.81;
    %Inputs
    
    delta_exp_train = delcf*delta_exp.delta(in_train_0:in_train_f);
    vx_exp_train = velcf*vx_exp.vx(in_train_0:in_train_f);
    vy_exp_train = velcf*vy_exp.vy(in_train_0:in_train_f);
    vz_exp_train = velcf*vz_exp.vz(in_train_0:in_train_f);
    vtheta_exp_train = avcf*vtheta_exp.vtheta(in_train_0:in_train_f);
    vphi_exp_train = avcf*vphi_exp.vphi(in_train_0:in_train_f);
    vpsi_exp_train = avcf*vpsi_exp.vpsi(in_train_0:in_train_f);
    
    Inputs_exp_train = [delta_exp_train', vx_exp_train', vy_exp_train', vz_exp_train', vtheta_exp_train', vphi_exp_train', vpsi_exp_train'];
    
    
    
    %Targets
    
    ax_exp_train = acf*ax_exp.ax(t_train_0:t_train_f);
    ay_exp_train = acf*ay_exp.ay(t_train_0:t_train_f);
    az_exp_train = acf*az_exp.az(t_train_0:t_train_f);
    
    Targets_exp_train = [ax_exp_train', ay_exp_train', az_exp_train'];
    
    Time_exp_train = time_exp.time(in_train_0:in_train_f)';
    
    %Test
    
    %Inputs
    
    delta_exp_test = delcf*delta_exp.delta(in_test_0:in_test_f);
    vx_exp_test = velcf*vx_exp.vx(in_test_0:in_test_f);
    vy_exp_test = velcf*vy_exp.vy(in_test_0:in_test_f);
    vz_exp_test = velcf*vz_exp.vz(in_test_0:in_test_f);
    vtheta_exp_test = avcf*vtheta_exp.vtheta(in_test_0:in_test_f);
    vphi_exp_test = avcf*vphi_exp.vphi(in_test_0:in_test_f);
    vpsi_exp_test = avcf*vpsi_exp.vpsi(in_test_0:in_test_f);
    
    Inputs_exp_test = [delta_exp_test', vx_exp_test', vy_exp_test', vz_exp_test', vtheta_exp_test', vphi_exp_test', vpsi_exp_test'];
    
    
    %Targets
    
    ax_exp_test = acf*ax_exp.ax(t_test_0:t_test_f);
    ay_exp_test = acf*ay_exp.ay(t_test_0:t_test_f);
    az_exp_test = acf*az_exp.az(t_test_0:t_test_f);
    
    Targets_exp_test = [ax_exp_test', ay_exp_test', az_exp_test'];
    
    Time_exp_test = time_exp.time(in_test_0:in_test_f)';
    
%Simulated Training + Experimental Testing



disp('Got to Downsampling!')
    
%Downsample Data to a Reasonable Level

%Rough

Inputs_rough_train = downsample(Inputs_rough_train, dsf);

Targets_rough_train = downsample(Targets_rough_train, dsf);

Time_rough_train = downsample(Time_rough_train.Time, dsf);

Inputs_rough_t = downsample(Inputs_rough_test, dsf);

Targets_rough_t = downsample(Targets_rough_test, dsf);

Time_rough_t = downsample(Time_rough_test.Time, dsf);

Inputs_rough_val = downsample(Inputs_rough_test, dsf, valoffset);

Targets_rough_val = downsample(Targets_rough_test, dsf, valoffset);

Time_rough_val = downsample(Time_rough_test.Time, dsf, valoffset);

Inputs_rough_test = Inputs_rough_t;

Targets_rough_test = Targets_rough_t;

Time_rough_test = Time_rough_t;

%Step

Inputs_step_train = downsample(Inputs_step_train, dsf);

Targets_step_train = downsample(Targets_step_train, dsf);

Time_step_train = downsample(Time_step_train, dsf);

Inputs_step_t = downsample(Inputs_step_test, dsf);

Targets_step_t = downsample(Targets_step_test, dsf);

Time_step_t = downsample(Time_step_test.Time, dsf);

Inputs_step_val = downsample(Inputs_step_test, dsf, valoffset);

Targets_step_val = downsample(Targets_step_test, dsf, valoffset);

Time_step_val = downsample(Time_step_test.Time, dsf, valoffset);

Inputs_step_test = Inputs_step_t;

Targets_step_test = Targets_step_t;

Time_step_test = Time_step_t;

    %Step w/ Noise
    
    Inputs_stepnoise_train = downsample(Inputs_stepnoise_train, dsf);

    Targets_stepnoise_train = downsample(Targets_stepnoise_train, dsf);

    Time_stepnoise_train = downsample(Time_stepnoise_train, dsf);

    Inputs_stepnoise_t = downsample(Inputs_stepnoise_test, dsf);

    Targets_stepnoise_t = downsample(Targets_stepnoise_test, dsf);

    Time_stepnoise_t = downsample(Time_stepnoise_test.Time, dsf);

    Inputs_stepnoise_val = downsample(Inputs_stepnoise_test, dsf, valoffset);

    Targets_stepnoise_val = downsample(Targets_stepnoise_test, dsf, valoffset);

    Time_stepnoise_val = downsample(Time_stepnoise_test.Time, dsf, valoffset);

    Inputs_stepnoise_test = Inputs_stepnoise_t;

    Targets_stepnoise_test = Targets_stepnoise_t;

    Time_stepnoise_test = Time_stepnoise_t;
    
    %Velocities
    
    Inputs_vel_train = downsample(Inputs_vel_train, dsf);

    Targets_vel_train = downsample(Targets_vel_train, dsf);

    Time_vel_train = downsample(Time_vel_train, dsf);

    Inputs_vel_t = downsample(Inputs_vel_test, dsf);

    Targets_vel_t = downsample(Targets_vel_test, dsf);

    Time_vel_t = downsample(Time_vel_test, dsf);

    Inputs_vel_val = downsample(Inputs_vel_test, dsf, valoffset);

    Targets_vel_val = downsample(Targets_vel_test, dsf, valoffset);

    Time_vel_val = downsample(Time_vel_test, dsf, valoffset);

    Inputs_vel_test = Inputs_vel_t;

    Targets_vel_test = Targets_vel_t;

    Time_vel_test = Time_vel_t;
    
    %Step + Velocity Difference
    
    
    %Experimental
    
%     Inputs_exp_train = downsample(Inputs_exp_train, dsf);
% 
%     Targets_exp_train = downsample(Targets_exp_train, dsf);
% 
%     Time_exp_train = downsample(Time_exp_train, dsf);
% 
%     Inputs_exp_t = downsample(Inputs_exp_test, dsf);
% 
%     Targets_exp_t = downsample(Targets_exp_test, dsf);
% 
%     Time_exp_t = downsample(Time_exp_test, dsf);
% 
     Inputs_exp_val = Inputs_exp_test;
% 
     Targets_exp_val = Targets_exp_test;
% 
     Time_exp_val = Time_exp_test;
% 
%     Inputs_exp_test = Inputs_exp_t;
% 
%     Targets_exp_test = Targets_exp_t;
% 
%     Time_exp_test = Time_exp_t;
    
    %Simulated Training + Experimental Testing

disp('Got to Transpose!')
    
%Transpose Data

Inputs_rough_train = Inputs_rough_train';

Targets_rough_train = Targets_rough_train';

Time_rough_train = Time_rough_train';

Inputs_rough_test = Inputs_rough_test';

Targets_rough_test = Targets_rough_test';

Time_rough_test = Time_rough_test';

Inputs_rough_val = Inputs_rough_val';

Targets_rough_val = Targets_rough_val';

Time_rough_val = Time_rough_val';



Inputs_step_train = Inputs_step_train';

Targets_step_train = Targets_step_train';

Time_step_train = Time_step_train';

Inputs_step_test = Inputs_step_test';

Targets_step_test = Targets_step_test';

Time_step_test = Time_step_test';

Inputs_step_val = Inputs_step_val';

Targets_step_val = Targets_step_val';

Time_step_val = Time_step_val';

    %Step w/ Noise
    
    Inputs_stepnoise_train = Inputs_stepnoise_train';

    Targets_stepnoise_train = Targets_stepnoise_train';

    Time_stepnoise_train = Time_stepnoise_train';

    Inputs_stepnoise_test = Inputs_stepnoise_test';

    Targets_stepnoise_test = Targets_stepnoise_test';

    Time_stepnoise_test = Time_stepnoise_test';

    Inputs_stepnoise_val = Inputs_stepnoise_val';

    Targets_stepnoise_val = Targets_stepnoise_val';

    Time_stepnoise_val = Time_stepnoise_val';

    
    %Velocities
    
    Inputs_vel_train = Inputs_vel_train';

    Targets_vel_train = Targets_vel_train';

    Time_vel_train = Time_vel_train';

    Inputs_vel_test = Inputs_vel_test';

    Targets_vel_test = Targets_vel_test';

    Time_vel_test = Time_vel_test';

    Inputs_vel_val = Inputs_vel_val';

    Targets_vel_val = Targets_vel_val';

    Time_vel_val = Time_vel_val';
    
    %Step + Velocity Difference
    
    
    %Experimental
    
    Inputs_exp_train = Inputs_exp_train';

    Targets_exp_train = Targets_exp_train';

    Time_exp_train = Time_exp_train';

    Inputs_exp_test = Inputs_exp_test';

    Targets_exp_test = Targets_exp_test';

    Time_exp_test = Time_exp_test';

    Inputs_exp_val = Inputs_exp_val';

    Targets_exp_val = Targets_exp_val';

    Time_exp_val = Time_exp_val';
    
    %Simulated Training + Experimental Testing

disp('Got to Normalization')   
    
%Normalize Data

    %Rough 

    [U_rough_train, T_rough_train] = Normalize_Data(Inputs_rough_train, Targets_rough_train, Time_rough_train, gradmethod);
    
    [U_rough_test, T_rough_test] = Normalize_Data(Inputs_rough_test, Targets_rough_test, Time_rough_test, gradmethod);
    
    [U_rough_val, T_rough_val] = Normalize_Data(Inputs_rough_val, Targets_rough_val, Time_rough_val, gradmethod);
    
    %Step Steer
    
    [U_step_train, T_step_train] = Normalize_Data(Inputs_step_train, Targets_step_train, Time_step_train, gradmethod);
    
    [U_step_test, T_step_test] = Normalize_Data(Inputs_step_test, Targets_step_test, Time_step_test, gradmethod);
    
    [U_step_val, T_step_val] = Normalize_Data(Inputs_step_val, Targets_step_val, Time_step_val, gradmethod);
    
    %Step w/ Noise
    
    [U_stepnoise_train, T_stepnoise_train] = Normalize_Data(Inputs_stepnoise_train, Targets_stepnoise_train, Time_stepnoise_train, gradmethod);
    
    [U_stepnoise_test, T_stepnoise_test] = Normalize_Data(Inputs_stepnoise_test, Targets_stepnoise_test, Time_stepnoise_test, gradmethod);
    
    [U_stepnoise_val, T_stepnoise_val] = Normalize_Data(Inputs_stepnoise_val, Targets_stepnoise_val, Time_stepnoise_val, gradmethod);
    
    %Velocities
    
    [U_vel_train, T_vel_train] = Normalize_Data(Inputs_vel_train, Targets_vel_train, Time_vel_train, gradmethod);
    
    [U_vel_test, T_vel_test] = Normalize_Data(Inputs_vel_test, Targets_vel_test, Time_vel_test, gradmethod);
    
    [U_vel_val, T_vel_val] = Normalize_Data(Inputs_vel_val, Targets_vel_val, Time_vel_val, gradmethod);
    
    
    %Step + Velocity Difference
    
    
    %Experimental
    
    [U_exp_train, T_exp_train] = Normalize_Data(Inputs_exp_train, Targets_exp_train, Time_exp_train, gradmethod);
    
    [U_exp_test, T_exp_test] = Normalize_Data(Inputs_exp_test, Targets_exp_test, Time_exp_test, gradmethod);
    
    [U_exp_val, T_exp_val] = Normalize_Data(Inputs_exp_val, Targets_exp_val, Time_exp_val, gradmethod);
    
    
    
    %Simulated Training + Experimental Testing

    
disp('Got to Training!')   
    
%Train

tic
disp('X-Training Rough')
[Cx_rough, Wx_rough, Bx_rough, Ux_rough, basesx_rough] = SARNN(validationmethod, k, v, U_rough_val{1}, T_rough_val{1}, U_rough_train{1}, T_rough_train{1}); %Trains the network to build xoutmat from xsmat
disp('Y-Training Rough')
[Cy_rough, Wy_rough, By_rough, Uy_rough, basesy_rough] = SARNN(validationmethod, k, v, U_rough_val{2}, T_rough_val{2}, U_rough_train{2}, T_rough_train{2});
disp('Z-Training Rough')
[Cz_rough, Wz_rough, Bz_rough, Uz_rough, basesz_rough] = SARNN(validationmethod, k, v, U_rough_val{3}, T_rough_val{3}, U_rough_train{3}, T_rough_train{3});
 %   Bases = [Bases, bases];

disp('X-Training Step')
[Cx_step, Wx_step, Bx_step, Ux_step, basesx_step] = SARNN(validationmethod, k, v, U_step_val{1}, T_step_val{1}, U_step_train{1}, T_step_train{1});
disp('Y-Training Step')
[Cy_step, Wy_step, By_step, Uy_step, basesy_step] = SARNN(validationmethod, k, v, U_step_val{2}, T_step_val{2}, U_step_train{2}, T_step_train{2});
disp('Z-Training Step')
[Cz_step, Wz_step, Bz_step, Uz_step, basesz_step] = SARNN(validationmethod, k, v, U_step_val{3}, T_step_val{3}, U_step_train{3}, T_step_train{3});


    %Step w/ Noise
    
disp('X-Training Step')
[Cx_stepnoise, Wx_stepnoise, Bx_stepnoise, Ux_stepnoise, basesx_stepnoise] = SARNN(validationmethod, k, v, U_stepnoise_val{1}, T_stepnoise_val{1}, U_stepnoise_train{1}, T_stepnoise_train{1});
disp('Y-Training Step')
[Cy_stepnoise, Wy_stepnoise, By_stepnoise, Uy_stepnoise, basesy_stepnoise] = SARNN(validationmethod, k, v, U_stepnoise_val{2}, T_stepnoise_val{2}, U_stepnoise_train{2}, T_stepnoise_train{2});
disp('Z-Training Step')
[Cz_stepnoise, Wz_stepnoise, Bz_stepnoise, Uz_stepnoise, basesz_stepnoise] = SARNN(validationmethod, k, v, U_stepnoise_val{3}, T_stepnoise_val{3}, U_stepnoise_train{3}, T_stepnoise_train{3});

    
    %Velocities
    
disp('X-Training Step')
[Cx_vel, Wx_vel, Bx_vel, Ux_vel, basesx_vel] = SARNN(validationmethod, k, v, U_vel_val{1}, T_vel_val{1}, U_vel_train{1}, T_vel_train{1});
disp('Y-Training Step')
[Cy_vel, Wy_vel, By_vel, Uy_vel, basesy_vel] = SARNN(validationmethod, k, v, U_vel_val{2}, T_vel_val{2}, U_vel_train{2}, T_vel_train{2});
disp('Z-Training Step')
[Cz_vel, Wz_vel, Bz_vel, Uz_vel, basesz_vel] = SARNN(validationmethod, k, v, U_vel_val{3}, T_vel_val{3}, U_vel_train{3}, T_vel_train{3});

    
    %Step + Velocity Difference
    
    
    %Experimental
    
[Cx_exp, Wx_exp, Bx_exp, Ux_exp, basesx_exp] = SARNN(validationmethod, k, v, U_exp_val{1}, T_exp_val{1}, U_exp_train{1}, T_exp_train{1});
disp('Y-Training Step')
[Cy_exp, Wy_exp, By_exp, Uy_exp, basesy_exp] = SARNN(validationmethod, k, v, U_exp_val{2}, T_exp_val{2}, U_exp_train{2}, T_exp_train{2});
disp('Z-Training Step')
[Cz_exp, Wz_exp, Bz_exp, Uz_exp, basesz_exp] = SARNN(validationmethod, k, v, U_exp_val{3}, T_exp_val{3}, U_exp_train{3}, T_exp_train{3});

    
    %Simulated Training + Experimental Testing
    
toc


disp('Got to Testing!')

%Test

outclass = 1;
ndim = 5;
nout = 1;

[resultx_rough, residualx_rough] = test(Wx_rough,Cx_rough,Bx_rough,Ux_rough,U_rough_test{1}, T_rough_test{1}, outclass, nout, ndim);
[resulty_rough, residualy_rough] = test(Wy_rough,Cy_rough,By_rough,Uy_rough,U_rough_test{2}, T_rough_test{2}, outclass, nout, ndim);
[resultz_rough, residualz_rough] = test(Wz_rough,Cz_rough,Bz_rough,Uz_rough,U_rough_test{3}, T_rough_test{3}, outclass, nout, ndim);
     %R = [R; result];
     %Res = [Res; residual'];
     %Original = [Original; xtestoutmat'];
     
[resultx_step, residualx_step] = test(Wx_step,Cx_step,Bx_step,Ux_step,U_step_test{1}, T_step_test{1}, outclass, nout, ndim);
[resulty_step, residualy_step] = test(Wy_step,Cy_step,By_step,Uy_step,U_step_test{2}, T_step_test{2}, outclass, nout, ndim);
[resultz_step, residualz_step] = test(Wz_step,Cz_step,Bz_step,Uz_step,U_step_test{3}, T_step_test{3}, outclass, nout, ndim);
    
% R = [R; result];
    % Res = [Res; residual'];
    % Original = [Original; xtestoutmat'];

    %Step w/ Noise
    
[resultx_stepnoise, residualx_stepnoise] = test(Wx_stepnoise,Cx_stepnoise,Bx_stepnoise,Ux_stepnoise,U_stepnoise_test{1}, T_stepnoise_test{1}, outclass, nout, ndim);
[resulty_stepnoise, residualy_stepnoise] = test(Wy_stepnoise,Cy_stepnoise,By_stepnoise,Uy_stepnoise,U_stepnoise_test{2}, T_stepnoise_test{2}, outclass, nout, ndim);
[resultz_stepnoise, residualz_stepnoise] = test(Wz_stepnoise,Cz_stepnoise,Bz_stepnoise,Uz_stepnoise,U_stepnoise_test{3}, T_stepnoise_test{3}, outclass, nout, ndim);
   
    
    %Velocities
    
[resultx_vel, residualx_vel] = test(Wx_vel,Cx_vel,Bx_vel,Ux_vel,U_vel_test{1}, T_vel_test{1}, outclass, nout, ndim);
[resulty_vel, residualy_vel] = test(Wy_vel,Cy_vel,By_vel,Uy_vel,U_vel_test{2}, T_vel_test{2}, outclass, nout, ndim);
[resultz_vel, residualz_vel] = test(Wz_vel,Cz_vel,Bz_vel,Uz_vel,U_vel_test{3}, T_vel_test{3}, outclass, nout, ndim);
  
    
    %Step + Velocity Difference
    
    
    %Experimental
    
[resultx_exp, residualx_exp] = test(Wx_exp,Cx_exp,Bx_exp,Ux_exp,U_exp_test{1}, T_exp_test{1}, outclass, nout, ndim);
[resulty_exp, residualy_exp] = test(Wy_exp,Cy_exp,By_exp,Uy_exp,U_exp_test{2}, T_exp_test{2}, outclass, nout, ndim);
[resultz_exp, residualz_exp] = test(Wz_exp,Cz_exp,Bz_exp,Uz_exp,U_exp_test{3}, T_exp_test{3}, outclass, nout, ndim);
  
    
    %Simulated Training + Experimental Testing
 
    
 disp('Got to Likelihood Search!')
    
% Likelihood Search
    
    Points = load('C:\Users\gmm0050\Documents\MATLAB\Vehicle Simulations\Runs\Run5\Clustering\Clusteringresults.mat', 'Points');
    Points_carsim = Points.Points;
    %Rough
    
    alphax_rough = gradient(Inputs_rough_test(5, :)) ./ gradient(Time_rough_test);
    alphay_rough = gradient(Inputs_rough_test(6, :)) ./ gradient(Time_rough_test);
    alphaz_rough = gradient(Inputs_rough_test(7, :)) ./ gradient(Time_rough_test);
    alpha_rough = [alphax_rough; alphay_rough; alphaz_rough];
    omega_rough = [Inputs_rough_test(5, :); Inputs_rough_test(6, :); Inputs_rough_test(7, :)];
    
    Htest_rough = zeros(3, 3, length(alpha_rough));
    
    for i = 1:length(alpha_rough)
        
        Htest_rough(:, :, i) = calc_H(alpha_rough(:, i), omega_rough(:, i));
    end
    
    Ytestxyz_rough = [T_rough_test{1}'; T_rough_test{2}'; T_rough_test{3}'];
    
    resultxyz_rough = [resultx_rough; resulty_rough; resultz_rough];
    
    Uall_rough_train = zeros(length(U_rough_train{1}), 7);
    
    Uall_rough_train(:, 1:5) = U_rough_train{1};
    
    Uall_rough_train(:, 6) = U_rough_train{2}(:, 5);
    
    Uall_rough_train(:, 7) = U_rough_train{3}(:, 5);
    
    
    Uall_rough_test = zeros(length(U_rough_test{1}), 7);
    
    Uall_rough_test(:, 1:5) = U_rough_test{1};
    
    Uall_rough_test(:, 6) = U_rough_test{2}(:, 5);
    
    Uall_rough_test(:, 7) = U_rough_test{3}(:, 5);
    
    [xestimate_rough, xmin_rough, fmin_rough, counteval_rough, stopflag_rough, outs_rough, bestever_rough] = LRGA(Points_carsim, Htest_rough, Uall_rough_train, Uall_rough_test, Ytestxyz_rough, resultxyz_rough);
    
    %Step
    
    alphax_step = gradient(Inputs_step_test(5, :)) ./ gradient(Time_step_test);
    alphay_step = gradient(Inputs_step_test(6, :)) ./ gradient(Time_step_test);
    alphaz_step = gradient(Inputs_step_test(7, :)) ./ gradient(Time_step_test);
    alpha_step = [alphax_step; alphay_step; alphaz_step];
    omega_step = [Inputs_step_test(5, :); Inputs_step_test(6, :); Inputs_step_test(7, :)];
    
    Htest_step = zeros(3, 3, length(alpha_step));
    
    for i = 1:length(alpha_step)
        
        Htest_step(:, :, i) = calc_H(alpha_step(:, i), omega_step(:, i));
    end
    
    Ytestxyz_step = [T_step_test{1}'; T_step_test{2}'; T_step_test{3}'];
    
    resultxyz_step = [resultx_step; resulty_step; resultz_step];
    
    Uall_step_train = zeros(length(U_step_train{1}), 7);
    
    Uall_step_train(:, 1:5) = U_step_train{1};
    
    Uall_step_train(:, 6) = U_step_train{2}(:, 5);
    
    Uall_step_train(:, 7) = U_step_train{3}(:, 5);
    
    
    Uall_step_test = zeros(length(U_step_test{1}), 7);
    
    Uall_step_test(:, 1:5) = U_step_test{1};
    
    Uall_step_test(:, 6) = U_step_test{2}(:, 5);
    
    Uall_step_test(:, 7) = U_step_test{3}(:, 5);
    
    
    [xestimate_step, xmin_step, fmin_step, counteval_step, stopflag_step, outs_step, bestever_step] = LRGA(Points_carsim, Htest_step, Uall_step_train, Uall_step_test, Ytestxyz_step, resultxyz_step);
    
    %Step Noise
    
    alphax_stepnoise = gradient(Inputs_stepnoise_test(5, :)) ./ gradient(Time_stepnoise_test);
    alphay_stepnoise = gradient(Inputs_stepnoise_test(6, :)) ./ gradient(Time_stepnoise_test);
    alphaz_stepnoise = gradient(Inputs_stepnoise_test(7, :)) ./ gradient(Time_stepnoise_test);
    alpha_stepnoise = [alphax_stepnoise; alphay_stepnoise; alphaz_stepnoise];
    omega_stepnoise = [Inputs_stepnoise_test(5, :); Inputs_stepnoise_test(6, :); Inputs_stepnoise_test(7, :)];
    
    Htest_stepnoise = zeros(3, 3, length(alpha_stepnoise));
    
    for i = 1:length(alpha_stepnoise)
        
        Htest_stepnoise(:, :, i) = calc_H(alpha_stepnoise(:, i), omega_stepnoise(:, i));
    end
    
    Ytestxyz_stepnoise = [T_stepnoise_test{1}'; T_stepnoise_test{2}'; T_stepnoise_test{3}'];
    
    resultxyz_stepnoise = [resultx_stepnoise; resulty_stepnoise; resultz_stepnoise];
    
    Uall_stepnoise_train = zeros(length(U_stepnoise_train{1}), 7);
    
    Uall_stepnoise_train(:, 1:5) = U_stepnoise_train{1};
    
    Uall_stepnoise_train(:, 6) = U_stepnoise_train{2}(:, 5);
    
    Uall_stepnoise_train(:, 7) = U_stepnoise_train{3}(:, 5);
    
    
    Uall_stepnoise_test = zeros(length(U_stepnoise_test{1}), 7);
    
    Uall_stepnoise_test(:, 1:5) = U_stepnoise_test{1};
    
    Uall_stepnoise_test(:, 6) = U_stepnoise_test{2}(:, 5);
    
    Uall_stepnoise_test(:, 7) = U_stepnoise_test{3}(:, 5);
    
    [xestimate_stepnoise, xmin_stepnoise, fmin_stepnoise, counteval_stepnoise, stopflag_stepnoise, outs_stepnoise, bestever_stepnoise] = LRGA(Points_carsim, Htest_stepnoise, Uall_stepnoise_train, Uall_stepnoise_test, Ytestxyz_stepnoise, resultxyz_stepnoise);
    
    %Vel
    
    alphax_vel = gradient(Inputs_vel_test(5, :)) ./ gradient(Time_vel_test);
    alphay_vel = gradient(Inputs_vel_test(6, :)) ./ gradient(Time_vel_test);
    alphaz_vel = gradient(Inputs_vel_test(7, :)) ./ gradient(Time_vel_test);
    alpha_vel = [alphax_vel; alphay_vel; alphaz_vel];
    omega_vel = [Inputs_vel_test(5, :); Inputs_vel_test(6, :); Inputs_vel_test(7, :)];
    
    Htest_vel = zeros(3, 3, length(alpha_vel));
    
    for i = 1:length(alpha_vel)
        
        Htest_vel(:, :, i) = calc_H(alpha_vel(:, i), omega_vel(:, i));
    end
    
    Ytestxyz_vel = [T_vel_test{1}'; T_vel_test{2}'; T_vel_test{3}'];
    
    resultxyz_vel = [resultx_vel; resulty_vel; resultz_vel];
    
    Uall_vel_train = zeros(length(U_vel_train{1}), 7);
    
    Uall_vel_train(:, 1:5) = U_vel_train{1};
    
    Uall_vel_train(:, 6) = U_vel_train{2}(:, 5);
    
    Uall_vel_train(:, 7) = U_vel_train{3}(:, 5);
    
    
    Uall_vel_test = zeros(length(U_vel_test{1}), 7);
    
    Uall_vel_test(:, 1:5) = U_vel_test{1};
    
    Uall_vel_test(:, 6) = U_vel_test{2}(:, 5);
    
    Uall_vel_test(:, 7) = U_vel_test{3}(:, 5);
    
    [xestimate_vel, xmin_vel, fmin_vel, counteval_vel, stopflag_vel, outs_vel, bestever_vel] = LRGA(Points_carsim, Htest_vel, Uall_vel_train, Uall_vel_test, Ytestxyz_vel, resultxyz_vel);
    
    %Exp
 
 disp('Got to Plots!')
   
% Plots

    % (1) Input Time Series
    
        %Rough
        figure(10)
        subplot(7, 1, 1)
        plot(Time_rough_train, Inputs_rough_train(1, :))
        xlabel('Time (s)')
        ylabel('Steer Angle (deg)')
        
        subplot(7, 1, 2)
        plot(Time_rough_train, Inputs_rough_train(2, :))
        xlabel('Time (s)')
        ylabel('Velocity (km/hr)')
        
        subplot(7, 1, 3)
        plot(Time_rough_train, Inputs_rough_train(3, :))
        xlabel('Time (s)')
        ylabel('Velocity (km/hr)')
        
        subplot(7, 1, 4)
        plot(Time_rough_train, Inputs_rough_train(4, :))
        xlabel('Time (s)')
        ylabel('Velocity (km/hr)')
        
        subplot(7, 1, 5)
        plot(Time_rough_train, Inputs_rough_train(5, :))
        xlabel('Time (s)')
        ylabel('Angular Velocity (deg/s)')
        
        subplot(7, 1, 6)
        plot(Time_rough_train, Inputs_rough_train(6, :))
        xlabel('Time (s)')
        ylabel('Angular Velocity (deg/s)')
        
        subplot(7, 1, 7)
        plot(Time_rough_train, Inputs_rough_train(7, :))
        xlabel('Time (s)')
        ylabel('Angular Velocity (deg/s)')
    
    % (2) Target Time Series
        figure(20)
        subplot(3, 1, 1)
        plot(Time_rough_train, Targets_rough_train(1, :))
        xlabel('Time(s)')
        ylabel('Accleration (m/s^2)')
        
        subplot(3, 1, 2)
        plot(Time_rough_train, Targets_rough_train(2, :))
        xlabel('Time(s)')
        ylabel('Accleration (m/s^2)')
        
        subplot(3, 1, 3)
        plot(Time_rough_train, Targets_rough_train(3, :))
        xlabel('Time(s)')
        ylabel('Accleration (m/s^2)')
    
    % (3) X/Y/Z Pred vs Truth
    
        %Rough
        figure(31)
        subplot(2, 3, 1)
        plot(Time_rough_test, resultx_rough, 'b', Time_rough_test, T_rough_test{1}, 'k')
        xlabel('Time (s)')
        ylabel('ax')

        subplot(2, 3, 2)
        plot(Time_rough_test, resulty_rough, 'b', Time_rough_test, T_rough_test{2}, 'k')
        xlabel('Time (s)')
        ylabel('ay')

        subplot(2, 3, 3)
        plot(Time_rough_test, resultz_rough, 'b', Time_rough_test, T_rough_test{3}, 'k')
        xlabel('Time (s)')
        ylabel('az')
        
        subplot(2, 3, 4)
        plot(Time_rough_test, residualx_rough', 'k')
        xlabel('Time (s)')
        ylabel('ax')

        subplot(2, 3, 5)
        plot(Time_rough_test, residualy_rough', 'k')
        xlabel('Time (s)')
        ylabel('ay')

        subplot(2, 3, 6)
        plot(Time_rough_test, residualz_rough', 'k')
        xlabel('Time (s)')
        ylabel('az')
        
        
        
        %Step
        figure(32)
        subplot(2, 3, 1)
        plot(Time_step_test, resultx_step, 'b', Time_step_test, T_step_test{1}, 'k')
        xlabel('Time (s)')
        ylabel('ax')

        subplot(2, 3, 2)
        plot(Time_step_test, resulty_step, 'b', Time_step_test, T_step_test{2}, 'k')
        xlabel('Time (s)')
        ylabel('ay')

        subplot(2, 3, 3)
        plot(Time_step_test, resultz_step, 'b', Time_step_test, T_step_test{3}, 'k')
        xlabel('Time (s)')
        ylabel('az')
        
        subplot(2, 3, 4)
        plot(Time_step_test, residualx_step', 'k')
        xlabel('Time (s)')
        ylabel('ax')

        subplot(2, 3, 5)
        plot(Time_step_test, residualy_step', 'k')
        xlabel('Time (s)')
        ylabel('ay')

        subplot(2, 3, 6)
        plot(Time_step_test, residualz_step', 'k')
        xlabel('Time (s)')
        ylabel('az')
        
        %Step w/ Noise 
        figure(33)
        subplot(2, 3, 1)
        plot(Time_stepnoise_test, resultx_stepnoise, 'b', Time_stepnoise_test, T_stepnoise_test{1}, 'k')
        xlabel('Time (s)')
        ylabel('ax')

        subplot(2, 3, 2)
        plot(Time_stepnoise_test, resulty_stepnoise, 'b', Time_stepnoise_test, T_stepnoise_test{2}, 'k')
        xlabel('Time (s)')
        ylabel('ay')

        subplot(2, 3, 3)
        plot(Time_stepnoise_test, resultz_stepnoise, 'b', Time_stepnoise_test, T_stepnoise_test{3}, 'k')
        xlabel('Time (s)')
        ylabel('az')
        
        subplot(2, 3, 4)
        plot(Time_stepnoise_test, residualx_stepnoise', 'k')
        xlabel('Time (s)')
        ylabel('ax')

        subplot(2, 3, 5)
        plot(Time_stepnoise_test, residualy_stepnoise', 'k')
        xlabel('Time (s)')
        ylabel('ay')

        subplot(2, 3, 6)
        plot(Time_stepnoise_test, residualz_stepnoise', 'k')
        xlabel('Time (s)')
        ylabel('az')
        
        %Vel
        figure(34)
        subplot(2, 3, 1)
        plot(Time_vel_test, resultx_vel, 'b', Time_vel_test, T_vel_test{1}, 'k')
        xlabel('Time (s)')
        ylabel('ax')

        subplot(2, 3, 2)
        plot(Time_vel_test, resulty_vel, 'b', Time_vel_test, T_vel_test{2}, 'k')
        xlabel('Time (s)')
        ylabel('ay')

        subplot(2, 3, 3)
        plot(Time_vel_test, resultz_vel, 'b', Time_vel_test, T_vel_test{3}, 'k')
        xlabel('Time (s)')
        ylabel('az')
        
        subplot(2, 3, 4)
        plot(Time_vel_test, residualx_vel', 'k')
        xlabel('Time (s)')
        ylabel('ax')

        subplot(2, 3, 5)
        plot(Time_vel_test, residualy_vel', 'k')
        xlabel('Time (s)')
        ylabel('ay')

        subplot(2, 3, 6)
        plot(Time_vel_test, residualz_vel', 'k')
        xlabel('Time (s)')
        ylabel('az')
        
        %Exp
        figure(35)
        subplot(2, 3, 1)
        plot(Time_exp_test, resultx_exp, 'b', Time_exp_test, T_exp_test{1}, 'k')
        xlabel('Time (s)')
        ylabel('ax')

        subplot(2, 3, 2)
        plot(Time_exp_test, resulty_exp, 'b', Time_exp_test, T_exp_test{2}, 'k')
        xlabel('Time (s)')
        ylabel('ay')

        subplot(2, 3, 3)
        plot(Time_exp_test, resultz_exp, 'b', Time_exp_test, T_exp_test{3}, 'k')
        xlabel('Time (s)')
        ylabel('az')
        
        subplot(2, 3, 4)
        plot(Time_exp_test, residualx_exp', 'k')
        xlabel('Time (s)')
        ylabel('ax')

        subplot(2, 3, 5)
        plot(Time_exp_test, residualy_exp', 'k')
        xlabel('Time (s)')
        ylabel('ay')

        subplot(2, 3, 6)
        plot(Time_exp_test, residualz_exp', 'k')
        xlabel('Time (s)')
        ylabel('az')
        
    % (4) Likelihood Heat Map
        
        %Rough
        
        %Step
        
        %Step w/ Noise 
        
        %Vel
        
        %Exp
        
    % (5) Normalization 3D Scatter plot
    
    
    