    %T2 - Figure 8
        
    bag_f8_t2_ixblue = rosbag('C:\Users\gmm0050\Documents\greg_folder\figure_8_computer_2021-05-01-17-33-00.bag');
    
    bag_f8_t2_novatel = rosbag('C:\Users\gmm0050\Documents\greg_folder\figure_8_car_2021-05-01-16-33-02.bag');
    
        %Memsense
        
        imu_mems_f8 = readMessages(select(bag_f8_t2_ixblue, "Topic", "/imu/data"));
        
        imu_mems_filtered_f8 = readMessages(select(bag_f8_t2_ixblue, "Topic", "/imu/data_filtered"));
        
        imu_mems_calibrated_f8 = readMessages(select(bag_f8_t2_ixblue, "Topic", "/imu/data_calibrated"));

        %IxBlue
        
        %ixblue_inertial_f8 = readMessages(select(bag_f8_t2_ixblue, "Topic", "/ixblue_ins_driver/ix/ins")); %N
        
        ixblue_imu_f8 = readMessages(select(bag_f8_t2_ixblue, "Topic", "/ixblue_ins_driver/standard/imu"));
        
        ixblue_gps_f8 = readMessages(select(bag_f8_t2_ixblue, "Topic", "/ixblue_ins_driver/standard/navSatFix"));
        
        %Novatel
        
        novatel_gps_f8 = readMessages(select(bag_f8_t2_novatel, "Topic", "/novatel/navSatFix"));
        
        %novatel_time_f8 = readMessages(select(bag_f8_t2_novatel, "Topic", "/novatel/gpsTimeTagged"));

        novatel_odom_f8 = readMessages(select(bag_f8_t2_novatel, "Topic", "/novatel/odom"));
        
        %Car Data
        
        car_steer_f8 = readMessages(select(bag_f8_t2_novatel, "Topic", "/vehicle/steering_report")); %N
        
        car_gpsvel_f8 = readMessages(select(bag_f8_t2_novatel, "Topic", "/vehicle/gps/vel"));
        
        car_gpstime_f8 = readMessages(select(bag_f8_t2_novatel, "Topic", "/vehicle/gps/time"));
        
        car_gps_f8 = readMessages(select(bag_f8_t2_novatel, "Topic", "/vehicle/gps/fix"));
        
        car_imu_f8 = readMessages(select(bag_f8_t2_novatel, "Topic", "/vehicle/imu/data_raw"));
        
        car_brake_f8 = readMessages(select(bag_f8_t2_novatel, "Topic", "/vehicle/brake_report")); %N
        
        
        
            %Figure - 8
    
        %Inputs (Steering + GPS Velocities + IxBlue Angular Velocities)
        
        delta_f8_t2 = cellfun(@(m) double(m.SteeringWheelAngle), car_steer_f8);
        
        vx_f8_t2 = cellfun(@(m) double(m.Twist.Twist.Linear.X), novatel_odom_f8); %% Use the Novatel for this?
        
        vy_f8_t2 = cellfun(@(m) double(m.Twist.Twist.Linear.Y), novatel_odom_f8);
        
        vz_f8_t2 = cellfun(@(m) double(m.Twist.Twist.Linear.Z), novatel_odom_f8);
        
        vtheta_f8_t2 = cellfun(@(m) double(m.AngularVelocity.X), ixblue_imu_f8);
        
        vphi_f8_t2 = cellfun(@(m) double(m.AngularVelocity.Y), ixblue_imu_f8);
        
        vpsi_f8_t2 = cellfun(@(m) double(m.AngularVelocity.Z), ixblue_imu_f8);
        
        %Targets (IxBlue Linear Accelerations
        
        ax_f8_t2 = cellfun(@(m) double(m.LinearAcceleration.X), ixblue_imu_f8);
        
        ay_f8_t2 = cellfun(@(m) double(m.LinearAcceleration.Y), ixblue_imu_f8);
        
        az_f8_t2 = cellfun(@(m) double(m.LinearAcceleration.Z), ixblue_imu_f8);
        
        %Sensor Accelerations
        
        ax_sens_f8_t2 = cellfun(@(m) double(m.LinearAcceleration.X), imu_mems_f8);
        
        ay_sens_f8_t2 = cellfun(@(m) double(m.LinearAcceleration.Y), imu_mems_f8);
        
        az_sens_f8_t2 = cellfun(@(m) double(m.LinearAcceleration.Z), imu_mems_f8);
        
        %Extras
        
        ax_sens_filt_f8_t2 = cellfun(@(m) double(m.LinearAcceleration.X), imu_mems_filtered_f8);
        
        ay_sens_filt_f8_t2 = cellfun(@(m) double(m.LinearAcceleration.Y), imu_mems_filtered_f8);
        
        az_sens_filt_f8_t2 = cellfun(@(m) double(m.LinearAcceleration.Z), imu_mems_filtered_f8);
        
        gpslat_f8_t2 = cellfun(@(m) double(m.Latitude), novatel_gps_f8);
        
        gpslong_f8_t2 = cellfun(@(m) double(m.Longitude), novatel_gps_f8);
        
        gpsalt_f8_t2 = cellfun(@(m) double(m.Altitude), novatel_gps_f8);
        
        
        imu_sec = cellfun(@(m) double(m.Header.Stamp.Sec), imu_mems_filtered_f8);
        
        imu_nsec = cellfun(@(m) double(m.Header.Stamp.Nsec), imu_mems_filtered_f8);
        
        gps_sec = cellfun(@(m) double(m.Header.Stamp.Sec), novatel_odom_f8);
        
        gps_nsec = cellfun(@(m) double(m.Header.Stamp.Nsec), novatel_odom_f8);
        
        delta_sec = cellfun(@(m) double(m.Header.Stamp.Sec), car_steer_f8);
        
        delta_nsec = cellfun(@(m) double(m.Header.Stamp.Nsec), car_steer_f8);
        
        gyro_sec = cellfun(@(m) double(m.Header.Stamp.Sec), ixblue_imu_f8);
        
        gyro_nsec = cellfun(@(m) double(m.Header.Stamp.Nsec), ixblue_imu_f8);
        

        
        