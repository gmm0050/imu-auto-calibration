function [r, Uinds] = findnclosest(U, u, n)

Distvec = U - u;

Distssq = sum(Distvec.^2, 2);

[~, Uinds] = mink(Distssq, n);

r = sqrt( max( Distssq(Uinds) ) );


end