function [normInputs, normTargets] = Normalize_Data(inputs, targets, alin, Time, gradmethod, is_nonlinear, sf, vcf, wcf, acf)

%vcf = 0.2778;%Conversion factor from km/hr to m/s
%wcf = pi/180;%Conversion factor from deg/s to rad/s
%g = 9.81; %Gravity
eps = 0.0001;%Tiny value to prevent a Divide-By-Zero Error
%sf = 1000;%Scaling Factor for Input Space

%Apply a Normalization procedure that will give us a set of inputs and
%targets with a relationship that can be easily be modeled by a GRBFNN

delta = inputs(1, :);
vx = inputs(2, :);
vy = inputs(3, :);
vz = inputs(4, :);
vtheta = inputs(5, :);
vphi = inputs(6, :);
vpsi = inputs(7, :);
%force = inputs(8, :);

ax = targets(1, :);
ay = targets(2, :);
az = targets(3, :);

sfx = sf(1);
sfy = sf(2);
sfz = sf(3);


%Normalize targets using a linear estimate of the acceleration taken from
%the measured velocities

vx = vx*vcf;
vy = vy*vcf;
vz = vz*vcf;

vtheta = vtheta*wcf;
vphi = vphi*wcf;
vpsi = vpsi*wcf;

axlin = alin(1, :);
aylin = alin(2, :);
azlin = alin(3, :);

ax = ax*acf;
ay = ay*acf;
az = az*acf;

if is_nonlinear == true
    Tx = ax - axlin;
    Ty = ay - aylin;
    Tz = az - azlin;
else
    Tx = ax;
    Ty = ay;
    Tz = az;
end

Tx = Tx';
Ty = Ty';
Tz = Tz';


%Feature Calculations

omxcross = vphi.*vz - vpsi.*vy;
omycross = vpsi.*vx - vtheta.*vz;
omzcross = vtheta.*vy - vx.*vphi;

beta = vy./vx; %beta = atand(vy./vx);
vbeta = gradient_estimate(beta, Time, gradmethod);

vdelta = gradient_estimate(delta, Time, gradmethod);


    %Feature Normalization Factors
    
    Rv = sqrt(vx.^2 + vy.^2 + vz.^2 + eps);
    
    Rcross = sqrt(omxcross.^2 + omycross.^2 + omzcross.^2 + eps);
    
    Romega = sqrt(vtheta.^2 + vphi.^2 + vz.^2 + eps);
    
    Rdelta = 180;
    
    %Applying Normalization Factors
    
    deltanormx = sfx*delta ./ Rdelta;
    deltanormy = sfy*delta ./ Rdelta;
    deltanormz = sfz*delta ./ Rdelta;

    vdeltanormx = sfx*vdelta ./ Rdelta;
    vdeltanormy = sfy*vdelta ./ Rdelta;
    vdeltanormz = sfz*vdelta ./ Rdelta;
    
    vxnorm = sf*vx ./ Rv;
    vynorm = sf*vy ./ Rv;
    vznorm = sf*vz ./ Rv;
    
    vthetanorm = sf*vtheta ./ Romega;
    vphinorm = sf*vphi ./ Romega;
    vpsinorm = sf*vpsi ./ Romega;
    
    omxcrossnorm = sf*omxcross ./ Rcross;
    omycrossnorm = sf*omycross ./ Rcross;
    omzcrossnorm = sf*omzcross ./ Rcross;
    
    % Features using RV

    kappa = ((Rv - vx)./(vx + eps)).*100;
    

    %Features to use for Training
    
    xFeatures = [deltanormx; Rv; kappa; vdeltanormx]; %[deltanorm; aylin; Rv; force];
    
    yFeatures = [deltanormy; Rv; kappa; vdeltanormy];
    
    zFeatures = [deltanormz; Rv; kappa; vdeltanormz];
    
%Create Input Matrices out of Features

xF = xFeatures';

yF = yFeatures';

zF = zFeatures';



%[xFtrain,xFval]=matricizeX(xinputvec, data_division, delay, len(xFeatures));
    
%[yFtrain,yFval]=matricizeX(yinputvec, data_division, delay, len(yFeatures));

%[zFtrain,zFval]=matricizeX(zinputvec, data_division, delay, len(zFeatures));

%Output a Cell Array containing the Normalized Inputs and Targets

normTargets = {};

normTargets{1} = Tx;
normTargets{2} = Ty;
normTargets{3} = Tz;

normInputs = {};

normInputs{1} = xF;
normInputs{2} = yF;
normInputs{3} = zF;

end


function [acclin] = gradient_estimate(v, Time, method)

%Backward
% if method == 'Backward'
%     
%     acclin = ( v(t) - v(t-1) ) / dt;
%     
% elseif method == 'Forward'
%         
%     acclin = ( v(t+1) - v(t) ) / dt;
%     
% elseif method == 'Three-Point'
%     
%     acclin = ( v(t+1) - v(t-1) ) / (2*dt);
%     
% elseif method == 'Five-Point'
%     
%     acclin = ( -1*v(t+2) + 8*v(t+1) -8*v(t-1) + v(t-2) ) / (12*dt);
    
if method == 'Gradient'
    
    acclin = gradient(v)./ gradient(Time);
    
else 
    
    print('Error: Method Not Implemented')
end
end