function [xmin, fmin, counteval, stopflag, outs, bestever] = MLS(Htest, resultxyz, Ysensorxyz, cp, T, is_fulldim, is_rot, wstatic, x0, sigma0, theta, phi)




%Main Function : CMA optimization
%------
tic

[xmin, fmin, counteval, stopflag, outs, bestever] = cmaes_ga('Likefun', x0, sigma0, [], theta, phi, resultxyz, Ysensorxyz, Htest, cp, T, is_fulldim, is_rot, wstatic);

toc
%------





end