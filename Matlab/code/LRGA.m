%load('C:\Users\gmm0050\Documents\MATLAB\Vehicle Simulations\Runs\Run5\Clustering\Clusteringresults.mat');
% Points = load('C:\Users\gmm0050\Documents\MATLAB\Vehicle Simulations\Runs\Run5\Clustering\Clusteringresults.mat', 'Points');

%Rough

%Step

%Step Noise

%Vel

%Exp


function [xestimate, xmin, fmin, counteval, stopflag, outs, bestever] = LRGA(Points, Htest, Utrain, Utest, Ytestxyz, resultxyz)
% Operator Parameters
%----------------
x0 = Points(:, 500);
psi0 = 0;
sigma0 = [2; 2; 2];
sigmapsi0 = 30;
is_rot = true;
%----------------



%load('C:\Users\gmm0050\Documents\MATLAB\Vehicle Simulations\Runs\Run5\Clustering\Clusteringresults.mat');
g = 9.81;
gvec = [0; 0; -g];

thetasensor = 10.87;%Truth value for theta
phisensor = 20.19;%Truth value for phi
psisensor = 5.28;%Truth value for psi
Rsensor = eye(3);

% Htest = Htest_rough;
% Utrain = Utrain_rough;
% Utest = Utest_rough;
% Ytestxyz = [xtest_rough'; ytest_rough'; ztest_rough'];
% resultxyz = [resultx_rough_no; resulty_rough_no; resultz_rough_no];
Sensorpoint = [0.4046; 0.7569; 0.8705]; %Truth value for x, y, z (e.g. the Lever Arm)
cp = [-2.578; 0; 0.540]; %Control Point Location
xmin_rough

if is_rot == true
    
    sigma0 = [sigma0; sigmapsi0];
    x0 = [x0; psi0];
    Rsensor = rotmatrix(thetasensor, phisensor, psisensor);
    
end


Ysensorxyz = zeros(3, length(resultxyz));

%length(resultxyz);

for t = 1:length(Htest(1, 1, :))
    
    Ysensorxyz(:, t) = Rsensor*(Ytestxyz(:, t) + Htest(:, :, t)*(Sensorpoint - cp));
end

initaccels = Rsensor*gvec;
T = length(resultxyz);
maxinitepochs = 50000;
%------
tic

att0 = initialize_r(initaccels, maxinitepochs);
theta = att0(1);
phi = att0(2);


%Main Function : CMA optimization
%------
tic

[xmin, fmin, counteval, stopflag, outs, bestever] = cmaes_ga('Likefun', x0, sigma0, [], theta, phi, resultxyz, Ysensorxyz, Htest, Utest, Utrain, cp, T, is_rot);

toc
%------

xminloc = xmin(1:3);

[rs, closest_indices] = findnclosest(Points, xminloc, 3);

closest_mesh_points = Points(:, closest_indices);

xestimate = closest_mesh_points(:, 1);
%xestimate = GA_search_polygon(closest_mesh_points)

end

