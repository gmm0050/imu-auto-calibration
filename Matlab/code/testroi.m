function [ result, residual]= testroi(w, c, b, r, Centers, Utest, Ytest, outclass, nout, ndim)
                

testing = length(Ytest);
approx = zeros(1, testing);
ell = length(w);
% if phi==1
% if test==1 

    for bl=1:testing	   %finds our function value 
        y=0;							
        for jl=1:ell
            moo = 0.0;
            if norm( Utest(bl, :) - Centers(jl, :) ) <= r(jl)
                for zj=1:ndim    
                    etay = log(w(jl))*( Utest(bl,zj)- Centers(jl,zj) )^2; %% Change from log(SS) to something else?
                    moo = moo + etay;
                end  
                m = c(jl)*exp(moo) + b(jl);    %finds the function value
                y=y+m;  %makes the sum
            end
        end														
        approx(bl)=y;										
    end

if outclass == 1
    result = approx;
    residual = Ytest - result';
elseif outclass == 2
    result = sign(approx);
end