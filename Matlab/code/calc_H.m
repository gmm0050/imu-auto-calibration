function H = calc_H(alpha, omega)

sa = skew(alpha);
so = skew(omega);
H = sa + so^2;

end

function M = skew(vec)

M = [0, -1*vec(3), vec(2); vec(3), 0, -1*vec(1); -1*vec(2), vec(1) 0];

end