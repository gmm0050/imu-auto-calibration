function [likelihood] = likelihood(deltavec, u)

likelihood = log(2*pi) - (1/2*(norm(u))^2)*( norm(deltavec)^2 );


end