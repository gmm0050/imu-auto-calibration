    %T2 - Road
        
    bag_road_t2_ixblue = rosbag('C:\Users\gmm0050\Documents\greg_folder\road_drive_computer_2021-05-01-17-36-27.bag');
    
    bag_road_t2_novatel = rosbag('C:\Users\gmm0050\Documents\greg_folder\road_drive_car_2021-05-01-16-36-31.bag');
    
        %Memsense
        
        imu_mems_road = readMessages(select(bag_road_t2_ixblue, "Topic", "/imu/data"));
        
        imu_mems_filtered_road = readMessages(select(bag_road_t2_ixblue, "Topic", "/imu/data_filtered"));
        
        imu_mems_calibrated_road = readMessages(select(bag_road_t2_ixblue, "Topic", "/imu/data_calibrated"));

        %IxBlue
        
        %ixblue_inertial_road = readMessages(select(bag_road_t2_ixblue, "Topic", "/ixblue_ins_driver/ix/ins"));
        
        ixblue_imu_road = readMessages(select(bag_road_t2_ixblue, "Topic", "/ixblue_ins_driver/standard/imu"));
        
        ixblue_gps_road = readMessages(select(bag_road_t2_ixblue, "Topic", "/ixblue_ins_driver/standard/navSatFix"));
        
        %Novatel
        
        novatel_gps_road = readMessages(select(bag_road_t2_novatel, "Topic", "/novatel/navSatFix"));
        
        %novatel_time_road = readMessages(select(bag_road_t2_novatel, "Topic", "/novatel/gpsTimeTagged"));
        
        novatel_odom_road = readMessages(select(bag_road_t2_novatel, "Topic", "/novatel/odom"));
        
        %Car Data
        
        car_steer_road = readMessages(select(bag_road_t2_novatel, "Topic", "/vehicle/steering_report"));
        
        car_gpsvel_road = readMessages(select(bag_road_t2_novatel, "Topic", "/vehicle/gps/vel"));
        
        car_gpstime_road = readMessages(select(bag_road_t2_novatel, "Topic", "/vehicle/gps/time"));
        
        car_gps_road = readMessages(select(bag_road_t2_novatel, "Topic", "/vehicle/gps/fix"));
        
        car_imu_road = readMessages(select(bag_road_t2_novatel, "Topic", "/vehicle/imu/data_raw"));
        
        car_brake_road = readMessages(select(bag_road_t2_novatel, "Topic", "/vehicle/brake_report"));
        
        
                delta_road_t2 = cellfun(@(m) double(m.steering_wheel_angle), car_steer_road) ;
        
        vx_road_t2 = cellfun(@(m) double(m.Twist.Twist.Linear.X), novatel_odom_road); %% Use the Novatel for this?
        
        vy_road_t2 = cellfun(@(m) double(m.Twist.Twist.Linear.Y), novatel_odom_road);
        
        vz_road_t2 = cellfun(@(m) double(m.Twist.Twist.Linear.Z), novatel_odom_road);
        
        vtheta_road_t2 = cellfun(@(m) double(m.AngularVelocity.X), ixblue_imu_road) ;
        
        vphi_road_t2 = cellfun(@(m) double(m.AngularVelocity.Y), ixblue_imu_road) ;
        
        vpsi_road_t2 = cellfun(@(m) double(m.AngularVelocity.Z), ixblue_imu_road) ;
        
        %Targets (IxBlue Linear Accelerations
        
        ax_road_t2 = cellfun(@(m) double(m.LinearAcceleration.X), ixblue_imu_road);
        
        ay_road_t2 = cellfun(@(m) double(m.LinearAcceleration.Y), ixblue_imu_road);
        
        az_road_t2 = cellfun(@(m) double(m.LinearAcceleration.Z), ixblue_imu_road);
        
        %Sensor Accelerations
        
        ax_sens_road_t2 = cellfun(@(m) double(m.LinearAcceleration.X), imu_mems_road);
        
        ay_sens_road_t2 = cellfun(@(m) double(m.LinearAcceleration.Y), imu_mems_road);
        
        az_sens_road_t2 = cellfun(@(m) double(m.LinearAcceleration.Z), imu_mems_road);
        
        %Extras
        
        ax_sens_filt_road_t2 = cellfun(@(m) double(m.LinearAcceleration.X), imu_mems_filtered_road);
        
        ay_sens_filt_road_t2 = cellfun(@(m) double(m.LinearAcceleration.Y), imu_mems_filtered_road);
        
        az_sens_filt_road_t2 = cellfun(@(m) double(m.LinearAcceleration.Z), imu_mems_filtered_road);
        
        gpslat_road_t2 = cellfun(@(m) double(m.Latitude), novatel_gps_road);
        
        gpslong_road_t2 = cellfun(@(m) double(m.Longitude), novatel_gps_road);
        
        gpsalt_road_t2 = cellfun(@(m) double(m.Altitude), novatel_gps_road);  
    