

function [attitude] = initialize_r(measured, maxepochs)


%Use a sim annealing search method to initialize the rotation matrix R,
%given readings of the IMU at rest. The g-vector will point directly
%towards the center of the earth.

%Initialize orientation by finding the mean value of the acceleration
%reading at rest. This direction will be closest to -z / +z
[~, maxindex] = max(abs(measured));


if maxindex == 1
    
    if measured(maxindex) > 0
        
        attitude0 = [0; -90; 0];
        
    elseif measured(maxindex) < 0
        
        attitude0 = [0; 90; 0];
        
    end
elseif maxindex == 2

    if measured(maxindex) > 0
        
        attitude0 = [90; 0; 0];
        
    elseif measured(maxindex) < 0
        
        attitude0 = [-90; 0; 0];
        
    end
elseif maxindex == 3
    
    if measured(maxindex) > 0
        
        attitude0 = [180; 0; 0];
        
    elseif measured(maxindex) < 0
        
        attitude0 = [0; 0; 0];
        
    end
end

disp('attitude0')
disp(attitude0)

g = 9.81;


axm = measured(1);
aym = measured(2);
azm = measured(3);

disp('Measured Accelerations')
disp([axm aym azm])

%Plot Error Surface for Visualization

% initgridspace = 2;
% initllim = -180;
% initulim = 180;
% initgridvec = initllim:initgridspace:initulim;
% 
% inittheta = (initgridvec)';
% initphi = (initgridvec)';
% %initpsi = (initgridvec)';
% 
% initaxp = -g*sind(initphi);
% initaxp = reshape(initaxp, 1, length(initaxp), 1);
% initayp = g*sind(inittheta).*cosd(initphi);
% initayp = reshape(initayp,length(initaxp), 1, 1);
% initazp = -g*cosd(inittheta).*cosd(initphi);
% initazp = reshape(initazp, 1, 1, length(initaxp));
%     
% initferror = (axm - initaxp).^2 + (aym - initayp).^2 + (azm - initazp).^2;
% [minerror, minloc] = min(initferror(:, :, 1), [], 'all', 'linear');
% 
% disp('min error')
% disp(minerror)



% z = initferror(:, :, 1);
% 
% figure('Name', 'Error Surface')
% surf(initphi, inittheta, z)
% xlabel('phi')
% ylabel('theta')
% zlabel('MSE')
% hold on
% [maxrows,irow] = max(z);
% [maxz, icol] = max(maxrows);
% h = scatter3(initphi(i),inittheta(i),z(i),'filled');
% h.SizeData = 150;
% [~,irow] = min(z(:));
% h = scatter3(initphi(i),inittheta(i),z(i),'filled');
% h.SizeData = 150;
% hold off

attitude = attitude0;
tau = 0.02;
gridspace = tau;
llim = -3;
ulim = 3;
gridvec = gridspace*llim:gridspace:gridspace*ulim;

disp('Start Loop')

for t = 1:maxepochs
    
    theta0 = attitude(1);
    phi0 = attitude(2);
    psi0 = attitude(3);
    
%     disp('theta0')
%     disp(theta0)
%     disp('phi0')
%     disp(phi0)
%     disp('psi0')
%     disp(psi0)
    
    theta = (theta0 + gridvec)';
    phi = (phi0 + gridvec)';
    psi = (psi0 + gridvec)';
    
%     if size(theta, 1) ~= 11
%         disp('theta dims:')
%         disp(size(theta))
%         disp('theta')
%         disp(theta)
%     end
%     if size(phi, 1) ~= 11
%         disp('phi dims:')
%         disp(size(phi))
%         disp('phi')
%         disp(phi)
%     end
%     if size(psi, 1) ~= 11
%         disp('psi dims:')
%         disp(size(psi))
%         disp('psi')
%         disp(psi)
%     end
    
%     axp = -g*sind(phi);
%     axp = reshape(axp, 1, length(axp), 1);
%     ayp = g*sind(theta).*cosd(phi);
%     ayp = reshape(ayp,length(axp), 1, 1);
%     azp = -g*cosd(theta).*cosd(phi);
%     azp = reshape(azp, 1, 1, length(axp));
%     
%     ferror = (axm - axp).^2 + (aym - ayp).^2 + (azm - azp).^2;
    
%     disp('ferror dimensions:')
%     disp(size(ferror))
%    [ftheta, fphi, fpsi] = gradient(ferror);
    
    
%     itheta = find(theta==theta0, 1);
%     iphi = find(phi==phi0, 1);
%     ipsi = find(psi==psi0, 1);
    
    
%     disp('location index:')
%     disp([itheta, iphi, ipsi])
    
    %fgrad = [ftheta(itheta); fphi(iphi); fpsi(ipsi)];
    ftheta = 2*(aym - g*sind(theta0)*cosd(phi0))*(-g*cosd(theta0)*cosd(phi0)) + 2*(azm + g*cosd(theta0)*cosd(phi0))*(-g*sind(theta0)*cosd(phi0));
    fphi = 2*(axm + g*sind(phi0))*(g*cosd(phi0)) + 2*(aym - g*sind(theta0)*cosd(phi0))*(g*sind(theta0)*sind(phi0)) + 2*(azm + g*cosd(theta0)*cosd(phi0))*(-g*cosd(theta0)*sind(phi0));
    fpsi = 0;
    fgrad = [ftheta; fphi; fpsi];
%     disp('attitude old:')
%     disp(attitude)
    
    attitude = attitude - tau*fgrad;
    
%     disp('attitude new:')
%     disp(attitude)
    
end

end