    %T2 - Square
    
    bag_square_t2_ixblue = rosbag('C:\Users\gmm0050\Documents\greg_folder\square_computer_2021-05-01-17-30-25.bag');
    
    bag_square_t2_novatel = rosbag('C:\Users\gmm0050\Documents\greg_folder\square_car_2021-05-01-16-30-24.bag');
    
        %Memsense
        
        imu_mems_square = readMessages(select(bag_square_t2_ixblue, "Topic", "/imu/data"));
        
        imu_mems_filtered_square = readMessages(select(bag_square_t2_ixblue, "Topic", "/imu/data_filtered"));
        
        imu_mems_calibrated_square = readMessages(select(bag_square_t2_ixblue, "Topic", "/imu/data_calibrated"));

        %IxBlue
        
        %ixblue_inertial_square = readMessages(select(bag_square_t2_ixblue, "Topic", "/ixblue_ins_driver/ix/ins"));
        
        ixblue_imu_square = readMessages(select(bag_square_t2_ixblue, "Topic", "/ixblue_ins_driver/standard/imu"));
        
        ixblue_gps_square = readMessages(select(bag_square_t2_ixblue, "Topic", "/ixblue_ins_driver/standard/navSatFix"));
        
        %Novatel
        
        novatel_gps_square = readMessages(select(bag_square_t2_novatel, "Topic", "/novatel/navSatFix"));
        
        %novatel_time_square = readMessages(select(bag_square_t2_novatel, "Topic", "/novatel/gpsTimeTagged"));
        
        novatel_odom_square = readMessages(select(bag_square_t2_novatel, "Topic", "/novatel/odom"));
        
        %Car Data
        
        car_steer_square = readMessages(select(bag_square_t2_novatel, "Topic", "/vehicle/steering_report"));
        
        car_gpsvel_square = readMessages(select(bag_square_t2_novatel, "Topic", "/vehicle/gps/vel"));
        
        car_gpstime_square = readMessages(select(bag_square_t2_novatel, "Topic", "/vehicle/gps/time"));
        
        car_gps_square = readMessages(select(bag_square_t2_novatel, "Topic", "/vehicle/gps/fix"));
        
        car_imu_square = readMessages(select(bag_square_t2_novatel, "Topic", "/vehicle/imu/data_raw"));
        
        car_brake_square = readMessages(select(bag_square_t2_novatel, "Topic", "/vehicle/brake_report"));
        
        
        
        delta_square_t2 = cellfun(@(m) double(m.steering_wheel_angle), car_steer_square);
        
        vx_square_t2 = cellfun(@(m) double(m.Twist.Twist.Linear.X), novatel_odom_square); %% Use the Novatel for this?
        
        vy_square_t2 = cellfun(@(m) double(m.Twist.Twist.Linear.Y), novatel_odom_square);
        
        vz_square_t2 = cellfun(@(m) double(m.Twist.Twist.Linear.Z), novatel_odom_square);
        
        vtheta_square_t2 = cellfun(@(m) double(m.AngularVelocity.X), ixblue_imu_square);
        
        vphi_square_t2 = cellfun(@(m) double(m.AngularVelocity.Y), ixblue_imu_square);
        
        vpsi_square_t2 = cellfun(@(m) double(m.AngularVelocity.Z), ixblue_imu_square);
        
        %Targets (IxBlue Linear Accelerations
        
        ax_square_t2 = cellfun(@(m) double(m.LinearAcceleration.X), ixblue_imu_square);
        
        ay_square_t2 = cellfun(@(m) double(m.LinearAcceleration.Y), ixblue_imu_square);
        
        az_square_t2 = cellfun(@(m) double(m.LinearAcceleration.Z), ixblue_imu_square);
        
        %Sensor Accelerations
        
        ax_sens_square_t2 = cellfun(@(m) double(m.LinearAcceleration.X), imu_mems_square);
        
        ay_sens_square_t2 = cellfun(@(m) double(m.LinearAcceleration.Y), imu_mems_square);
        
        az_sens_square_t2 = cellfun(@(m) double(m.LinearAcceleration.Z), imu_mems_square);
        
        %Extras
        
        ax_sens_filt_square_t2 = cellfun(@(m) double(m.LinearAcceleration.X), imu_mems_filtered_square);
        
        ay_sens_filt_square_t2 = cellfun(@(m) double(m.LinearAcceleration.Y), imu_mems_filtered_square);
        
        az_sens_filt_square_t2 = cellfun(@(m) double(m.LinearAcceleration.Z), imu_mems_filtered_square);
        
        gpslat_square_t2 = cellfun(@(m) double(m.Latitude), novatel_gps_square);
        
        gpslong_square_t2 = cellfun(@(m) double(m.Longitude), novatel_gps_square);
        
        gpsalt_square_t2 = cellfun(@(m) double(m.Altitude), novatel_gps_square);
        