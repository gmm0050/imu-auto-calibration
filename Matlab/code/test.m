function [ result, residual]=test(w,c,b,U,xtestmat,stufftest, outclass, nout, ndim)
                

SVT = xtestmat;
testing = length(stufftest);
ell = length(w);
% if phi==1
% if test==1 

    for bl=1:testing	   %finds our function value 
        y=0;							
        for jl=0:ell-1 
            moo = 0.0;
            for zj=1:ndim    
                etay = log(w(jl+1))*( SVT(bl,zj)- U(jl+1,zj) )^2; %% Change from log(SS) to something else?
                moo = moo + etay;
            end  
            m = c(jl+1)*exp(moo) + b(jl+1);    %finds the function value
            y=y+m;                                    %makes the sum
        end														
        approx(bl)=y;										
    end

if outclass == 1
    result = approx;
    residual = stufftest - result';
elseif outclass == 2
    result = sign(approx);
end
    
% %signs our predictions for classification
% 
%     if outclass == 2
%         Yout=approx;
%         result = sign(approx);
% 	elseif outclass == 1
%         Yout=approx;
%         result = approx;
%     end

%prints these values to a file function value.output
    fid7 = fopen('functionvalue.output','w');
    fprintf(fid7,'%12.8f\n',result);
    fclose(fid7);

% elseif test==2


%%%

% for bl=1:testing            %finds our function value
%     y=0;							
%     for jl=0:ell-1 
%         moo = 0.0;
% 
%  for zj=1:ndim    
% 
%    eta = log(SS(jl+1))*( SVT(bl,zj)-xsvec(kstar(jl+1),zj) )^2; 
%    moo = moo + eta; 
% 
%  end  
% 
% 
%    m=CC(jl+1)*exp(moo) + DD(jl+1);    %finds the function value
% 
%    y=y+m;                                    %makes the sum
% end														
% approx(bl)=y; 
% 
% %prints these values to a file function value.output
%     fid7 = fopen('functionvalue.output','w');
%     fprintf(fid7,'%12.8f\n',approx(bl));
%     fclose(fid7);
% disp('** Got to Point 4 **')

% end



%signs our predictions for classification

% if outclass == 2
%    result = sign(approx);
% 
% elseif outclass == 1
%    result = approx;
%    
% end


%prints these values to a file function value.output
% fid7 = fopen('functionvalue.output','w');
% fprintf(fid7,'%12.8f\n',result);
%     disp('*** made it here ****')
% fclose(fid7);
% 
% 
% 
% end
%  
% end
end