
%% (0) Set Global Params

dsf = 10;
k = 5; % Number of training loops before validation loop
v = 10; % Number of validation error measurments used to set stopping criterion for training
validationmethod = 1;
valoffset = 5;
gradmethod = 'Gradient';

%% (1) Load in ROS Data from Matlab Workspace

% --- Figure-8 ---

% -- Time Signatures --
load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run8/EXP/fig8/f8t3.mat', 'comptime_delta');
comptime_delta_f8 = comptime_delta;
load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run8/EXP/fig8/f8t3.mat', 'comptime_etalin');
comptime_etalin_f8 = comptime_etalin;
load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run8/EXP/fig8/f8t3.mat', 'comptime_vectornav');
comptime_vectornav_f8 = comptime_vectornav;
load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run8/EXP/fig8/f8t3.mat', 'comptime_novatel');
comptime_novatel_f8 = comptime_novatel;
load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run8/EXP/fig8/f8t3.mat', 'comptime_rrn140');
comptime_rrn140_f8 = comptime_rrn140;
load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run8/EXP/fig8/f8t3.mat', 'comptime_brake');
comptime_brake_f8 = comptime_brake;
load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run8/EXP/fig8/f8t3.mat', 'comptime_throttle');
comptime_throttle_f8 = comptime_throttle;

% For ax_etalin etc
load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run8/EXP/fig8/f8t3.mat', 'comptime_imu_etalin');
comptime_imu_etalin_f8 = comptime_imu_etalin;

% For mkz wheel odometer
load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run8/EXP/fig8/f8t3.mat', 'comptime_vehicle');
comptime_vehicle_f8 = comptime_vehicle;

% For GPS: vx_novatel / vx_septentrio
%gpstime_etalin_f8 = load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run8/EXP/fig8/f8t3.mat', 'gpstime_etalin');
load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run8/EXP/fig8/f8t3.mat', 'gpstime_novatel');
gpstime_novatel_f8 = gpstime_novatel;
load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run8/EXP/fig8/f8t3.mat', 'comptime_septentrio');
comptime_septentrio_f8 = comptime_septentrio;

% -- Accelerations and Angular Velocities

% Etalin
load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run8/EXP/fig8/f8t3.mat', 'ax_etalin');
ax_etalin_f8 = ax_etalin;
load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run8/EXP/fig8/f8t3.mat', 'ay_etalin');
ay_etalin_f8 = ay_etalin;
load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run8/EXP/fig8/f8t3.mat', 'az_etalin');
az_etalin_f8 = az_etalin;%% Flip sign to test %%

load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run8/EXP/fig8/f8t3.mat', 'vtheta_etalin');
vtheta_etalin_f8 = vtheta_etalin;
load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run8/EXP/fig8/f8t3.mat', 'vphi_etalin');
vphi_etalin_f8 = vphi_etalin;
load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run8/EXP/fig8/f8t3.mat', 'vpsi_etalin');
vpsi_etalin_f8 = vpsi_etalin;

% Vectornav
load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run8/EXP/fig8/f8t3.mat', 'ax_vectornav');
ax_vectornav_f8 = ax_vectornav;
load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run8/EXP/fig8/f8t3.mat', 'ay_vectornav');
ay_vectornav_f8 = ay_vectornav;
load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run8/EXP/fig8/f8t3.mat', 'az_vectornav');
az_vectornav_f8 = az_vectornav;%% Flip sign to test %%

load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run8/EXP/fig8/f8t3.mat', 'vtheta_vectornav');
vtheta_vectornav_f8 = vtheta_vectornav;
load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run8/EXP/fig8/f8t3.mat', 'vphi_vectornav');
vphi_vectornav_f8 = vphi_vectornav;
load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run8/EXP/fig8/f8t3.mat', 'vpsi_vectornav');
vpsi_vectornav_f8 = vpsi_vectornav;

% -- Delta --
load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run8/EXP/fig8/f8t3.mat', 'delta');
delta_f8 = delta;
load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run8/EXP/fig8/f8t3.mat', 'brakeforce');
brake_f8 = brakeforce;
load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run8/EXP/fig8/f8t3.mat', 'throttleforce');
throttle_f8 = throttleforce;

vdelta_f8 = gradient(delta_f8)./gradient(comptime_delta_f8);

% -- Velocities --

% Etalin
load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run8/EXP/fig8/f8t3.mat', 'vx_etalin');
vx_etalin_f8 = vx_etalin;
load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run8/EXP/fig8/f8t3.mat', 'vy_etalin');
vy_etalin_f8 = vy_etalin;
load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run8/EXP/fig8/f8t3.mat', 'vz_etalin');
vz_etalin_f8 = vz_etalin;

% Novatel
load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run8/EXP/fig8/f8t3.mat', 'vx_novatel');
vx_novatel_f8 = vx_novatel;
load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run8/EXP/fig8/f8t3.mat', 'vy_novatel');
vy_novatel_f8 = vy_novatel;
load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run8/EXP/fig8/f8t3.mat', 'vz_novatel');
vz_novatel_f8 = vz_novatel;

% Septentrio
load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run8/EXP/fig8/f8t3.mat', 'vx_septentrio');
vx_septentrio_f8 = vx_septentrio;
load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run8/EXP/fig8/f8t3.mat', 'vy_septentrio');
vy_septentrio_f8 = vy_septentrio;
load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run8/EXP/fig8/f8t3.mat', 'vz_septentrio');
vz_septentrio_f8 = vz_septentrio;

% -- Wheel Encoders --

% RRN-140
load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run8/EXP/fig8/f8t3.mat', 'wheelcount_rrn140');
wheelcount_rrn140_f8 = wheelcount_rrn140;
load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run8/EXP/fig8/f8t3.mat', 'rcfvx_rrn140');
rcfvx_rrn140_f8 = rcfvx_rrn140;
load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run8/EXP/fig8/f8t3.mat', 'rcfyaw_rrn140');
rcfyaw_rrn140_f8 = rcfyaw_rrn140;

% MKZ
load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run8/EXP/fig8/f8t3.mat', 'wheelcount_mkz_ur');
wheelcount_mkz_ur_f8 = wheelcount_mkz_ur;
load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run8/EXP/fig8/f8t3.mat', 'wheelcount_mkz_ul');
wheelcount_mkz_ul_f8 = wheelcount_mkz_ul;
load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run8/EXP/fig8/f8t3.mat', 'wheelcount_mkz_lr');
wheelcount_mkz_lr_f8 = wheelcount_mkz_lr;
load('C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run8/EXP/fig8/f8t3.mat', 'wheelcount_mkz_ll');
wheelcount_mkz_ll_f8 = wheelcount_mkz_ll;


%% (2) Form Input and Target Time Series

disp('Got Past Loading Stage!')

% -- Time Syncing --

% Vectornav time will serve as our time signatures for training and testing

time0_f8 = comptime_vectornav_f8(1);

time_f8 = comptime_vectornav_f8 - time0_f8;

% Aligning time signatures with our time vector
time_imu_etalin_f8 = comptime_imu_etalin_f8 - time0_f8;

time_etalin_f8 = comptime_etalin_f8 - time0_f8;

time_delta_f8 = comptime_delta_f8 - time0_f8;

time_brake_f8 = comptime_brake_f8 - time0_f8;

time_throttle_f8 = comptime_throttle_f8 - time0_f8;

time_novatel_f8 = comptime_novatel_f8 - time0_f8;

time_rrn140_f8 = comptime_rrn140_f8 - time0_f8;

time_vehicle_f8 = comptime_vehicle_f8 - time0_f8;

time_septentrio_f8 = comptime_septentrio_f8 - time0_f8;



% RRN-140

% -- Unit Conversion --

% Raw Delta is in radians
dcf = 180 / pi;
delta0 = 0.1500; % We also need to remove the systematic error in the steer angle measurement

delta_f8 = dcf*(delta_f8 - delta0); % delta0 is in radians

% Raw Etalin Velocities are in m/s

% Raw Etalin Angular Velocities are in rad/s (?)

% Raw Etalin Accelerations are in m/s^2

% Raw Vectornav Accelerations are in m/s^2

% Raw Novatel Velocities are in m/s (ECEF)

% Raw Septentrio Velocities are in m/s (ECEF)

% Raw RRN-140 Velocity is in m/s

% Raw MKZ Wheel Velocities are in kph (?)

kphcf = 0.2778;

v_mkz_ur_f8 = kphcf*wheelcount_mkz_ur_f8;
v_mkz_ul_f8 = kphcf*wheelcount_mkz_ul_f8;
v_mkz_ll_f8 = kphcf*wheelcount_mkz_ll_f8;
v_mkz_lr_f8 = kphcf*wheelcount_mkz_lr_f8;


% -- Geometric Transformation --

% Transform Etalin Velocities / Angular Velocities / Accelerations into the vehicle frame

% Load the Transformation Matrix from Etalin to Vehicle Frame
load("C:/Users/gmm0050/Documents/MATLAB/Vehicle Simulations/Runs/Run8/EXP/gt_t3.mat", 'Rve')
%theta_ev = ;
%Rev = eul2rotm(theta_ev); % Rotation matrix from Etalin to Vehicle Frame from Ground Truth



% -- Smooth Accels and Gyros (and Etalin Velocities) --

% Low Pass Filter (Fpass = 6 Hz, Fs = 150)

Fpass = 1.00;% 1.00 % 6 % 0.35
Fstop = 1.05;% 1.05 % 7 % 0.36
Apass = 0.1;
Astop = 80;
Fs = 150;
ripple = 0.01;
filtmethod = 'lowpassiir';
filtdesignmethod = 'ellip';

h1 = designfilt(filtmethod, 'PassbandFrequency', Fpass, 'StopbandFrequency', Fstop, 'PassbandRipple', ripple, 'StopbandAttenuation', 80, 'DesignMethod', filtdesignmethod, 'SampleRate', Fs);
%Filter_lp_6hz = design(h1, 'butter', 'MatchExactly', 'passband');
d = designfilt(filtmethod, 'PassbandFrequency', Fpass, 'StopbandFrequency', Fstop, 'PassbandRipple', 0.01, 'StopbandAttenuation', 80, 'DesignMethod', filtdesignmethod, 'SampleRate', Fs);
% Low Pass Filter (Fpass = 9Hz, Fs = 150)

Fpass = 1.40;% 1.40 % 9 % 0.80
Fstop = 1.45;% 1.45 % 10 % 0.82

h2 = designfilt(filtmethod, 'PassbandFrequency', Fpass, 'StopbandFrequency', Fstop, 'PassbandRipple', ripple, 'StopbandAttenuation', 80, 'DesignMethod', filtdesignmethod, 'SampleRate', Fs);
%Filter_lp_9hz = design(h2, 'butter', 'MatchExactly', 'passband');

% Low Pass Filter (Fpass = 0.6, Fs = 10)

Fpass = 1.0; %0.6 % 0.8
Fstop = 1.1; %0.7 % 0.9
Fs = 10;

h3 = designfilt(filtmethod, 'PassbandFrequency', Fpass, 'StopbandFrequency', Fstop, 'PassbandRipple', ripple, 'StopbandAttenuation', 80, 'DesignMethod', filtdesignmethod, 'SampleRate', Fs);
%Filter_lp_06hz_vectornav = design(h3, 'butter', 'MatchExactly', 'passband');

% Low Pass Filter (Fpass = 0.6, Fs = 10)

Fpass = 0.35; %0.6
Fstop = 0.36; %0.7
Fs = 10;

h4 = designfilt(filtmethod, 'PassbandFrequency', Fpass, 'StopbandFrequency', Fstop, 'PassbandRipple', ripple, 'StopbandAttenuation', 80, 'DesignMethod', filtdesignmethod, 'SampleRate', Fs);
%Filter_lp_06hz_vectornav = design(h3, 'butter', 'MatchExactly', 'passband');

% - Filter Signals -

ax_etalin_filtered = filtfilt(h1, ax_etalin);
ay_etalin_filtered = filtfilt(h1, ay_etalin);
az_etalin_filtered = filtfilt(h1, az_etalin);

vtheta_etalin_filtered = filtfilt(h2, vtheta_etalin);
vphi_etalin_filtered = filtfilt(h1, vphi_etalin);
vpsi_etalin_filtered = filtfilt(h1, vpsi_etalin);

vx_etalin_filtered = filtfilt(h1, vx_etalin);
vy_etalin_filtered = filtfilt(h2, vy_etalin);
vz_etalin_filtered = filtfilt(h1, vz_etalin);

ax_vectornav_filtered = filtfilt(h3, ax_vectornav);
ay_vectornav_filtered = filtfilt(h3, ay_vectornav);
az_vectornav_filtered = filtfilt(h3, az_vectornav);

% Construct acceleration / velocity / angular velocity vector time series

a_etalin_f8 = [ax_etalin_filtered'; ay_etalin_filtered'; az_etalin_filtered'];
omega_etalin_f8 = [vtheta_etalin_filtered'; vphi_etalin_filtered'; vpsi_etalin_filtered'];
v_etalin_f8 = [vx_etalin_filtered'; vy_etalin_filtered'; vz_etalin_filtered'];
a_vectornav_f8 = [ax_vectornav_filtered'; ay_vectornav_filtered'; az_vectornav_filtered'];

a_etalin_orig_f8 = a_etalin_f8;
omega_etalin_orig_f8 = omega_etalin_f8;
v_etalin_orig_f8 = v_etalin_f8;

% Calculate Transformation using etalin initial accelerations

t0_init_f8 = 1;
tf_init_f8 = 1600;
init_accels_etalin_f8 = median(a_etalin_f8(:, t0_init_f8:tf_init_f8), 2);
att0_etalin_f8 = initialize_r(init_accels_etalin_f8, 5000);
att0_etalin_rad_f8 = (1/dcf)*att0_etalin_f8';
Rinit_etalin_f8 = eul2rotm(att0_etalin_rad_f8, 'XYZ'); % From vehicle\ground frame to etalin frame

% Transform Etalin accelerations into vehicle/ground frame

a_etalin_f8 = Rinit_etalin_f8 \ a_etalin_f8;

omega_etalin_f8 = Rinit_etalin_f8 \ omega_etalin_f8;

v_etalin_f8 = Rinit_etalin_f8 \ v_etalin_f8;

% Remove Gravity Vector From az_etalin

gz_etalin_f8 = median(a_etalin_f8(3, 1:1600));

a_etalin_f8(3, :) = a_etalin_f8(3, :) - gz_etalin_f8;

% Calculate alpha_f8

%omega_f8 = omega_etalin_f8; % Convert omega back to radians to get the correct units for the Rigid-Body Equation

alphax_f8 = gradient(omega_etalin_f8(1, :)) ./ gradient(time_imu_etalin_f8)';
alphay_f8 = gradient(omega_etalin_f8(2, :)) ./ gradient(time_imu_etalin_f8)';
alphaz_f8 = gradient(omega_etalin_f8(3, :)) ./ gradient(time_imu_etalin_f8)';

alphax_orig_f8 = gradient(omega_etalin_orig_f8(1, :)) ./ gradient(time_imu_etalin_f8)';
alphay_orig_f8 = gradient(omega_etalin_orig_f8(2, :)) ./ gradient(time_imu_etalin_f8)';
alphaz_orig_f8 = gradient(omega_etalin_orig_f8(3, :)) ./ gradient(time_imu_etalin_f8)';

% Interpolate Down to time_f8

ax_etalin_f8 = interp1(time_imu_etalin_f8, a_etalin_f8(1, :)', time_f8);
ay_etalin_f8 = interp1(time_imu_etalin_f8, a_etalin_f8(2, :)', time_f8);
az_etalin_f8 = interp1(time_imu_etalin_f8, a_etalin_f8(3, :)', time_f8);

a_etalin_f8 = [ax_etalin_f8'; ay_etalin_f8'; az_etalin_f8'];

ax_etalin_orig_f8 = interp1(time_imu_etalin_f8, a_etalin_orig_f8(1, :)', time_f8);
ay_etalin_orig_f8 = interp1(time_imu_etalin_f8, a_etalin_orig_f8(2, :)', time_f8);
az_etalin_orig_f8 = interp1(time_imu_etalin_f8, a_etalin_orig_f8(3, :)', time_f8);

a_etalin_orig_f8 = [ax_etalin_orig_f8'; ay_etalin_orig_f8'; az_etalin_orig_f8'];

vtheta_etalin_f8 = interp1(time_imu_etalin_f8, omega_etalin_f8(1, :)', time_f8);
vphi_etalin_f8 = interp1(time_imu_etalin_f8, omega_etalin_f8(2, :)', time_f8);
vpsi_etalin_f8 = interp1(time_imu_etalin_f8, omega_etalin_f8(3, :), time_f8);

vtheta_etalin_orig_f8 = interp1(time_imu_etalin_f8, omega_etalin_orig_f8(1, :)', time_f8);
vphi_etalin_orig_f8 = interp1(time_imu_etalin_f8, omega_etalin_orig_f8(2, :)', time_f8);
vpsi_etalin_orig_f8 = interp1(time_imu_etalin_f8, omega_etalin_orig_f8(3, :)', time_f8);

alphax_f8 = interp1(time_imu_etalin_f8, alphax_f8', time_f8);
alphay_f8 = interp1(time_imu_etalin_f8, alphay_f8', time_f8);
alphaz_f8 = interp1(time_imu_etalin_f8, alphaz_f8', time_f8);

alphax_orig_f8 = interp1(time_imu_etalin_f8, alphax_orig_f8', time_f8);
alphay_orig_f8 = interp1(time_imu_etalin_f8, alphay_orig_f8', time_f8);
alphaz_orig_f8 = interp1(time_imu_etalin_f8, alphaz_orig_f8', time_f8);

alpha_orig_f8 = [alphax_orig_f8'; alphay_orig_f8'; alphaz_orig_f8'];

% - Delta -
delta_f8 = interp1(time_delta_f8, delta_f8, time_f8);
brake_f8 = interp1(time_brake_f8, brake_f8, time_f8);
brake_f8(1) = brake_f8(2);
throttle_f8(1) = throttle_f8(2);
throttle_f8 = interp1(time_throttle_f8, throttle_f8, time_f8);
force_f8 = throttle_f8 - brake_f8 + (0.13 - throttle_f8(1));

% - Velocities -

% Etalin
vx_etalin_f8 = interp1(time_etalin_f8, v_etalin_f8(1, :)', time_f8);
vy_etalin_f8 = interp1(time_etalin_f8, v_etalin_f8(2, :)', time_f8);
vz_etalin_f8 = interp1(time_etalin_f8, v_etalin_f8(3, :)', time_f8);

v_etalin_f8 = [vx_etalin_f8'; vy_etalin_f8'; vz_etalin_f8'];

% Novatel
vx_novatel_f8 = interp1(time_novatel_f8, vx_novatel_f8, time_f8);
vy_novatel_f8 = interp1(time_novatel_f8, vy_novatel_f8, time_f8);
vz_novatel_f8 = interp1(time_novatel_f8, vz_novatel_f8, time_f8);

% Septentrio
vx_septentrio_f8 = interp1(time_septentrio_f8, vx_septentrio_f8, time_f8);
vy_septentrio_f8 = interp1(time_septentrio_f8, vy_septentrio_f8, time_f8);
vz_septentrio_f8 = interp1(time_septentrio_f8, vz_septentrio_f8, time_f8);


% Transform RRN-140 Wheel Velocity into Vehicle Frame 

%v_rrn140_f8 = Rrv * 


% -- GPS-to-Vehicle Yaw Alignment --

% Unit Conversion

vtheta_etalin_f8 = dcf*vtheta_etalin_f8;
vphi_etalin_f8 = dcf*vphi_etalin_f8;
vpsi_etalin_f8 = dcf*vpsi_etalin_f8;

omega_f8 = (1/dcf)*[vtheta_etalin_f8'; vphi_etalin_f8'; vpsi_etalin_f8'];

alpha_f8 = [alphax_f8'; alphay_f8'; alphaz_f8'];

alinx_f8 = gradient(vx_etalin_f8)./gradient(time_f8);
aliny_f8 = gradient(vy_etalin_f8)./gradient(time_f8);
alinz_f8 = gradient(vz_etalin_f8)./gradient(time_f8);

alin_f8 = [alinx_f8'; aliny_f8'; alinz_f8'];


% -- Normalization and Feature Transforms --
is_nonlinear = true;
sfx = 1000;
sfy = 1000;
sfz = 1000;
sf = [sfx; sfy; sfz];

Inputs_f8 = [delta_f8'; vx_etalin_f8'; vy_etalin_f8'; vz_etalin_f8'; vtheta_etalin_f8'; vphi_etalin_f8'; vpsi_etalin_f8'; force_f8'];

Targets_f8 = [ax_etalin_f8'; ay_etalin_f8'; az_etalin_f8'];

[U_f8, T_f8] = Normalize_Data(Inputs_f8, Targets_f8, alin_f8, time_f8', gradmethod, is_nonlinear, sf, 1, dcf, 1);


% Remove Gravity Vector from az ?


% -- Split into Training / Val / Test Sets --

t0_train_f8 = 200; % Provisional (look at delta_f8)
tf_train_f8 = 1500;

Ux_train_f8 = U_f8{1}(t0_train_f8:tf_train_f8, :);
Uy_train_f8 = U_f8{2}(t0_train_f8:tf_train_f8, :);
Uz_train_f8 = U_f8{3}(t0_train_f8:tf_train_f8, :);
Tx_train_f8 = T_f8{1}(t0_train_f8:tf_train_f8);
Ty_train_f8 = T_f8{2}(t0_train_f8:tf_train_f8);
Tz_train_f8 = T_f8{3}(t0_train_f8:tf_train_f8);

t0_val_f8 = 1200;
tf_val_f8 = 1800;

Ux_val_f8 = U_f8{1}(t0_val_f8:tf_val_f8, :);
Uy_val_f8 = U_f8{2}(t0_val_f8:tf_val_f8, :);
Uz_val_f8 = U_f8{3}(t0_val_f8:tf_val_f8, :);
Tx_val_f8 = T_f8{1}(t0_val_f8:tf_val_f8);
Ty_val_f8 = T_f8{2}(t0_val_f8:tf_val_f8);
Tz_val_f8 = T_f8{3}(t0_val_f8:tf_val_f8);

t0_test_f8 = 200;%1500
tf_test_f8 = 1800;

Ux_test_f8 = U_f8{1}(t0_test_f8:tf_test_f8, :);
Uy_test_f8 = U_f8{2}(t0_test_f8:tf_test_f8, :);
Uz_test_f8 = U_f8{3}(t0_test_f8:tf_test_f8, :);
Tx_test_f8 = T_f8{1}(t0_test_f8:tf_test_f8);
Ty_test_f8 = T_f8{2}(t0_test_f8:tf_test_f8);
Tz_test_f8 = T_f8{3}(t0_test_f8:tf_test_f8);


%% (3) Training 

disp('Got to Training Stage!')
tic

% --- Figure-8 ---
maxtrainepochsx = 2000;
maxtrainepochsy = 2000;
maxtrainepochsz = 2000;

disp('X-Training Fig-8')
[Cx_f8, Wx_f8, Bx_f8, Uxout_f8, basesx_f8, valmsex_f8, trainmsex_f8] = SARNN(validationmethod, k, v, maxtrainepochsx, Ux_val_f8, Tx_val_f8, Ux_train_f8, Tx_train_f8); %Trains the network to build xoutmat from xsmat
disp('Y-Training Fig-8')
[Cy_f8, Wy_f8, By_f8, Uyout_f8, basesy_f8, valmsey_f8, trainmsey_f8] = SARNN(validationmethod, k, v, maxtrainepochsy, Uy_val_f8, Ty_val_f8, Uy_train_f8, Ty_train_f8);
disp('Z-Training Fig-8')
[Cz_f8, Wz_f8, Bz_f8, Uzout_f8, basesz_f8, valmsez_f8, trainmsez_f8] = SARNN(validationmethod, k, v, maxtrainepochsz, Uz_val_f8, Tz_val_f8, Uz_train_f8, Tz_train_f8);

toc

C_f8 = {};
C_f8{1} = Cx_f8;
C_f8{2} = Cy_f8;
C_f8{3} = Cz_f8;

W_f8 = {};
W_f8{1} = Wx_f8;
W_f8{2} = Wy_f8;
W_f8{3} = Wz_f8;

B_f8 = {};
B_f8{1} = Bx_f8;
B_f8{2} = By_f8;
B_f8{3} = Bz_f8;

Uout_f8 = {};
Uout_f8{1} = Uxout_f8;
Uout_f8{2} = Uyout_f8;
Uout_f8{3} = Uzout_f8;
   

%% (4) Testing

disp('Got to Testing Stage!')
outclass = 1;
ndimx = 3;
ndimy = 3;
ndimz = 3;
nout = 1;

% --- Figure-8 ---
[resultx_f8, residualx_f8] = test(Wx_f8, Cx_f8, Bx_f8, Uxout_f8, Ux_test_f8, Tx_test_f8, outclass, nout, ndimx);
[resulty_f8, residualy_f8] = test(Wy_f8,Cy_f8,By_f8,Uyout_f8,Uy_test_f8, Ty_test_f8, outclass, nout, ndimy);
[resultz_f8, residualz_f8] = test(Wz_f8,Cz_f8,Bz_f8,Uzout_f8,Uz_test_f8, Tz_test_f8, outclass, nout, ndimz);
  
[resultx_train_f8, residualx_train_f8] = test(Wx_f8, Cx_f8, Bx_f8, Uxout_f8, Ux_train_f8, Tx_train_f8, outclass, nout, ndimx);
[resulty_train_f8, residualy_train_f8] = test(Wy_f8,Cy_f8,By_f8,Uyout_f8,Uy_train_f8, Ty_train_f8, outclass, nout, ndimy);
[resultz_train_f8, residualz_train_f8] = test(Wz_f8,Cz_f8,Bz_f8,Uzout_f8,Uz_train_f8, Tz_train_f8, outclass, nout, ndimz);
  


%% (5) Maximum Likelihood Search

disp('Got to Maximum Likelihood Search!')

% -- Calculate Angular Acceleration -- 

% --- Figure-8 ---

% -- Form the Rigid-Body Transformation Matrix --

H_f8 = zeros(3, 3, length(time_f8));
H_orig_f8 = zeros(3, 3, length(time_f8));

for i = 1:length(time_f8)
        
    H_f8(:, :, i) = calc_H(alpha_f8(:, i), omega_f8(:, i));
    H_orig_f8(:, :, i) = calc_H(alpha_orig_f8(:, i), omega_etalin_orig_f8(:, i));

end

H_train_f8 = H_f8(:, :, t0_train_f8:tf_train_f8);
H_test_f8 = H_f8(:, :, 20:tf_test_f8);
H_train_and_test_f8 = H_f8(:, :, 20:end-20);

H_tt_orig_f8 = H_orig_f8(:, :, 20:end-20);

% -- Form Result / Target / Input Matrices --

Yxyz_test_f8 = [Tx_test_f8'; Ty_test_f8'; Tz_test_f8'];
    
resultxyz_f8 = [resultx_f8; resulty_f8; resultz_f8];
    
Uall_f8 = zeros(length(time_f8), 4);

Uall_f8(:, 1:4) = U_f8{1}(:, 1:4); 
%Uall_f8(:, 6) = U_f8{2}(:, 5);
%Uall_f8(:, 7) = U_f8{3}(:, 5);

Uall_train_f8 = Uall_f8(t0_train_f8:tf_train_f8, :);

Uall_test_f8 = Uall_f8(t0_test_f8:tf_test_f8, :);

% -- Calculate Dist to Training Set for all epochs --

deltaUx_f8 = zeros(1, length(time_f8));
deltaUy_f8 = zeros(1, length(time_f8));
deltaUz_f8 = zeros(1, length(time_f8));
for t = 1:length(time_f8)

    [rx, ~] = findnclosest(Uxout_f8, Uall_f8(t, 1:4), 1);
    deltaUx_f8(t) = rx;
    [ry, ~] = findnclosest(Uyout_f8, [Uall_f8(t, 1:4)], 1);
    deltaUy_f8(t) = ry;
    [rz, ~] = findnclosest(Uzout_f8, [Uall_f8(t, 1:4)], 1);
    deltaUz_f8(t) = rz;

end

% -- Run Maximum Likelihood Search --

% (1) Need to write a new Likelihood wrapper function script
% (2) Need to write a new MLS script

% -- Set Parameters and Form IMU Accel Vector --

point0 = [3.5519; 0.7575; -0.4591]; % Initial Pose Estimate
psi0 = 48.9;%37.8835; Initial Yaw Estimate
s0 = [1; 1; 1; 5; 5; 5; 1; 1; 1]; % Initial Covariance in meters for lever arm (1-3) and degrees for misalignment angles (4-6)
bias0 = [-0.33; 0; 0]; % bx = -0.642 % Initial Estimate of the Bias of the Vectornav 
cp = [0; 0; 0]; % Control Point is wherever we want it to be in relation to the vehicle frame
is_fulldim = true; % Estimate all 6 DOF of the relative pose [r_iv, R_iv]
is_rot = true; % Estimate lever arm and yaw, but keep initial estimate of theta/phi fixed


bx = 0.30;
bz = 0.1178; % Presumed Bias in the Z-Acceleration of the Vectornav
Ysensorxyz_f8 = a_vectornav_f8; % + [bx; 0; 0];%- [0; 0; bz]; %[ax_vectornav_f8'; ay_vectornav_f8'; az_vectornav_f8']; % a_vectornav_f8; % Accel Readings from the IMU mounted arbitrarily on the vehicle


% -- Initialize Phi / Theta using Gravity Vector --

maxinitepochs = 5000;
init_accels_f8 = median(Ysensorxyz_f8(:, t0_init_f8:tf_init_f8), 2); % Approximate init accel with median value over standstill phase
att0 = initialize_r(init_accels_f8, maxinitepochs);
theta0 = att0(1);
phi0 = att0(2);

% Use CMA-ES if necessary
%[att0min, fatt0min] = cmaes_ga('gravity_error', [0;0;0], [2; 2; 2], [], init_accels_f8);

if is_fulldim == true
    x0 = zeros(9, 1);
    x0(1:3) = point0;
    x0(4) = theta0;
    x0(5) = phi0;
    x0(6) = psi0;
    theta = theta0;
    phi = phi0;
    sigma0 = s0;
    x0(7:9) = bias0;
else
    if is_rot == true

        x0 = zeros(7, 1);
        x0(1:3) = point0;
        x0(4) = psi0;
        theta = theta0;
        phi = phi0;
        sigma0 = [s0(1:3); s0(6); s0(7:9)];
        x0(5:7) = bias0;
    else
        x0 = zeros(6, 1);
        x0 = point0;
        theta = 0;
        phi = 0;
        psi = 0;
        sigma0 = [s0(1:3); s0(7:9)];
        x0(4:6) = bias0;
    end
end


% -- Perform Maximum Likelihood Search using CMA --
% (Using True Accelerations measured by etalin
Y_f8 = a_etalin_f8 + [0; 0; gz_etalin_f8];%a_etalin_orig_f8;%a_etalin_f8 + [0; 0; gz_etalin_f8];
Yxyz_test_f8 = Y_f8(:, 20:tf_test_f8);
Ysensorxyz_test_f8 = Ysensorxyz_f8(:, 20:tf_test_f8); 
T = length(20:tf_test_f8); % Number of test measurements used to estimate the pose of the IMU
wstatic = 4;
[xmin, fmin, counteval, stopflag, outs, bestever] = MLS(H_test_f8, Uall_train_f8, Uall_test_f8, Yxyz_test_f8, Ysensorxyz_test_f8, cp, T, is_fulldim, is_rot, wstatic, x0, sigma0, theta, phi);

r_iv_min = xmin(1:3);
th_iv_min = xmin(4:6);

% -- Form Result vectors for plotting --
axlin_test_f8 = axlin_f8(t0_test_f8:tf_test_f8);
ax_est_f8 = axlin_test_f8' + resultz_f8';
aylin_test_f8 = aylin_f8(t0_test_f8:tf_test_f8);
ay_est_f8 = aylin_test_f8' + resulty_f8';
azlin_test_f8 = azlin_f8(t0_test_f8:tf_test_f8);
az_est_f8 = azlin_test_f8' + resultz_f8';

R0 = rotmatrix(theta0, phi0, psi0);
accels_induced = zeros(size(a_etalin_f8));
for t = 1:length(time_f8)
    accels_induced(:, t) = H_f8(:, :, t)*(point0);
end
a_sensor_vehicle = a_etalin_f8 + accels_induced; % Vehicle Frame 
a_sensor_vehicle_gv = a_sensor_vehicle + [0; 0; gz_etalin_f8];
a_sensor_rotated = R0*a_sensor_vehicle_gv + bias0;
%bz = median(a_vectornav_f8(3, t0_init_f8:tf_init_f8)) - median(a_sensor_rotated(3, t0_init_f8:tf_init_f8));

R = rotmatrix(th_iv_min(1), th_iv_min(2), th_iv_min(3));
accels_ind_out = zeros(size(a_etalin_f8));
for t = 1:length(time_f8)
    accels_ind_out(:, t) = H_f8(:, :, t)*r_iv_min;
end
a_sensor_vehicle_out = a_etalin_f8 + accels_ind_out;
a_sensor_vehicle_gv_out = a_sensor_vehicle_out + [0; 0; gz_etalin_f8];
a_sensor_out = R*a_sensor_vehicle_gv_out + xmin(7:9);

a_sensor_vehicle_est = resultxyz_f8 + accels_ind_out(:, t0_test_f8:tf_test_f8);
a_sensor_vehicle_gv_est = a_sensor_vehicle_est + [0; 0; gz_etalin_f8];
a_sensor_est = R*a_sensor_vehicle_gv_est + xmin(7:9);

residualxyz_0 = a_sensor_rotated - a_vectornav_f8;
residualxyz_out = a_sensor_out - a_vectornav_f8;
residualxyz_est = a_sensor_est - a_vectornav_f8(t0_test_f8:tf_test_f8);

mse_0 = norm(a_sensor_rotated(:, 20:end-20) - a_vectornav_f8(:, 20:end-20));
mse_out = norm(a_sensor_out(:, 20:end-20) - a_vectornav_f8(:, 20:end-20));




% -- Perform Maximum Likelihood Search using CMA --
Yest_f8 = [ax_est_f8'; ay_est_f8'; az_est_f8'] + [0; 0; gz_etalin_f8];
[xest, fest, countevalest, stopflagest, outsest, besteverest] = MLS(H_test_f8, Uall_train_f8, Uall_test_f8, Yest_f8, Ysensorxyz_test_f8, cp, T, is_fulldim, is_rot, x0, sigma0, theta, phi);

r_iv_est = xest(1:3);
th_iv_est = xest(4:6);

 

%% (6) Make Plots

% ------------------ Figure-8 -------------------


%-------------------------
% (1) Raw Result vs Target with Residual
figure
sgtitle('Estimated vs Target Nonlinear Acceleration')
subplot(2, 3, 1)
hold on
plot(time_f8(t0_test_f8:tf_test_f8), Tx_test_f8, 'Color', 'k', 'LineWidth', 1.0)
plot(time_f8(t0_test_f8:tf_test_f8), resultx_f8, 'Color', 'b', 'LineWidth', 1.0)
hold off
title('X')
xlabel('Time (s)')
ylabel('Acceleration (m/s^2)')
legend('Truth', 'Estimated')

subplot(2, 3, 2)
hold on
plot(time_f8(t0_test_f8:tf_test_f8), Ty_test_f8, 'Color', 'k', 'LineWidth', 1.0)
plot(time_f8(t0_test_f8:tf_test_f8), resulty_f8, 'Color', 'b', 'LineWidth', 1.0)
hold off
title('Y')
xlabel('Time (s)')
ylabel('Acceleration (m/s^2)')
legend('Truth', 'Estimated')

subplot(2, 3, 3)
hold on
plot(time_f8(t0_test_f8:tf_test_f8), Tz_test_f8, 'Color', 'k', 'LineWidth', 1.0)
plot(time_f8(t0_test_f8:tf_test_f8), resultz_f8, 'Color', 'b', 'LineWidth', 1.0)
hold off
title('Z')
xlabel('Time (s)')
ylabel('Acceleration (m/s^2)')
legend('Truth', 'Estimated')

subplot(2, 3, 4)
plot(time_f8(t0_test_f8:tf_test_f8), residualx_f8, 'Color', 'k')
title('Ax Residual')
xlabel('Time (s)')
ylabel('Residual Error (m/s^2)')

subplot(2, 3, 5)
plot(time_f8(t0_test_f8:tf_test_f8), residualy_f8, 'Color', 'k')
title('Ay Residual')
xlabel('Time (s)')
ylabel('Residual Error (m/s^2)')

subplot(2, 3, 6)
plot(time_f8(t0_test_f8:tf_test_f8), residualz_f8, 'Color', 'k')
title('Az Residual')
xlabel('Time (s)')
ylabel('Residual Error (m/s^2)')


% (2) Result vs Truth (Etalin)
figure

axlin_test_f8 = axlin_f8(t0_test_f8:tf_test_f8);
ax_est_f8 = axlin_test_f8' + resultx_f8';
aylin_test_f8 = aylin_f8(t0_test_f8:tf_test_f8);
ay_est_f8 = aylin_test_f8' + resulty_f8';
azlin_test_f8 = azlin_f8(t0_test_f8:tf_test_f8);
az_est_f8 = azlin_test_f8' + resultz_f8';

sgtitle('Estimated vs Ground Truth Accelerations')
subplot(2, 3, 1)
hold on
plot(time_f8(t0_test_f8:tf_train_f8), ax_etalin_f8(t0_test_f8:tf_train_f8), 'Color', 'k', "LineWidth", 1.00)
plot(time_f8(t0_test_f8:tf_train_f8), ax_est_f8(1:end-300), 'Color', 'b', "LineWidth", 1.00)
hold off
title('X')
xlabel('Time (s)')
ylabel('Acceleration (m/s^2)')
legend('Truth', 'Estimated')

subplot(2, 3, 2)
hold on
plot(time_f8(t0_test_f8:tf_train_f8), ay_etalin_f8(t0_test_f8:tf_train_f8), 'Color', 'k', "LineWidth", 1.00)
plot(time_f8(t0_test_f8:tf_train_f8), ay_est_f8(1:end-300), 'Color', 'b', "LineWidth", 1.00 )
hold off
title('Y')
xlabel('Time (s)')
ylabel('Acceleration (m/s^2)')
legend('Truth', 'Estimated')

subplot(2, 3, 3)
hold on
plot(time_f8(t0_test_f8:tf_train_f8), a_etalin_f8(3, t0_test_f8:tf_train_f8)', 'Color', 'k', "LineWidth", 1.00)
plot(time_f8(t0_test_f8:tf_train_f8), az_est_f8(1:end-300), 'Color', 'b', "LineWidth", 1.00 )
hold off
title('Z')
xlabel('Time (s)')
ylabel('Acceleration (m/s^2)')
legend('Truth', 'Estimated')

subplot(2, 3, 4)
plot(time_f8(t0_test_f8:tf_train_f8), residualx_f8(1:end-300), 'k')
title('Ax Residual')
xlabel('Time (s)')
ylabel('Estimation Error (m/s^2)')

subplot(2, 3, 5)
plot(time_f8(t0_test_f8:tf_train_f8), residualy_f8(1:end-300), 'k')
title('Ay Residual')
xlabel('Time (s)')
ylabel('Estimation Error (m/s^2)')

subplot(2, 3, 6)
plot(time_f8(t0_test_f8:tf_train_f8), residualz_f8(1:end-300), 'k')
title('Az Residual')
xlabel('Time (s)')
ylabel('Estimation Error (m/s^2)')


% (3) Result vs Truth (Etalin) with Distance from Uout

% (4) Scatter Plots of Residual vs Distance
figure
sgtitle('Distribution of Residuals According to Distance from Training Set')
subplot(1, 3, 1)
hold on
scatter(deltaUx_f8(t0_test_f8:tf_test_f8), residualx_f8', 'MarkerEdgeColor', 'k')
hold off
title('X')
xlabel('Distance from Training Set')
ylabel('Residual Error')

subplot(1, 3, 2)
hold on
scatter(deltaUy_f8(t0_test_f8:tf_test_f8), residualy_f8', 'MarkerEdgeColor', 'k')
hold off
title('Y')
xlabel('Distance from Training Set')
ylabel('Residual Error')

subplot(1, 3, 3)
hold on
scatter(deltaUz_f8(t0_test_f8:tf_test_f8), residualz_f8', 'MarkerEdgeColor', 'k')
hold off
title('Z')
xlabel('Distance from Training Set')
ylabel('Residual Error')



% (5) Validation vs Training Error
figure
sgtitle('Validation vs Training Error')
subplot(1, 3, 1)
hold on
plot(5*(1:length(valmsex_f8)), trainmsex_f8, 'Color', 'k', 'LineWidth', 1.0)
plot(5*(1:length(trainmsex_f8)), valmsex_f8, 'Color', 'b', 'LineWidth', 1.0)
hold off
title('X')
xlabel('Training Epoch')
ylabel('MSE')
legend('Training Error', 'Validation Error')

subplot(1, 3, 2)
hold on
plot(5*(1:length(valmsey_f8)), trainmsey_f8, 'Color', 'k', 'LineWidth', 1.0)
plot(5*(1:length(trainmsey_f8)), valmsey_f8, 'Color', 'b', 'LineWidth', 1.0)
hold off
title('Y')
xlabel('Training Epoch')
ylabel('MSE')
legend('Training Error', 'Validation Error')

subplot(1, 3,  3)
hold on
plot(5*(1:length(valmsez_f8)), trainmsez_f8, 'Color', 'k', 'LineWidth', 1.0)
plot(5*(1:length(trainmsez_f8)), valmsez_f8, 'Color', 'b', 'LineWidth', 1.0)
hold off
title('Z')
xlabel('Training Epoch')
ylabel('MSE')
legend('Training Error', 'Validation Error')


% (5.1) Validation vs Training (X)
figure
hold on
plot(5*(1:length(valmsex_f8)), trainmsex_f8, 'Color', 'k', 'LineWidth', 1.0)
plot(5*(1:length(trainmsex_f8)), valmsex_f8, 'Color', 'b', 'LineWidth', 1.0)
hold off
title('X-Acceleration Validation vs Training Error')
xlabel('Training Epoch')
ylabel('MSE')
legend('Training Error', 'Validation Error')

% (5.2) Validation vs Training (Y)
figure
hold on
plot(5*(1:length(valmsey_f8)), trainmsey_f8, 'Color', 'k', 'LineWidth', 1.0)
plot(5*(1:length(trainmsey_f8)), valmsey_f8, 'Color', 'b', 'LineWidth', 1.0)
hold off
title('Y-Acceleration Validation vs Training Error')
xlabel('Training Epoch')
ylabel('MSE')
legend('Training Error', 'Validation Error')

% (5.3) Validation vs Training (Z)
figure
hold on
plot(5*(1:length(valmsez_f8)), trainmsez_f8, 'Color', 'k', 'LineWidth', 1.0)
plot(5*(1:length(trainmsez_f8)), valmsez_f8, 'Color', 'b', 'LineWidth', 1.0)
hold off
title('Z-Acceleration Validation vs Training Error')
xlabel('Training Epoch')
ylabel('MSE')
legend('Training Error', 'Validation Error')

% (6) Accelerations Induced in the Vehicle Frame

% (6.1) Induced Accels (All)
figure
hold on
plot(time_f8(20:end-20), accels_ind_out(:, 20:end-20), 'LineWidth', 1.00)
hold off
title('Accelerations Induced by Lever Arm in the Vehicle Frame')
xlabel('Time (s)')
ylabel('Acceleration (m/s^2)')
legend('Ax', 'Ay', 'Az')

% (7) Fourier Transforms Raw vs Filtered

% (7.1) Accels Etalin
L = 37072; %2400;
Fs = 150;% 10;

figure
sgtitle('Spectral Density of Raw vs Filtered Ground Truth Accelerations')
Y1 = fft(ax_etalin(1:L));
P2 = abs(Y1/L);
P1 = P2(1:(L/2+1));
P1(2:end-1) = 2*P1(2:end-1);
f = Fs*(0:(L/2))/L;

Y2 = fft(ax_etalin_filtered(1:L));
P4 = abs(Y2/L);
P3 = P4(1:(L/2+1));
P3(2:end-1) = 2*P3(2:end-1);
%f = Fs*(0:(L/2))/L;

subplot(2, 3, 1)
hold on
plot(f(2:end-1), P1(2:end-1), 'Color', 'k', 'LineWidth', 0.75)
hold off
title('Raw Signal (X)')
xlabel('Frequency (Hz)')
ylabel('Power Spectral Density')

subplot(2, 3, 4)
hold on
plot(f(2:end-1), P3(2:end-1), 'Color', 'k', 'LineWidth', 0.75)
hold off
title('Filtered Signal (X)')
xlabel('Frequency (Hz)')
ylabel('Power Spectral Density')

Y1 = fft(ay_etalin(1:L));
P2 = abs(Y1/L);
P1 = P2(1:(L/2+1));
P1(2:end-1) = 2*P1(2:end-1);
f = Fs*(0:(L/2))/L;

Y2 = fft(ay_etalin_filtered(1:L));
P4 = abs(Y2/L);
P3 = P4(1:(L/2+1));
P3(2:end-1) = 2*P3(2:end-1);
%f = Fs*(0:(L/2))/L;

subplot(2, 3, 2)
hold on
plot(f(2:end-1), P1(2:end-1), 'Color', 'k', 'LineWidth', 0.75)
hold off
title('Raw Signal (Y)')
xlabel('Frequency (Hz)')
ylabel('Power Spectral Density')

subplot(2, 3, 5)
hold on
plot(f(2:end-1), P3(2:end-1), 'Color', 'k', 'LineWidth', 0.75)
hold off
title('Filtered Signal (Y)')
xlabel('Frequency (Hz)')
ylabel('Power Spectral Density')

Y1 = fft(az_etalin(1:L));
P2 = abs(Y1/L);
P1 = P2(1:(L/2+1));
P1(2:end-1) = 2*P1(2:end-1);
f = Fs*(0:(L/2))/L;

Y2 = fft(az_etalin_filtered(1:L));
P4 = abs(Y2/L);
P3 = P4(1:(L/2+1));
P3(2:end-1) = 2*P3(2:end-1);
%f = Fs*(0:(L/2))/L;

subplot(2, 3, 3)
hold on
plot(f(2:end-1), P1(2:end-1), 'Color', 'k', 'LineWidth', 0.75)
hold off
title('Raw Signal (Z)')
xlabel('Frequency (Hz)')
ylabel('Power Spectral Density')

subplot(2, 3, 6)
hold on
plot(f(2:end-1), P3(2:end-1), 'Color', 'k', 'LineWidth', 0.75)
hold off
title('Filtered Signal (Z)')
xlabel('Frequency (Hz)')
ylabel('Power Spectral Density')



% (7.2) Gyros Etalin
figure 
sgtitle('Spectral Density of Raw vs Filtered Ground Truth Angular Velocites')
Y1 = fft(vtheta_etalin(1:L));
P2 = abs(Y1/L);
P1 = P2(1:(L/2+1));
P1(2:end-1) = 2*P1(2:end-1);
f = Fs*(0:(L/2))/L;

Y2 = fft(vtheta_etalin_filtered(1:L));
P4 = abs(Y2/L);
P3 = P4(1:(L/2+1));
P3(2:end-1) = 2*P3(2:end-1);
%f = Fs*(0:(L/2))/L;

subplot(2, 3, 1)
hold on
plot(f(2:end-1), P1(2:end-1), 'Color', 'k', 'LineWidth', 0.75)
hold off
title('Raw Signal (X)')
xlabel('Frequency (Hz)')
ylabel('Power Spectral Density')

subplot(2, 3, 4)
hold on
plot(f(2:end-1), P3(2:end-1), 'Color', 'k', 'LineWidth', 0.75)
hold off
title('Filtered Signal (X)')
xlabel('Frequency (Hz)')
ylabel('Power Spectral Density')


Y1 = fft(vphi_etalin(1:L));
P2 = abs(Y1/L);
P1 = P2(1:(L/2+1));
P1(2:end-1) = 2*P1(2:end-1);
f = Fs*(0:(L/2))/L;

Y2 = fft(vphi_etalin_filtered(1:L));
P4 = abs(Y2/L);
P3 = P4(1:(L/2+1));
P3(2:end-1) = 2*P3(2:end-1);
%f = Fs*(0:(L/2))/L;

subplot(2, 3, 2)
hold on
plot(f(2:end-1), P1(2:end-1), 'Color', 'k', 'LineWidth', 0.75)
hold off
title('Raw Signal (Y)')
xlabel('Frequency (Hz)')
ylabel('Power Spectral Density')

subplot(2, 3, 5)
hold on
plot(f(2:end-1), P3(2:end-1), 'Color', 'k', 'LineWidth', 0.75)
hold off
title('Filtered Signal (Y)')
xlabel('Frequency (Hz)')
ylabel('Power Spectral Density')


Y1 = fft(vpsi_etalin(1:L));
P2 = abs(Y1/L);
P1 = P2(1:(L/2+1));
P1(2:end-1) = 2*P1(2:end-1);
f = Fs*(0:(L/2))/L;

Y2 = fft(vpsi_etalin_filtered(1:L));
P4 = abs(Y2/L);
P3 = P4(1:(L/2+1));
P3(2:end-1) = 2*P3(2:end-1);
%f = Fs*(0:(L/2))/L;

subplot(2, 3, 3)
hold on
plot(f(2:end-1), P1(2:end-1), 'Color', 'k', 'LineWidth', 0.75)
hold off
title('Raw Signal (Z)')
xlabel('Frequency (Hz)')
ylabel('Power Spectral Density')

subplot(2, 3, 6)
hold on
plot(f(2:end-1), P3(2:end-1), 'Color', 'k', 'LineWidth', 0.75)
hold off
title('Filtered Signal (Z)')
xlabel('Frequency (Hz)')
ylabel('Power Spectral Density')


% (7.3) Accels Vectornav

L = 2400;
Fs = 10;
figure
sgtitle('Spectral Density of Raw vs Filtered Measured Accelerations')
Y1 = fft(ax_vectornav(1:L));
P2 = abs(Y1/L);
P1 = P2(1:(L/2+1));
P1(2:end-1) = 2*P1(2:end-1);
f = Fs*(0:(L/2))/L;

Y2 = fft(ax_vectornav_filtered(1:L));
P4 = abs(Y2/L);
P3 = P4(1:(L/2+1));
P3(2:end-1) = 2*P3(2:end-1);
%f = Fs*(0:(L/2))/L;

subplot(2, 3, 1)
hold on
plot(f(2:end-1), P1(2:end-1),  'Color', 'k', 'LineWidth', 0.75)
hold off
title('Raw Signal (X)')
xlabel('Frequency (Hz)')
ylabel('Power Spectral Density')

subplot(2, 3, 4)
hold on
plot(f(2:end-1), P3(2:end-1),  'Color', 'k', 'LineWidth', 0.75)
hold off
title('Filtered Signal (X)')
xlabel('Frequency (Hz)')
ylabel('Power Spectral Density')

Y1 = fft(ay_vectornav(1:L));
P2 = abs(Y1/L);
P1 = P2(1:(L/2+1));
P1(2:end-1) = 2*P1(2:end-1);
f = Fs*(0:(L/2))/L;

Y2 = fft(ay_vectornav_filtered(1:L));
P4 = abs(Y2/L);
P3 = P4(1:(L/2+1));
P3(2:end-1) = 2*P3(2:end-1);
%f = Fs*(0:(L/2))/L;

subplot(2, 3, 2)
hold on
plot(f(2:end-1), P1(2:end-1),  'Color', 'k', 'LineWidth', 0.75)
hold off
title('Raw Signal (Y)')
xlabel('Frequency (Hz)')
ylabel('Power Spectral Density')

subplot(2, 3, 5)
hold on
plot(f(2:end-1), P3(2:end-1),  'Color', 'k', 'LineWidth', 0.75)
hold off
title('Filtered Signal (Y)')
xlabel('Frequency (Hz)')
ylabel('Power Spectral Density')

Y1 = fft(az_vectornav(1:L));
P2 = abs(Y1/L);
P1 = P2(1:(L/2+1));
P1(2:end-1) = 2*P1(2:end-1);
f = Fs*(0:(L/2))/L;

Y2 = fft(az_vectornav_filtered(1:L));
P4 = abs(Y2/L);
P3 = P4(1:(L/2+1));
P3(2:end-1) = 2*P3(2:end-1);
%f = Fs*(0:(L/2))/L;

subplot(2, 3, 3)
hold on
plot(f(2:end-1), P1(2:end-1), 'Color', 'k', 'LineWidth', 0.75)
hold off
title('Raw Signal (Z)')
xlabel('Frequency (Hz)')
ylabel('Power Spectral Density')

subplot(2, 3, 6)
hold on
plot(f(2:end-1), P3(2:end-1), 'Color', 'k', 'LineWidth', 0.75)
hold off
title('Filtered Signal (Z)')
xlabel('Frequency (Hz)')
ylabel('Power Spectral Density')



% (8) Raw vs Filtered Signals in Time Domain

% (8.1) Accels Etalin
figure
sgtitle('Raw vs Filtered Ground Truth Accelerations')

subplot(1, 3, 1)
hold on
plot(time_imu_etalin_f8(100:end-100), ax_etalin(100:end-100))
plot(time_imu_etalin_f8(100:end-100), ax_etalin_filtered(100:end-100), 'Color', 'k', 'LineWidth', 1.0)
hold off
title('X')
xlabel('Time (s)')
ylabel('Acceleration (m/s^2)')
legend('Raw', 'Filtered')

subplot(1, 3, 2)
hold on
plot(time_imu_etalin_f8(100:end-100), ay_etalin(100:end-100))
plot(time_imu_etalin_f8(100:end-100), ay_etalin_filtered(100:end-100), 'Color', 'k', 'LineWidth', 1.0)
hold off
title('Y')
xlabel('Time (s)')
ylabel('Acceleration (m/s^2)')
legend('Raw', 'Filtered')

subplot(1, 3, 3)
hold on
plot(time_imu_etalin_f8(100:end-100), az_etalin(100:end-100))
plot(time_imu_etalin_f8(100:end-100), az_etalin_filtered(100:end-100), 'Color', 'k', 'LineWidth', 1.0)
hold off
title('Z')
xlabel('Time (s)')
ylabel('Acceleration (m/s^2)')
legend('Raw', 'Filtered')

% (8.1.1) (X)
figure
hold on
plot(time_imu_etalin_f8(100:end-100), ax_etalin(100:end-100))
plot(time_imu_etalin_f8(100:end-100), ax_etalin_filtered(100:end-100), 'Color', 'k', 'LineWidth', 1.0)
hold off
title('Raw vs Filtered X-Acceleration')
xlabel('Time (s)')
ylabel('Acceleration (m/s^2)')
legend('Raw', 'Filtered')

% (8.1.2) (Y)
figure
hold on
plot(time_imu_etalin_f8(100:end-100), ay_etalin(100:end-100))
plot(time_imu_etalin_f8(100:end-100), ay_etalin_filtered(100:end-100), 'Color', 'k', 'LineWidth', 1.0)
hold off
title('Raw vs Filtered Y-Acceleration')
xlabel('Time (s)')
ylabel('Acceleration (m/s^2)')
legend('Raw', 'Filtered')

% (8.1.3) (Z)
figure
hold on
plot(time_imu_etalin_f8(100:end-100), az_etalin(100:end-100))
plot(time_imu_etalin_f8(100:end-100), az_etalin_filtered(100:end-100), 'Color', 'k', 'LineWidth', 1.0)
hold off
title('Raw vs Filtered Z-Acceleration')
xlabel('Time (s)')
ylabel('Acceleration (m/s^2)')
legend('Raw', 'Filtered')

% (8.2) Gyros Etalin
figure
sgtitle('Raw vs Filtered Angular Velocities in Vehicle Frame')

subplot(1, 3, 1)
hold on
plot(time_imu_etalin_f8(100:end-100), vtheta_etalin(100:end-100))
plot(time_imu_etalin_f8(100:end-100), vtheta_etalin_filtered(100:end-100), 'Color', 'k', 'LineWidth', 1.0)
hold off
title('Roll (X)')
xlabel('Time (s)')
ylabel('Roll Rate (rad/s)')
legend('Raw', 'Filtered')

subplot(1, 3, 2)
hold on
plot(time_imu_etalin_f8(100:end-100), vphi_etalin(100:end-100))
plot(time_imu_etalin_f8(100:end-100), vphi_etalin_filtered(100:end-100), 'Color', 'k', 'LineWidth', 1.0)
hold off
title('Pitch (Y)')
xlabel('Time (s)')
ylabel('Pitch Rate (rad/s)')
legend('Raw', 'Filtered')

subplot(1, 3, 3)
hold on
plot(time_imu_etalin_f8(100:end-100), vpsi_etalin(100:end-100))
plot(time_imu_etalin_f8(100:end-100), vpsi_etalin_filtered(100:end-100), 'Color', 'k', 'LineWidth', 1.0)
hold off
title('Yaw (Z)')
xlabel('Time (s)')
ylabel('Yaw Rate (rad/s)')
legend('Raw', 'Filtered')


% (8.3) Accels Vectornav
figure
sgtitle('Raw vs Filtered Accelerations in Sensor Frame')
subplot(1, 3, 1)
hold on
plot(time_f8(20:end-20), ax_vectornav(20:end-20))
plot(time_f8(20:end-20), ax_vectornav_filtered(20:end-20), 'Color', 'k', 'LineWidth', 1.0)
hold off
title('X')
xlabel('Time (s)')
ylabel('Acceleration (m/s^2)')
legend('Raw', 'Filtered')

subplot(1, 3, 2)
hold on
plot(time_f8(20:end-20), ay_vectornav(20:end-20))
plot(time_f8(20:end-20), ay_vectornav_filtered(20:end-20), 'Color', 'k', 'LineWidth', 1.0)
hold off
title('Y')
xlabel('Time (s)')
ylabel('Acceleration (m/s^2)')
legend('Raw', 'Filtered')

subplot(1, 3, 3)
hold on
plot(time_f8(20:end-20), az_vectornav(20:end-20))
plot(time_f8(20:end-20), az_vectornav_filtered(20:end-20), 'Color', 'k', 'LineWidth', 1.0)
hold off
title('Z')
xlabel('Time (s)')
ylabel('Acceleration (m/s^2)')
legend('Raw', 'Filtered')



% (9) Transformed Accels vs Measured Accels with Residuals

% (9.1) All Transformed vs All Measured
figure
subplot(2, 1, 1)
hold on
plot(time_f8(20:end-20), a_sensor_out(:, 20:end-20), 'LineWidth', 1.00)
hold off
title('Accelerations Transformed from Vehicle Frame into the Sensor Frame')
xlabel('Time (s)')
ylabel('Acceleration (m/s^2)')
legend('Ax', 'Ay', 'Az')
subplot(2, 1, 2)
hold on
plot(time_f8(20:end-20), a_vectornav_f8(:, 20:end-20), 'LineWidth', 1.00);
hold off
title('Accelerations Measured in the Sensor Frame')
xlabel('Time (s)')
ylabel('Acceleration (m/s^2)')
legend('Ax', 'Ay', 'Az')

% (9.2) Transformed Etalin Accels vs Sensor Measurements in Sensor Frame (X/Y/Z)
figure
sgtitle('Transformed vs Measured Accelerations in Sensor Frame')

subplot(2, 3, 1)
hold on
plot(time_f8(20:end-20), a_vectornav_f8(1, 20:end-20), 'Color', 'k', 'LineWidth', 0.75);
plot(time_f8(20:end-20), a_sensor_out(1, 20:end-20), 'Color', [0.9500 0.2750 0.0980], 'LineWidth', 1.00)
hold off
title('Ax')
xlabel('Time (s)')
ylabel('Acceleration (m/s^2)')
legend('Measured', 'Transformed')

subplot(2, 3, 4)
hold on
plot(time_f8(20:end-20), residualxyz_out(1, 20:end-20), 'Color', 'k')
hold off
title('Ax Residual')
xlabel('Time (s)')
ylabel('Residual Error (m/s^2)')

subplot(2, 3, 2)
hold on
plot(time_f8(20:end-20), a_vectornav_f8(2, 20:end-20), 'Color', 'k', 'LineWidth', 0.75);
plot(time_f8(20:end-20), a_sensor_out(2, 20:end-20), 'Color', [0.9500 0.2750 0.0980], 'LineWidth', 1.00) %[0.85, 0.325, 0.098]
hold off
title('Ay')
xlabel('Time (s)')
ylabel('Acceleration (m/s^2)')
legend('Measured', 'Transformed')

subplot(2, 3, 5)
hold on
plot(time_f8(20:end-20), residualxyz_out(2, 20:end-20), 'Color', 'k')
hold off
title('Ay Residual')
xlabel('Time (s)')
ylabel('Residual Error (m/s^2)')

subplot(2, 3, 3)
hold on
plot(time_f8(20:end-20), a_vectornav_f8(3, 20:end-20), 'Color', 'k', 'LineWidth', 0.75);
plot(time_f8(20:end-20), a_sensor_out(3, 20:end-20), 'Color', [0.9500 0.2750 0.0980], 'LineWidth', 1.00)
hold off
title('Az')
xlabel('Time (s)')
ylabel('Acceleration (m/s^2)')
legend('Measured', 'Transformed')

subplot(2, 3, 6)
hold on
plot(time_f8(20:end-20), residualxyz_out(3, 20:end-20), 'Color', 'k')
hold off
title('Az Residual')
xlabel('Time (s)')
ylabel('Residual Error (m/s^2)')



% (10) PSD of Induced Accels in the Vehicle Frame
L = 2406;
Fs = 10;
% (10.1) PSD of Induced Accels (X)
figure 
sgtitle('Spectral Density of Induced Accelerations')
Y1 = fft(accels_ind_out(1, 1:L));
P2 = abs(Y1/L);
P1 = P2(1:(L/2+1));
P1(2:end-1) = 2*P1(2:end-1);
f = Fs*(0:(L/2))/L;

subplot(3, 1, 1)
hold on
plot(f(2:end-1), P1(2:end-1), 'Color', 'k', 'LineWidth', 0.75)
hold off
title('Induced X-Acceleration in the Vehicle Frame')
xlabel('Frequency (Hz)')
ylabel('Power Spectral Density')
 

Y1 = fft(accels_ind_out(2, 1:L));
P2 = abs(Y1/L);
P1 = P2(1:(L/2+1));
P1(2:end-1) = 2*P1(2:end-1);
f = Fs*(0:(L/2))/L;

subplot(3, 1, 2)
hold on
plot(f(2:end-1), P1(2:end-1), 'Color', 'k', 'LineWidth', 0.75)
hold off
title('Induced Y-Acceleration in the Vehicle Frame')
xlabel('Frequency (Hz)')
ylabel('Power Spectral Density')
 

Y1 = fft(accels_ind_out(3, 1:L));
P2 = abs(Y1/L);
P1 = P2(1:(L/2+1));
P1(2:end-1) = 2*P1(2:end-1);
f = Fs*(0:(L/2))/L;

subplot(3, 1, 3)
hold on
plot(f(2:end-1), P1(2:end-1), 'Color', 'k', 'LineWidth', 0.75)
hold off
title('Induced Z-Acceleration in the Vehicle Frame')
xlabel('Frequency (Hz)')
ylabel('Power Spectral Density')


% (11) PSD of Transformed Accel Residual

figure 
sgtitle('Spectral Density of Transformed Ground Truth Acceleration Residual')
Y1 = fft(residualxyz_out(1, 20:end-20));
P2 = abs(Y1/L);
P1 = P2(1:(L/2+1));
P1(2:end-1) = 2*P1(2:end-1);
f = Fs*(0:(L/2))/L;

subplot(3, 1, 1)
hold on
plot(f(2:end-1), P1(2:end-1), 'Color', 'k', 'LineWidth', 0.75)
hold off
title('Sensor Frame X-Acceleration Residual')
xlabel('Frequency (Hz)')
ylabel('Power Spectral Density')

Y1 = fft(residualxyz_out(2, 20:end-20));
P2 = abs(Y1/L);
P1 = P2(1:(L/2+1));
P1(2:end-1) = 2*P1(2:end-1);
f = Fs*(0:(L/2))/L;

subplot(3, 1, 2)
hold on
plot(f(2:end-1), P1(2:end-1), 'Color', 'k', 'LineWidth', 0.75)
hold off
title('Sensor Frame Y-Acceleration Residual')
xlabel('Frequency (Hz)')
ylabel('Power Spectral Density')

subplot(3, 1, 3)
Y1 = fft(residualxyz_out(3, 20:end-20));
P2 = abs(Y1/L);
P1 = P2(1:(L/2+1));
P1(2:end-1) = 2*P1(2:end-1);
f = Fs*(0:(L/2))/L;

hold on
plot(f(2:end-1), P1(2:end-1), 'Color', 'k', 'LineWidth', 0.75)
hold off
title('Sensor Frame Z-Acceleration Residual')
xlabel('Frequency (Hz)')
ylabel('Power Spectral Density')


% (12) PSD of Transformed Result Residual
L = 300;
Fs = 10;
% (12.1) PSD of Transformed Result Residual (X)
figure 
sgtitle('Spectral Density of Transformed Estimated Acceleration Residual')
Y1 = fft(residualxyz_est(1, 1:L));
P2 = abs(Y1/L);
P1 = P2(1:(L/2+1));
P1(2:end-1) = 2*P1(2:end-1);
f = Fs*(0:(L/2))/L;

subplot(3, 1, 1)
hold on
plot(f(2:end-1), P1(2:end-1), 'Color', 'k', 'LineWidth', 0.75)
hold off
title('Sensor Frame X-Acceleration Residual')
xlabel('Frequency (Hz)')
ylabel('Power Spectral Density')

subplot(3, 1, 2)
Y1 = fft(residualxyz_est(2, 1:L));
P2 = abs(Y1/L);
P1 = P2(1:(L/2+1));
P1(2:end-1) = 2*P1(2:end-1);
f = Fs*(0:(L/2))/L;

hold on
plot(f(2:end-1), P1(2:end-1), 'Color', 'k', 'LineWidth', 0.75)
hold off
title('Sensor Frame Y-Acceleration Residual')
xlabel('Frequency (Hz)')
ylabel('Power Spectral Density')

subplot(3, 1, 3)
Y1 = fft(residualxyz_est(3, 1:L));
P2 = abs(Y1/L);
P1 = P2(1:(L/2+1));
P1(2:end-1) = 2*P1(2:end-1);
f = Fs*(0:(L/2))/L;

hold on
plot(f(2:end-1), P1(2:end-1), 'Color', 'k', 'LineWidth', 0.75)
hold off
title('Sensor Frame Z-Acceleration Residual')
xlabel('Frequency (Hz)')
ylabel('Power Spectral Density')


% (13) Raw Inputs (delta/vx/vy/vz/vtheta/vphi/vpsi)

figure
sgtitle('Raw Input Time Series - Figure-8 Dataset')
grid on
subplot(4, 2, 1)
plot(time_f8(20:end-20), Inputs_f8(1, 20:end-20), 'Color', 'k', 'LineWidth', 1.0)
xlabel('Time (s)')
ylabel('Steer Angle (deg)')
        
grid on
subplot(4, 2, 2)
plot(time_f8(20:end-20), Inputs_f8(2, 20:end-20), 'Color', 'k', 'LineWidth', 1.0)
xlabel('Time (s)')
ylabel('X-Velocity (m/s)')
        
grid on
subplot(4, 2, 3)
plot(time_f8(20:end-20), Inputs_f8(3, 20:end-20), 'Color', 'k', 'LineWidth', 1.0)
xlabel('Time (s)')
ylabel('Y-Velocity (m/s)')
        
grid on
subplot(4, 2, 4)
plot(time_f8(20:end-20), Inputs_f8(4, 20:end-20), 'Color', 'k', 'LineWidth', 1.0)
xlabel('Time (s)')
ylabel('Z-Velocity (m/s)')
        
grid on
subplot(4, 2, 5)
plot(time_f8(20:end-20), Inputs_f8(5, 20:end-20), 'Color', 'k', 'LineWidth', 1.0)
xlabel('Time (s)')
ylabel('Pitch Angular Velocity (deg/s)')
        
grid on
subplot(4, 2, 6)
plot(time_f8(20:end-20), Inputs_f8(6, 20:end-20), 'Color', 'k', 'LineWidth', 1.0)
xlabel('Time (s)')
ylabel('Roll Angular Velocity (deg/s)')
        
grid on
subplot(4, 2, 7)
plot(time_f8(20:end-20), Inputs_f8(7, 20:end-20), 'Color', 'k', 'LineWidth', 1.0)
xlabel('Time (s)')
ylabel('Yaw Angular Velocity (deg/s)')
        
grid on

% (14) Normalized Inputs 

figure
sgtitle('Normalized Input Time Series - Figure-8 Dataset')
grid on
subplot(3, 1, 1)
plot(time_f8(t0_test_f8:tf_test_f8), D, 'Color', 'k', 'LineWidth', 1.0)
xlabel('Time (s)')
ylabel('Steer Angle')
        
grid on
subplot(3, 1, 2)
plot(time_f8(t0_test_f8:tf_test_f8), V, 'Color', 'k', 'LineWidth', 1.0)
xlabel('Time (s)')
ylabel('Velocity Magnitude (m/s)')
        
grid on
subplot(3, 1, 3)
plot(time_f8(t0_test_f8:tf_test_f8), K, 'Color', 'k', 'LineWidth', 1.0)
xlabel('Time (s)')
ylabel('Longitudinal Slip (%)')
        


% (15) Targets (Ax/Ay/Az)

figure
sgtitle('Target Time Series - Figure-8 Dataset')
grid on
subplot(3, 1, 1)
plot(time_f8(20:end-20), T_f8{1}(20:end-20)', 'Color', 'k', 'LineWidth', 1.0)
xlabel('Time (s)')
ylabel('X-Acceleration (m/s^2)')

grid on
subplot(3, 1, 2)
plot(time_f8(20:end-20), T_f8{2}(20:end-20)', 'Color', 'k', 'LineWidth', 1.0)
xlabel('Time (s)')
ylabel('Y-Acceleration (m/s^2)')

grid on
subplot(3, 1, 3)
plot(time_f8(20:end-20), T_f8{3}(20:end-20)', 'Color', 'k', 'LineWidth', 1.0)
xlabel('Time (s)')
ylabel('Z-Acceleration (m/s^2)')

% (15.2) Truth 
figure
sgtitle('Target Time Series - Figure-8 Dataset')
grid on
subplot(3, 1, 1)
plot(time_f8(20:end-20), a_etalin_f8(1, 20:end-20)', 'Color', 'k', 'LineWidth', 1.0)
xlabel('Time (s)')
ylabel('X-Acceleration (m/s^2)')

grid on
subplot(3, 1, 2)
plot(time_f8(20:end-20), a_etalin_f8(2, 20:end-20)', 'Color', 'k', 'LineWidth', 1.0)
xlabel('Time (s)')
ylabel('Y-Acceleration (m/s^2)')

grid on
subplot(3, 1, 3)
plot(time_f8(20:end-20), a_etalin_f8(3, 20:end-20)', 'Color', 'k', 'LineWidth', 1.0)
xlabel('Time (s)')
ylabel('Z-Acceleration (m/s^2)')



% (16) Pose Estimation Error vs Std with Error Bars

% (16.1) Pose Estimation Error (X) vs Standard Deviation of Estimation Error
figure
s = std(errx, 0, 1);
m = mean(errx, 1);
errorbar(sigmas, m, s, 'Color', 'k', 'LineWidth', 1.0)
xlabel('Estimation Error Standard Deviation (m/s^2)')
ylabel('Longitudinal Position Error (m)')

% (16.2) Pose Estimation Error (Y) vs Standard Deviation of Estimation Error
figure
s = std(erry, 0, 1);
m = mean(erry, 1);
errorbar(sigmas, m, s, 'Color', 'k', 'LineWidth', 1.0)
xlabel('Estimation Error Standard Deviation (m/s^2)')
ylabel('Lateral Position Error (m)')

% (16.3) Pose Estimation Error (Z) vs Standard Deviation of Estimation Error
figure
s = std(errz, 0, 1);
m = mean(errz, 1);
errorbar(sigmas, m, s, 'Color', 'k', 'LineWidth', 1.0)
xlabel('Estimation Error Standard Deviation (m/s^2)')
ylabel('Vertical Position Error (m)')

% (16.4) Pose Estimation Error (Roll) vs Standard Deviation of Estimation Error
figure
s = std(errtheta, 0, 1);
m = mean(errtheta, 1);
errorbar(sigmas, m, s, 'Color', 'k', 'LineWidth', 1.0)
xlabel('Estimation Error Standard Deviation (m/s^2)')
ylabel('Roll Angle Error (deg)')

% (16.5) Pose Estimation Error (Pitch) vs Standard Deviation of Estimation Error
figure
s = std(errphi, 0, 1);
m = mean(errphi, 1);
errorbar(sigmas, m, s, 'Color', 'k', 'LineWidth', 1.0)
xlabel('Estimation Error Standard Deviation (m/s^2)')
ylabel('Pitch Angle Error (deg)')

% (16.6) Pose Estimation Error (Yaw) vs Standard Deviation of Estimation Error
figure
errpsi = errpsi - 1;
s = std(errpsi, 0, 1);
m = mean(errpsi, 1);
errorbar(sigmas, m, s, 'Color', 'k', 'LineWidth', 1.0)
xlabel('Estimation Error Standard Deviation (m/s^2)')
ylabel('Yaw Angle Error (deg)')

% (16.7) Pose Estimation Error (Attitude) vs Standard Deviation of Estimation Error
figure
err_att = err_att + 3;
s = std(err_att, 0, 1);
m = mean(err_att, 1);
errorbar(sigmas, m, s, 'Color', 'k', 'LineWidth', 1.0)
xlabel('Estimation Error Standard Deviation (m/s^2)')
ylabel('Total Angle Error (deg)')
title('Sensitivity of Total Attitude Error to Model Estimation Error')

% (16.8) Pose Estimation Error (Position) vs Standard Deviation of Estimation Error
figure
s = std(err_r, 0, 1);
m = mean(err_r, 1);
errorbar(sigmas, m, s, 'Color', 'k', 'LineWidth', 1.0)
xlabel('Estimation Error Standard Deviation (m/s^2)')
ylabel('Total Position Error (m)')
title('Sensitivity of Total Position Error to Model Estimation Error')

% (16.9) Pose Estimation Error (X/Y/Z) vs Standard Deviation of Estimation Error
figure
sgtitle('Sensitivity of Sensor Position Estimate to Model Estimation Error')
sx = std(errx, 0, 1);
mx = mean(errx, 1);
sy = std(erry, 0, 1);
my = mean(erry, 1);
sz = std(errz, 0, 1);
mz = mean(errz, 1);
subplot(3, 1, 1)
errorbar(sigmas, mx, sx, 'Color', 'k', 'LineWidth', 1.0)
xlabel('Estimation Error Standard Deviation (m/s^2)')
ylabel('Longitudinal Error (m)')
subplot(3, 1, 2)
errorbar(sigmas, my, sy, 'Color', 'k', 'LineWidth', 1.0)
xlabel('Estimation Error Standard Deviation (m/s^2)')
ylabel('Lateral Error (m)')
subplot(3, 1, 3)
errorbar(sigmas, mz, sz, 'Color', 'k', 'LineWidth', 1.0)
xlabel('Estimation Error Standard Deviation (m/s^2)')
ylabel('Vertical Error (m)')

% (16.10) Pose Estimation Error (Theta/Phi/Psi) vs Standard Deviation of Estimation Error
figure
sgtitle('Sensitivity of Sensor Orientation Estimate to Model Estimation Error')
stheta = std(errtheta, 0, 1);
mtheta = mean(errtheta, 1);
sphi = std(errphi, 0, 1);
mphi = mean(errphi, 1);
spsi = std(errpsi, 0, 1);
mpsi = mean(errpsi, 1);
subplot(3, 1, 1)
errorbar(sigmas, mtheta, stheta, 'Color', 'k', 'LineWidth', 1.0)
xlabel('Estimation Error Standard Deviation (m/s^2)')
ylabel('Roll Angle Error (deg)')
subplot(3, 1, 2)
errorbar(sigmas, mphi, sphi, 'Color', 'k', 'LineWidth', 1.0)
xlabel('Estimation Error Standard Deviation (m/s^2)')
ylabel('Pitch Angle Error (deg)')
subplot(3, 1, 3)
errorbar(sigmas, mpsi, spsi, 'Color', 'k', 'LineWidth', 1.0)
xlabel('Estimation Error Standard Deviation (m/s^2)')
ylabel('Yaw Angle Error (deg)')

% (17) 3D Scatter Plot of Cycles in Feature Space
X = U_f8{1}(:, 1);
Y = U_f8{1}(:, 2);
Z = U_f8{1}(:, 3);
Xcycle1 = X(200:257);
Ycycle1 = Y(200:257);
Zcycle1 = Z(200:257);
Xcycle2 = X(258:346);
Ycycle2 = Y(258:346);
Zcycle2 = Z(258:346);
Xcycle3 = X(347:443);
Ycycle3 = Y(347:443);
Zcycle3 = Z(347:443);
Xcycle4 = X(444:560);
Ycycle4 = Y(444:560);
Zcycle4 = Z(444:560);
Xcycle5 = X(561:750);
Ycycle5 = Y(561:750);
Zcycle5 = Z(561:750);
Xcycle6 = X(751:858);
Ycycle6 = Y(751:858);
Zcycle6 = Z(751:858);
Xcycle7 = X(859:960);
Ycycle7 = Y(859:960);
Zcycle7 = Z(859:960);
Xcycle8 = X(961:1058);
Ycycle8 = Y(961:1058);
Zcycle8 = Z(961:1058);
Xcycle9 = X(1059:1177);
Ycycle9 = Y(1059:1177);
Zcycle9 = Z(1059:1177);
Xcycle10 = X(1178:1293);
Ycycle10 = Y(1178:1293);
Zcycle10 = Z(1178:1293);
Xcycle11 = X(1294:1410);
Ycycle11 = Y(1294:1410);
Zcycle11 = Z(1294:1410);
Xcycle12 = X(1411:1522);
Ycycle12 = Y(1411:1522);
Zcycle12 = Z(1411:1522);
Xcycle13 = X(1523:1615);
Ycycle13 = Y(1523:1615);
Zcycle13 = Z(1523:1615);
Xcycle14 = X(1616:1717);
Ycycle14 = Y(1616:1717);
Zcycle14 = Z(1616:1717);
Xcycle15 = X(1718:1835);
Ycycle15 = Y(1718:1835);
Zcycle15 = Z(1718:1835);
Xcycle16 = X(1816:2003);
Ycycle16 = Y(1816:2003);
Zcycle16 = Z(1816:2003);
Xcycle17 = X(2004:2111);
Ycycle17 = Y(2004:2111);
Zcycle17 = Z(2004:2111);
Xcycle18 = X(2112:2243);
Ycycle18 = Y(2112:2243);
Zcycle18 = Z(2112:2243);
numcycles = 18;
cmap = jet(numcycles);

figure
hold on
scatter3(Xcycle1, Ycycle1, Zcycle1, 'MarkerFaceColor', cmap(1, :))
scatter3(Xcycle2, Ycycle2, Zcycle2, 'MarkerFaceColor', cmap(2, :))
scatter3(Xcycle3, Ycycle3, Zcycle3, 'MarkerFaceColor', cmap(3, :))
scatter3(Xcycle4, Ycycle4, Zcycle4, 'MarkerFaceColor', cmap(4, :))
scatter3(Xcycle5, Ycycle5, Zcycle5, 'MarkerFaceColor', cmap(5, :))
scatter3(Xcycle6, Ycycle6, Zcycle6, 'MarkerFaceColor', cmap(6, :))
scatter3(Xcycle7, Ycycle7, Zcycle7, 'MarkerFaceColor', cmap(7, :))
scatter3(Xcycle8, Ycycle8, Zcycle8, 'MarkerFaceColor', cmap(8, :))
scatter3(Xcycle9, Ycycle9, Zcycle9, 'MarkerFaceColor', cmap(9, :))
scatter3(Xcycle10, Ycycle10, Zcycle10, 'MarkerFaceColor', cmap(10, :))
scatter3(Xcycle11, Ycycle11, Zcycle11, 'MarkerFaceColor', cmap(11, :))
scatter3(Xcycle12, Ycycle12, Zcycle12, 'MarkerFaceColor', cmap(12, :))
scatter3(Xcycle13, Ycycle13, Zcycle13, 'MarkerFaceColor', cmap(13, :))
scatter3(Xcycle14, Ycycle14, Zcycle14, 'MarkerFaceColor', cmap(14, :))
scatter3(Xcycle15, Ycycle15, Zcycle15, 'MarkerFaceColor', cmap(15, :))
scatter3(Xcycle16, Ycycle16, Zcycle16, 'MarkerFaceColor', cmap(16, :))
scatter3(Xcycle17, Ycycle17, Zcycle17, 'MarkerFaceColor', cmap(17, :))
scatter3(Xcycle18, Ycycle18, Zcycle18, 'MarkerFaceColor', cmap(18, :))

% (18) Plot of Ay Residual in Feature Space
colormap(jet)
clim([min(abs(residualy_f8)), max(abs(residualy_f8))]);

figure
scatter3(X(t0_test_f8:tf_test_f8), Y(t0_test_f8:tf_test_f8), Z(t0_test_f8:tf_test_f8), [], abs(residualy_f8))
colorbar
title('Residuals in the Feature Space')
xlabel('Delta norm')
ylabel('Vy norm')
zlabel('Vz norm')


figure
hold on
plot(time_f8(t0_test_f8:tf_test_f8), 10*residualy_f8)
plot(time_f8(t0_test_f8:tf_test_f8), Ux_test_f8(:, 4))

figure
hold on
scatter3(Xcycle3, Ycycle3, Zcycle3, 'MarkerFaceColor', cmap(3, :))
scatter3(Xcycle4, Ycycle4, Zcycle4, 'MarkerFaceColor', cmap(4, :))

figure
colormap(jet)
clim([min(abs(residualy_f8)), max(abs(residualy_f8))]);
hold on
scatter3(Xcycle3, Ycycle3, Zcycle3, [], abs(residualy_f8(147:243)))
scatter3(Xcycle4, Ycycle4, Zcycle4, [], abs(residualy_f8(244:360)))
colorbar
title('Residuals in the Feature Space')
xlabel('Delta norm')
ylabel('Vy norm')
zlabel('Vz norm')

figure
V = sqrt(vx_etalin_f8.^2 + vy_etalin_f8.^2 + vz_etalin_f8.^2 + 0.0001);
beta_f8 = atan(vy_etalin_f8./vx_etalin_f8);
vbeta_f8 = gradient(beta_f8)./gradient(time_f8);
x = time_f8(t0_test_f8:tf_test_f8);
y = V(t0_test_f8:tf_test_f8).*vpsi_etalin_f8(t0_test_f8:tf_test_f8)./dcf ;%+ V(t0_test_f8:tf_test_f8).*vbeta_f8(t0_test_f8:tf_test_f8);
y = y ./ max(abs(y));
grady = gradient(y)./gradient(time_f8(t0_test_f8:tf_test_f8));
u = asin(y);
z = -1*sin(2*u);
vpsiratio = vpsi_etalin_f8./delta_f8;
subplot(2, 1, 1)
hold on
plot(time_f8(t0_test_f8:tf_test_f8), y)
plot(time_f8(t0_test_f8:tf_test_f8), residualy_f8)
plot(time_f8(t0_test_f8:tf_test_f8), y.*grady)
subplot(2, 1, 2)
hold on
y2 = sin(x/8);
plot(x, sin(x/8))
plot(x, asin(y2))
plot(x, -1*sin(2*y2))


figure 
L = 1600;
Fs = 10;
sgtitle('Spectral Density of Raw vs Filtered Ground Truth Angular Velocites')
Y1 = fft(vbeta_f8(t0_test_f8:t0_test_f8+L));
P2 = abs(Y1/L);
P1 = P2(1:(L/2+1));
P1(2:end-1) = 2*P1(2:end-1);
f = Fs*(0:(L/2))/L;

Y2 = fft(residualy_f8(1:L));
P4 = abs(Y2/L);
P3 = P4(1:(L/2+1));
P3(2:end-1) = 2*P3(2:end-1);
%f = Fs*(0:(L/2))/L;

subplot(2, 1, 1)
hold on
plot(f(2:end-1), P1(2:end-1), 'Color', 'k', 'LineWidth', 0.75)
hold off
title('Raw Signal (X)')
xlabel('Frequency (Hz)')
ylabel('Power Spectral Density')

subplot(2, 1, 2)
hold on
plot(f(2:end-1), P3(2:end-1), 'Color', 'k', 'LineWidth', 0.75)
hold off
title('Filtered Signal (X)')
xlabel('Frequency (Hz)')
ylabel('Power Spectral Density')

figure 
plot(y, ay_etalin_f8(t0_test_f8:tf_test_f8))

figure
axylin_f8 = sqrt(axlin_f8.^2 + aylin_f8.^2);
hold on
plot(time_f8(t0_test_f8:tf_test_f8), axlin_f8(t0_test_f8:tf_test_f8))
plot(time_f8(t0_test_f8:tf_test_f8), residualy_f8)

figure
axlin = axlin_f8(t0_test_f8:tf_test_f8);
aylin = aylin_f8(t0_test_f8:tf_test_f8);
T = time_f8(t0_test_f8:tf_test_f8);
V = sqrt(vx_etalin_f8.^2 + vy_etalin_f8.^2 + vz_etalin_f8.^2 + 0.0001);
K = ((V - vx_etalin_f8) ./ (vx_etalin_f8 + 0.0001))*100;
V = V(t0_test_f8:tf_test_f8);
K = K(t0_test_f8:tf_test_f8);
D = (1000/dcf)*delta_f8(t0_test_f8:tf_test_f8);
aylin_f8 = gradient(vy_etalin_f8)./gradient(time_f8);
Zy = ay_etalin_f8 - aylin_f8;
Zy = Zy(t0_test_f8:tf_test_f8);
%F = U_f8{1}(t0_test_f8:tf_test_f8, 4);
%Y = U_f8{1}(t0_test_f8:tf_test_f8, 3);
%Y = aylin;
%X = axlin;
%Z = axlin;
%W = ay_etalin_f8(t0_test_f8:tf_test_f8);

figure
scatter3(D, V, K, [], Zy)
colorbar
xlabel('Normalized Steer Angle')
ylabel('Velocity Magnitude (m/s)')
zlabel('Longitudinal Slip (%)')
title('Target Lateral Acceleration in Input Feature Space')



figure
scatter3(D, V, vratio, [], Zy)
xlabel('Delta')
ylabel('Velocity')
zlabel('Longitudinal Slip Percent')

figure
Zx = T_f8{1}(t0_test_f8:tf_test_f8);
scatter3(F, V, vratio, [], Zx)
xlabel('Force')
ylabel('Velocity')
zlabel('Longitudinal Slip Percent')

figure
Zz = T_f8{1}(t0_test_f8:tf_test_f8);
scatter3(D, X, V, [], Zz)
xlabel('Delta')
ylabel('Axlin')
zlabel('Velocity')



figure
scatter3(D(1:end-300), Y(1:end-300), V(1:end-300), [], abs(residualy_f8(1:end-300)) )
xlabel('Delta')
ylabel('Aylin')
zlabel('Velocity')

figure
plot(T, Z)

figure 
plot(vpsi_etalin)
title('etalin yaw rate')

T = time_f8(t0_test_f8:tf_test_f8);
V = sqrt(vx_etalin_f8.^2 + vy_etalin_f8.^2 + vz_etalin_f8.^2 + 0.0001);
K = ((V - vx_etalin_f8) ./ (vx_etalin_f8 + 0.0001))*100;
V = V(t0_test_f8:tf_test_f8);
K = K(t0_test_f8:tf_test_f8);
D = (1000/dcf)*delta_f8(t0_test_f8:tf_test_f8);
aylin_f8 = gradient(vy_etalin_f8)./gradient(time_f8);
Zy = ay_etalin_f8 - aylin_f8;
Zy = Zy(t0_test_f8:tf_test_f8);

figure
scatter3(D, V, K, [], Zy)
colorbar
xlabel('Normalized Steer Angle')
ylabel('Velocity Magnitude (m/s)')
zlabel('Longitudinal Slip (%)')
title('Target Lateral Acceleration in Input Feature Space')

