function R = rotmatrix(x, y, z)

xsensormatrix = [1,  0,  0;   0 , cosd(x), -1*sind(x);   0 , sind(x), cosd(x)]; 
ysensormatrix = [cosd(y), 0, sind(y);   0 , 1, 0;   -1*sind(y), 0, cosd(y)];
zsensormatrix = [cosd(z), -1*sind(z), 0;   sind(z), cosd(z), 0;   0 0 1];

R = xsensormatrix*ysensormatrix*zsensormatrix;


end
