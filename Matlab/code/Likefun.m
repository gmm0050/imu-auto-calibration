function [likesum] = Likefun(x, theta, phi, resultxyz, Ysensorxyz, Htest, Utest, Utrain, cp, T, is_rot)

point = [x(1); x(2); x(3)];
likesum = 0;

if is_rot == true
    psi = x(4);
    Rmatrix = rotmatrix(theta, phi, psi);
else
    Rmatrix = eye(3);
end

for t = 1:T
            
   [~, Uindmc] = findnclosest(Utrain, Utest(t, :), 1);
    
   umc = Utrain(Uindmc, :);
        
   deltaxyz = Ysensorxyz(:, t) - Rmatrix*(resultxyz(:, t) + Htest(:, :, t)*(point - cp));
            
   likesum = likesum - likelihood(deltaxyz, umc); %calculate negative likelihood of the delta vector 
        
end




end