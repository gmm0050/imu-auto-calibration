

function [c, w, b, U, r, t, validation_vector, Rsqi, msei] = SARNNroi(validationmethod, kperval, v, n, Uval, Yval, Utrain, Ytrain)
 
 % validationmethod = method for finding error in validation routine:
 %                      /direct error = 1  /fourier error = 2
 % k = # of training loops conducted before each validation loop
 %
 % v = # of times validation error must increase in a row to trigger the
 %      validation loop to stop
 
%format long
%options = optimset('GradObj','on');

%these commands remove all previous output files 
!/bin/rm -rf *.output
!/bin/rm -rf *.dat


%load the user input data
%load('x2dot_train_matrix.mat', 'x2dot_train_matrix')
%load('x2dot_test_matrix.mat','x2dot_test_matrix')

%%Parameters

nsample = length(Utrain);
validating = length(Yval);
ndim = size(Utrain, 2);
nout = size(Ytrain, 2);
outclass = 1;
xsvec = Utrain;
valsize = -1;
%%
% userparam = load('user_arti_params1.txt');
% 
% %number of training samples 
% nsample = length(xsmat);
% 
% %number of input dimensions
% %ndim=userparam(1,1);
% ndim = size(xsmat, 2) - 1;
% 
% %output dimension number
% nout=userparam(1,2);
% 
% %allows the user to set a tolerance (max allowable diff)
% tol=userparam(1,3); %change this back eventually
% 
% 											
% %this chooses what basis function to use (1 or 2)
% base=userparam(1,4);	                                		
% 
% %sensitivity plot of one dim (1) or all dim (2)
% whatplot=userparam(1,5);		    				
% 
% %plot max diff convergence (1) or approx error(2) 
% whatplot2=userparam(1,6);	
% 
% if whatplot==1
%     sensdim=userparam(1,7);	%input component for sensitivity plot
% elseif whatplot==2
%     %sensdim = 0; 
%     sensdim = 1;
% end
% 
% %upper bound for the optimization routine
% minimerlimit=userparam(1,8);
% 
% %regression problem (1) or classification (2)
% outclass=userparam(1,9);
% 
% %test the SVM with a test file (1) or not (2)
% test=userparam(1,10);

%-------------------------------------------------------------------------

ntraining = nsample;
 %makes the last column the output
resid0 = Ytrain;
resid1 = resid0;
%residvalid = xsmat(ntraining+1:nsample, end);%validation set's output (surface data)

%-------------------------------------------------------------------------

% if test==1
% 
%     %xtestmat = x2dot_test_matrix;
%     disp('*** made it here ****')
%     testing=length(xtestmat); %number of testing samples
%  	SVT = xtestmat(1:testing,1:ndim); 
%     stufftest=xtestmat(1:testing,end);
%     range = max(stufftest) - min(stufftest);
%     %stufftest_not_scaled = mapminmax('reverse',stufftest,Ts);
% elseif test == 2
%     testing=length(xsmat);
%     SVT = xsvec; 
%     stufftest = resid0;
%     %stufftest_notscaled=mapminmax('reverse',stufftest,Ts);
% end

%theout=userparam(1,11); %do you want output files made (1) or not (2)
theout = 2;
%-------------------------------------------------------------------------
disp('** Training **') 

if theout==1
    disp('*** made it to theout ****')
    fid1 = fopen('kstar.output','w');
%    fid2 = fopen('xcenterd.output','w');
    fid3 = fopen('daw.output','w');
elseif theout == 2
end

% initialize the variable vector (dubya bounds)
minw = 1.0*10^-64; %Equal to 0.001, approximately zero
maxw = 1.0-(1.0*10^-6); %Equal to 0.999999, approximately one
%maxw = 1.0-(1.0*10^-6); %Equal to 0.999999, approximately one


%-------------------------------------------------------------------------
disp('*** starting calculations ****') % the xcenters and dubyas
noisy = (rand(ntraining,1)/100.0);

%-------------------------------------------------------------------------
%first, determine k by finding the optimal ratio of training samples to validation samples
validation_vector = [];%validation error at each validation loop
validationflag = 0; %stopping criterion reached: /Yes == 1  /No == 0
loop_counter = 0; % # of validation loops completed
index = 0;%initializing index
bases = [];
val_prediction = zeros(1, validating);



%while index < 1000
while validationflag == 0 && index <= (ntraining/10) % (ntraining - 2*k)
for i = 1:kperval 
    index = kperval*loop_counter + i; 
    %This is the training loop within the driver function
    %"closed_loop_SFAX_2". 
    %thedubya(index) determines the weight of the next basis using
    %the function fminbnd2, which itself uses eetester
    %
    %bee(index) determines the bias  of the next basis using the
    %function delfa
   % if rem(index,2)==0 %Adds opposite noise to sample with even/odd indices
   %     resid0 = resid0 + noisy;
   % else
   %     resid0 = resid0 - noisy;
    %end
    
    [~,thecenter(index)]=  max(abs(resid0));
    
%-------------------------------------------------------------------------
% find the N closest training points: use these to find w, c, and b
    
    [r(index), Uinds] = findnclosest(Utrain, Utrain( thecenter(index), : ), n);
%-------------------------------------------------------------------------
    
    residn = resid0(Uinds);
    Rsq(index) = dot(resid0,resid0);
    Rsqn(index) = dot(residn,residn);%Rsq is a vector that contains the initial mse's of each training sample
    totalresidn = sum(residn);

%-------------------------------------------------------------------------
% find value of w (thedubya) that minimizes eetester
    %options = optimset('GradObj','on','TolFun',1e-8,'TolX',1,'MaxFunEvals',100000,'MaxIter',100000);
    %thecenter(index) = fminbnd(@eetesta,1,nsample,options,wpara,Rsq(index),totalresid,resid0,nsample,xsvec);
    %thecenter = floor(thecenter)

    options = optimset('TolFun',1.0e-64,'TolX',1.0e-64,'MaxFunEvals',1e64,'MaxIter',1e64);
    thedubya(index) = fminbnd2(@eetester,minw,maxw,options,Rsqn(index),totalresidn,residn,n,Utrain,Uinds,ndim,thecenter(index));

%-------------------------------------------------------------------------    

    bee(index) = delfa(totalresidn, residn,n,Utrain,Uinds,thecenter(index),...
                                                    thedubya(index),ndim);

    A = Rnumer(residn,n,Utrain,Uinds,thecenter(index),thedubya(index),ndim);
    B = totalphi(n,Utrain,Uinds,thecenter(index),thedubya(index),ndim);
    C = sqphi(n,Utrain,Uinds,thecenter(index),thedubya(index),ndim);
    coeff(index) = (A - B*bee(index))/C;
    for kk = 1:n %updates resid0 to resid1
        resid1(Uinds(kk)) = resid0(Uinds(kk))-bee(index)-coeff(index)*phi(Utrain,Uinds(kk),thecenter(index),thedubya(index),ndim);
    end
    resid0 = resid1;
 
    if theout==1
        fprintf(fid1,'%5.0f\n',thecenter(index));
    %    fprintf(fid2,'%12.8f\n',xcenter(i,:));
    elseif theout == 2
    end


%-------------------------------------------------------------------------

    goop = max(abs(resid0));
  

end%end of training loop 
%% Validation Loop
    loop_counter = loop_counter + 1;
    bases = [bases, loop_counter*kperval];
    disp('Bases Generated:')
    disp(loop_counter*kperval)
    training_counter = (loop_counter)*kperval;%#of training samples used in total
                                      % training_counter = index
    %all of the ANN's determinant properties assembled for use in
    %validate()
    w_val = thedubya(training_counter - kperval + 1:training_counter);
    b_val = bee(training_counter - kperval + 1:training_counter);
    c_val = coeff(training_counter - kperval + 1:training_counter);
    r_val = r(training_counter - kperval + 1: training_counter);
    Utrain_val = Utrain(thecenter(training_counter - kperval + 1:training_counter), :);

    disp('c = ')
    disp(coeff(end))
    
    disp('A/B/C = ')
    disp([A B C])
    
    disp('max of residual')
    disp(goop)
    
    disp('w = ')
    disp(thedubya(end))
    
    valsize = size(val_prediction);
    
    [validation_vector, validationflag, val_prediction] = validate(val_prediction, w_val, c_val, b_val, r_val, Utrain_val, Uval, Yval, nout, ndim, outclass, ... 
                                                       validation_vector, validationmethod, v, bases);
     t = training_counter;
     w = thedubya(1:training_counter);
     b = bee(1:training_counter);
     c = coeff(1:training_counter);
     %center = thecenter(1:training_counter); 
     U = Utrain(thecenter, :);
    
    disp('Training Error:')
    disp((resid0.'*resid0)/ntraining)
    disp('Validation Error:')
    disp(validation_vector(end))
    if validation_vector(end) == min(validation_vector)
        t_opt = training_counter;
        w_opt = w;
        b_opt = b;
        c_opt = c;
        U_opt = U;
    else
    end
                                                  % ndim, nsample, base, whatplot, outclass, theout, ...
                                                   %validation_sample, SVT, test, testing, centers, ...
                                                  % validation_vector, validationmethod, v);
end

numbases = bases(end);
%k = # of training loops per validation loop.
%*********************************************************************
%goop
%thecenter
%thedubya
%bee 
ell = t_opt; %finds the number of bases that were used for convergence 
%nvectors = training_counter;    
%littlec = coeff(1:nvectors);
%daw = thedubya(1:nvectors);
%littlebud = sum(bee(1:nvectors));
Rsqi = Rsq(t_opt);
msei = Rsqi/ntraining;
%training_mse = (resid0.'*resid0)/ntraining;
%-------------------------------------------------------------------------



if theout==1
    fclose(fid1);
%    fclose(fid2);

    disp('** Writing Output Files **')

    %prints our dubyas to a file daw.output
    fprintf(fid3,'%12.8f\n',daw);   
    fclose(fid3);

    %prints our coeffs to a file coeffs.output
    fid4 = fopen('coeffs.output','w');
    fprintf(fid4,'%12.8f\n',littlec);
    fclose(fid4);

    %prints our residual squared to a file residsq.output
    fid5 = fopen('residsq.output','w');
    fprintf(fid5,'%12.8f\n',Rsq);
    fclose(fid5);

    elseif theout == 2
end

avgresid=sum(resid0)/ntraining;


disp('******************************')
fprintf('         target number = %5.0f \n',nout)
fprintf('     number of samples = %5.0f \n',ntraining)
fprintf('       number of bases = %5.0f \n',t_opt)
%fprintf('              little b = %5.4f \n',b_opt)
fprintf('                   Rsq = %5.4f \n',Rsqi)
fprintf('                   mse = %5.4f \n',msei)
fprintf('        max diff found = %5.4f \n',goop)
fprintf('          residual avg = %5.4f \n',avgresid)
% fprintf('      last coeff value = %5.4f \n',c_opt(t_opt))
disp('******************************')
%-------------------------------------------------------------------------

%bignorm =1.;


% disp(' ')
% disp('** Testing The Network **')
% disp('******************************')
% fprintf('          number of samples = %5.0f \n',testing)
% fprintf('          mse of prediction = %5.0f \n',msepredic)
% fprintf(' max abs diff of prediction = %5.0f \n',maxDiff)
% disp('******************************')


%disp('** Got to Point 3 **')

% x=1:ntraining;
% sv=1:ell;
% x1=1:testing;

if outclass == 2
    kcount = 0;
    for j=1:testing
		t=abs(abs(DIFF(j,:)) - 1.0*10^-64);
			if t>1.0*10^-64
                kcount = kcount + 1;
            end
    end  
    pooh = testing - ntraining;

    disp('   ')
    fprintf('             number of misclassifications = %5.0f \n',kcount)
    fprintf('              # of remaining test samples = %5.0f \n',pooh)





    if pooh==0
        percpooh=0;
    else
        percpooh = 100.0*kcount/pooh;
    end
    fprintf('percentage of those samples misclassified = %5.0f \n',percpooh)
    disp('   ')

elseif outclass == 1
    
% something

end
 end

%-------------------------------------------------------------------------
% Returns value for eta
function etavalue = eta(xsvec,j,m,ndim)
m = round(m);
etavalue = 0;
for d=1:ndim
    etavalue = (xsvec(j,d)-xsvec(m,d))*(xsvec(j,d)-xsvec(m,d)) + etavalue;
end
%etavalue = (xsvec(j,1) - xsvec(m,1))*(xsvec(j,1) - ...
%        xsvec(m,1))+ (xsvec(j,2) - xsvec(m,2))*...
%        (xsvec(j,2) - xsvec(m,2));
end
%-------------------------------------------------------------------------
% Returns value for phi, also calls the function etavalue
function phivalue = phi(xsvec,j,m,w,ndim)

phivalue = abs(w)^eta(xsvec,j,m,ndim);
end
%-------------------------------------------------------------------------
% Returns value for totalphi (sum of all phivalue from 1 to nsample)
function totalphivalue = totalphi(nsample,xsvec,Uinds,xi,w,ndim)

for j = 1:nsample
    phi_val(j) = phi(xsvec,Uinds(j),xi,w,ndim);
   
end
totalphivalue = sum(phi_val);
end
%-------------------------------------------------------------------------
% Returns value for sqphi (sum of phi squared)
function sqphivalue = sqphi(nsample,xsvec,Uinds,xi,w,ndim)

for j = 1:nsample
   phi_vals(j) = phi(xsvec,Uinds(j),xi,w,ndim)*phi(xsvec,Uinds(j),xi,w,ndim);
end
sqphivalue = sum(phi_vals);
end
%-------------------------------------------------------------------------
% Returns value for Rnumer (multiplies phi and resid)
function Rnumervalue = Rnumer(resid0,nsample,xsvec,Uinds,xi,w,ndim)
for j = 1:nsample
   phi_res(j) = phi(xsvec,Uinds(j),xi,w,ndim)*resid0(j);
end
Rnumervalue = sum(phi_res);
end
%-------------------------------------------------------------------------
% Returns value for eetesta (optimization of thecenter)
function eetestervalue = eetester(w,~,totalresid,resid0,nsample,xsvec,Uinds,ndim,xi)

A = Rnumer(resid0,nsample,xsvec,Uinds,xi,w,ndim);
B = totalphi(nsample,xsvec,Uinds,xi,w,ndim);
%C = sqphi(nsample,xsvec,xi,w,ndim);
eetestervalue = -(A - B*totalresid/nsample)^2;
%-------------------------------------------------------------------------
% Returns value for eetester (optimization of thedubya)
%function eetestervalue = eetester(wxi,Rsq,totalresid,resid0,nsample,xsvec)
%function eetestervalue = eetester(wxi,Rsq,totalresid,resid0,nsample,xsvec)
%w = wxi(1);
%xi = wxi(2);

%A = Rnumer(resid0,nsample,xsvec,xi,w);
%B = totalphi(nsample,xsvec,xi,w);
%C = sqphi(nsample,xsvec,xi,w);
%eetestervalue = (Rsq - (totalresid^2)/nsample) - ...
%                ((A - B*totalresid/nsample)^2/(C - B^2/nsample));
end
%-------------------------------------------------------------------------
% Returns value for delfa
function delfavalue = delfa(totalresid, resid0,nsample,xsvec,Uinds,xi,w,ndim)

A = Rnumer(resid0,nsample,xsvec,Uinds,xi,w,ndim);
B = totalphi(nsample,xsvec,Uinds,xi,w,ndim);
C = sqphi(nsample,xsvec,Uinds,xi,w,ndim);
delfavalue = (totalresid - (B*A)/C)/(nsample - (B)^2/C);  
end
%-------------------------------------------------------------------------
function BigKvalue = BigK(xsvec,thecenter,thedubya,nvectors,jug,wine,~)
for kk=1:nvectors
    BigKvalue(kk) = thedubya(kk)^((jug-xsvec(thecenter(kk),1))^2 + ...
                        (wine - xsvec(thecenter(kk),2))^2);
end
end
%-------------------------------------------------------------------------
function approxvalue = approx(littlec,littlebud,xsvec,thecenter,...
                                        thedubya,nvectors,jug,wine,~)
A = BigK(xsvec,thecenter,thedubya,nvectors,jug,wine);
approxvalue = dot(littlec,A)+littlebud;
end
%-------------------------------------------------------------------------
function classvalue = class(littlec,littlebud,xsvec,thecenter,...
                                        thedubya,nvectors,jug,wine,ndim)
classvalue = round(approx(littlec,littlebud,xsvec,thecenter,...
                                        thedubya,nvectors,jug,wine,ndim));   
end
%------------------------------------------------------------------------
function [xf,fval,exitflag,output] = fminbnd2(funfcn,ax,bx,options,varargin)
%FMINBND Scalar bounded nonlinear function minimization.
%   X = FMINBND(FUN,x1,x2) attempts to find  a local minimizer X of the function 
%   FUN in the interval x1 <= X <= x2. FUN accepts scalar input X and returns a 
%   scalar function value F evaluated at X.
%
%   X = FMINBND(FUN,x1,x2,OPTIONS) minimizes with the default optimization
%   parameters replaced by values in the structure OPTIONS, created with
%   the OPTIMSET function. See OPTIMSET for details. FMINBND uses these
%   options: Display, TolX, MaxFunEval, MaxIter, FunValCheck, and OutputFcn.
%
%   [X,FVAL] = FMINBND(...) also returns the value of the objective function,
%   FVAL, computed in FUN, at X.
%
%   [X,FVAL,EXITFLAG] = FMINBND(...) also returns an EXITFLAG that describes 
%   the exit condition of FMINBND. Possible values of EXITFLAG and the 
%   corresponding exit conditions are
%
%    1  FMINBND converged with a solution X based on OPTIONS.TolX.
%    0  Maximum number of function evaluations or iterations reached.
%   -1  Algorithm terminated by the output function.
%   -2  Bounds are inconsistent (that is, ax > bx).
%
%   [X,FVAL,EXITFLAG,OUTPUT] = FMINBND(...) also returns a structure
%   OUTPUT with the number of iterations taken in OUTPUT.iterations, the
%   number of function evaluations in OUTPUT.funcCount, the algorithm name 
%   in OUTPUT.algorithm, and the exit message in OUTPUT.message.
%
%   Examples
%     FUN can be specified using @:
%        X = fminbnd(@cos,3,4)
%      computes pi to a few decimal places and gives a message upon termination.
%        [X,FVAL,EXITFLAG] = fminbnd(@cos,3,4,optimset('TolX',1e-12,'Display','off'))
%      computes pi to about 12 decimal places, suppresses output, returns the
%      function value at x, and returns an EXITFLAG of 1.
%
%     FUN can also be an anonymous function:
%        x = fminbnd(@(x) sin(x)+3,2,5)
%
%   If FUN is parameterized, you can use anonymous functions to capture the 
%   problem-dependent parameters. Suppose you want to minimize the objective
%   given in the function MYFUN, which is parameterized by its second argument A.
%   Here MYFUN is an M-file function such as
%
%     function f = myfun(x,a)
%     f = (x - a)^2;
%
%   To optimize for a specific value of A, first assign the value to A. Then 
%   create a one-argument anonymous function that captures that value of A 
%   and calls MYFUN with two arguments. Finally, pass this anonymous function 
%   to FMINBND:
%
%     a = 1.5; % define parameter first
%     x = fminbnd(@(x) myfun(x,a),0,1)
% 
%   See also OPTIMSET, FMINSEARCH, FZERO, FUNCTION_HANDLE.

%   Reference: "Computer Methods for Mathematical Computations",
%   Forsythe, Malcolm, and Moler, Prentice-Hall, 1976.

%   Original coding by Duane Hanselman, University of Maine.
%   Copyright 1984-2004 The MathWorks, Inc.
%   $Revision: 1.18.4.6 $  $Date: 2004/04/16 22:05:24 $


defaultopt = struct('Display','notify',...
    'MaxFunEvals',500,'MaxIter',500,'TolX',1e-4,'FunValCheck','off','OutputFcn',[]);

% If just 'defaults' passed in, return the default options in X
if nargin==1 && nargout <= 1 && isequal(funfcn,'defaults')
    xf = defaultopt;
    return
end

if nargin < 3
    error('MATLAB:fminbnd:NotEnoughInputs',...
        'fminbnd requires three input arguments.')
end

% initialization
if nargin<4
    options = [];
end

% Check for non-double inputs
if ~isa(ax,'double') || ~isa(bx,'double')
  error('MATLAB:fminbnd:NonDoubleInput', ...
        'FMINBND only accepts inputs of data type double.')
end

printtype = optimget(options,'Display',defaultopt,'fast');
tol = optimget(options,'TolX',defaultopt,'fast');
maxfun = optimget(options,'MaxFunEvals',defaultopt,'fast');
maxiter = optimget(options,'MaxIter',defaultopt,'fast');
funValCheck = strcmp(optimget(options,'FunValCheck',defaultopt,'fast'),'on');
funccount = 0;
iter = 0;
xf = []; fx = [];

switch printtype
    case 'notify'
        print = 1;
    case {'none','off'}
        print = 0;
    case 'iter'
        print = 3;
    case 'final'
        print = 2;
    otherwise
        print = 1;
end

% Handle the output
outputfcn = optimget(options,'OutputFcn',defaultopt,'fast');
if isempty(outputfcn)
    haveoutputfcn = false;
else
    haveoutputfcn = true;
    % Convert to function handle as needed.
    outputfcn = fcnchk(outputfcn,length(varargin));
end

% checkbounds
if ax > bx
    exitflag = -2;
    xf=[]; fval = [];
    msg=sprintf(['Exiting due to infeasibility: the lower bound exceeds the' ...
        ' upper bound.']);
    if print > 0
        disp(' ')
        disp(msg)
    end
    output.iterations = 0;
    output.funcCount = 0;
    output.algorithm = 'golden section search, parabolic interpolation';
    output.message = msg;
    % Have not initialized OutputFcn; do not need to call it before returning
    return
end

% Assume we'll converge
exitflag = 1;

header = ' Func-count     x          f(x)         Procedure';
procedure='       initial';

% Convert to function handle as needed.
funfcn = fcnchk(funfcn,length(varargin));

if funValCheck
    % Add a wrapper function, CHECKFUN, to check for NaN/complex values without
    % having to change the calls that look like this:
    % f = funfcn(x,varargin{:});
    % x is the first argument to CHECKFUN, then the user's function,
    % then the elements of varargin. To accomplish this we need to add the 
    % user's function to the beginning of varargin, and change funfcn to be
    % CHECKFUN.
    varargin = {funfcn, varargin{:}};
    funfcn = @checkfun;
end

% Initialize the output function.
if haveoutputfcn
    [xOutputfcn, optimValues, stop] = callOutputFcn(outputfcn,xf,'init',funccount,iter, ...
        fx,procedure,varargin{:});
    if stop
        [xf,fval,exitflag,output] = cleanUpInterrupt(xOutputfcn,optimValues);
        if  print > 0
            disp(output.message)
        end
        return;
    end
end

% evaluate endpoints
fa = funfcn(ax,varargin{:});
fb = funfcn(bx,varargin{:});
funccount = funccount + 2;
% Compute the start point
seps = sqrt(eps);
c = 0.5*(3.0 - sqrt(5.0));%%Why is this the formula for c? Is it arbitrary?
a = ax; b = bx;
v = a + c*(b-a);
w = v; xf = v;
d = 0.0; e = 0.0;
x= xf; fx = funfcn(x,varargin{:});
funccount = funccount + 1;
if print > 2
    disp(' ')
    disp(header)
    fprintf('%5.0f   %12.6g %12.6g %s\n',funccount,xf,fx,procedure)
end

% OutputFcn call
xOutputfcn = xf; % Last x passed to outputfcn; has the input x's shape
if haveoutputfcn
    [xOutputfcn, optimValues, stop] = callOutputFcn(outputfcn,xf,'iter',funccount,iter, ...
        fx,procedure,varargin{:});
    if stop  % Stop per user request.
        [xf,fval,exitflag,output] = cleanUpInterrupt(xOutputfcn,optimValues);
        if  print > 0
            disp(output.message)
        end
        return;
    end
end

fv = fx; fw = fx;
xm = 0.5*(a+b);
tol1 = seps*abs(xf) + tol/3.0;
tol2 = 2.0*tol1;

% Main loop
while ( abs(xf-xm) > (tol2 - 0.5*(b-a)) )
    gs = 1;
    % Is a parabolic fit possible
    if abs(e) > tol1
        % Yes, so fit parabola
        gs = 0;
        r = (xf-w)*(fx-fv);
        q = (xf-v)*(fx-fw);
        p = (xf-v)*q-(xf-w)*r;
        q = 2.0*(q-r);
        if q > 0.0,  p = -p; end
        q = abs(q);
        r = e;  e = d;

        % Is the parabola acceptable
        if ( (abs(p)<abs(0.5*q*r)) && (p>q*(a-xf)) && (p<q*(b-xf)) )

            % Yes, parabolic interpolation step
            d = p/q;
            x = xf+d;
            procedure = '       parabolic';

            % f must not be evaluated too close to ax or bx
            if ((x-a) < tol2) || ((b-x) < tol2)
                si = sign(xm-xf) + ((xm-xf) == 0);
                d = tol1*si;
            end
        else
            % Not acceptable, must do a golden section step
            gs=1;
        end
    end
    if gs
        % A golden-section step is required
        if xf >= xm, e = a-xf;    else e = b-xf;  end
        d = c*e;
        procedure = '       golden';
    end

    % The function must not be evaluated too close to xf
    si = sign(d) + (d == 0);
    x = xf + si * max( abs(d), tol1 );
    fu = funfcn(x,varargin{:});
    funccount = funccount + 1;
    

    iter = iter + 1;
    if print > 2
        fprintf('%5.0f   %12.6g %12.6g %s\n',funccount, x, fu, procedure);
    end
    % OutputFcn call
    if haveoutputfcn
        [xOutputfcn, optimValues, stop] = callOutputFcn(outputfcn,x,'iter',funccount,iter, ...
            fu,procedure,varargin{:});
        if stop  % Stop per user request.
            [xf,fval,exitflag,output] = cleanUpInterrupt(xOutputfcn,optimValues);
            if  print > 0
                disp(output.message);
            end
            return;
        end
    end

    % Update a, b, v, w, x, xm, tol1, tol2
    if fu <= fx
        if x >= xf, a = xf; else b = xf; end
        v = w; fv = fw;
        w = xf; fw = fx;
        xf = x; fx = fu;
    else % fu > fx
        if x < xf, a = x; else b = x; end
        if ( (fu <= fw) || (w == xf) )
            v = w; fv = fw;
            w = x; fw = fu;
        elseif ( (fu <= fv) || (v == xf) || (v == w) )
            v = x; fv = fu;
        end
    end
    xm = 0.5*(a+b);
    tol1 = seps*abs(xf) + tol/3.0; tol2 = 2.0*tol1;

    if funccount >= maxfun || iter >= maxiter
        exitflag = 0;
        output.iterations = iter;
        output.funcCount = funccount;
        output.algorithm = 'golden section search, parabolic interpolation';
        fval = fx;
        msg = terminate(xf,exitflag,fval,funccount,maxfun,iter,maxiter,tol,print);
        output.message = msg;
        % OutputFcn call
        if haveoutputfcn
            callOutputFcn(outputfcn,xf,'done',funccount,iter,fval,procedure,varargin{:});
        end
        return
    end
end % while

% check that endpoints are less than the minimum found
if ( (fa < fx) && (fa <= fb) )
    xf = ax; fx = fa;
elseif fb < fx
    xf = bx; fx = fb;
end
fval = fx;
output.iterations = iter;
output.funcCount = funccount;
output.algorithm = 'golden section search, parabolic interpolation';
msg = terminate(xf,exitflag,fval,funccount,maxfun,iter,maxiter,tol,print);
output.message = msg;
% OutputFcn call
if haveoutputfcn
    callOutputFcn(outputfcn,xf,'done',funccount,iter,fval,procedure,varargin{:});
end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function msg = terminate(~,exitflag,finalf,funccount,maxfun,~,~,tol,print)

switch exitflag
    case 1
        msg = ...
            sprintf(['Optimization terminated:\n' ...
            ' the current x satisfies the termination criteria using OPTIONS.TolX of %e \n'], tol);
        if print > 1 % only print msg if not 'off' or 'notify'
            disp(' ')
            disp(msg)
        end
    case 0
        if funccount >= maxfun
            msg = sprintf(['Exiting: Maximum number of function evaluations has been exceeded\n' ...
                '         - increase MaxFunEvals option.\n' ...
                '         Current function value: %f \n'], finalf);
            if print > 0
                disp(' ')
                disp(msg)
            end
        else
            msg = sprintf(['Exiting: Maximum number of iterations has been exceeded\n' ...
                '         - increase MaxIter option.\n' ...
                '         Current function value: %f \n'], finalf);
            if print > 0
                disp(' ')
                disp(msg)
            end
        end
    otherwise
        error('MATLAB:fminbnd:InvalidExitflag','Invalid exitflag value in TERMINATE.')
end
end
%--------------------------------------------------------------------------
function [xOutputfcn, optimValues, stop] = callOutputFcn(outputfcn,x,state,funccount,iter,  ...
    f,procedure,varargin)
% CALLOUTPUTFCN assigns values to the struct OptimValues and then calls the
% outputfcn.
%
% state - can have the values 'init','iter', or 'done'.
% We do not handle the case 'interrupt' because we do not want to update
% xOutputfcn or optimValues (since the values could be inconsistent) before calling
% the outputfcn; in that case the outputfcn is called directly rather than
% calling it inside callOutputFcn.

% For the 'done' state we do not check the value of 'stop' because the
% optimization is already done.
optimValues.funccount = funccount;
optimValues.iteration = iter;
optimValues.fval = f;
optimValues.procedure = procedure;

xOutputfcn = x;  % Set xOutputfcn to be x
switch state
    case {'iter','init'}
        stop = outputfcn(xOutputfcn,optimValues,state,varargin{:});
    case 'done'
        stop = false;
        outputfcn(xOutputfcn,optimValues,state,varargin{:});
    otherwise
        error('MATLAB:fminbnd:InvalidState','Unknown state in CALLOUTPUTFCN.')
end
end
%--------------------------------------------------------------------------
function [x,FVAL,EXITFLAG,OUTPUT] = cleanUpInterrupt(xOutputfcn,optimValues)
% CLEANUPINTERRUPT updates or sets all the output arguments of FMINBND when the optimization
% is interrupted.

x = xOutputfcn;
FVAL = optimValues.fval;
EXITFLAG = -1;
OUTPUT.iterations = optimValues.iteration;
OUTPUT.funcCount = optimValues.funccount;
OUTPUT.algorithm = 'golden section search, parabolic interpolation';
OUTPUT.message = 'Optimization terminated prematurely by user.';
end
%--------------------------------------------------------------------------
function f = checkfun(x,userfcn,varargin)
% CHECKFUN checks for complex or NaN results from userfcn.

f = userfcn(x,varargin{:});
% Note: we do not check for Inf as FMINBND handles it naturally.
if isnan(f)
    error('MATLAB:fminbnd:checkfun:NaNFval', ...
        'User function ''%s'' returned NaN when evaluated at %g;\n FMINBND cannot continue.', ...
        localChar(userfcn), x);
elseif ~isreal(f)
    error('MATLAB:fminbnd:checkfun:ComplexFval', ...
        'User function ''%s'' returned a complex value when evaluated at %g;\n FMINBND cannot continue.', ...
        localChar(userfcn),x);
end
end

%--------------------------------------------------------------------------
function strfcn = localChar(fcn)
% Convert the fcn to a string for printing

if ischar(fcn)
    strfcn = fcn;
elseif isa(fcn,'inline')
    strfcn = char(fcn);
elseif isa(fcn,'function_handle')
    strfcn = func2str(fcn);
else
    try
        strfcn = char(fcn);
    catch
        strfcn = '(name not printable)';
    end
end
end

%validate(training_counter, sensdim, w, c, b, xsvec, ...
          %                                         ndim, phi, whatplot, outclass, theout, ...
           %                                        validation_sample, SVT, test, testing, centers, input_dimension, Ts, ...
           %                                        validation_vector, validationmethod);

function[validation_vector, validationflag, val_prediction] = validate(prediction, w, c, b, r, U, Valmat, Valoutmat,nout,ndim,outclass,  ...
                                                   validation_vector, method, v, bases)

% (1)Produce a prediction using open loop SFA (the function "values")
%
% (2)Find the error based on the validation method being used
%
% (3)Add the error value to the validation_error vector
%
% (4)Check to see if validation_vector has reached its stopping criterion,
%    and output an exitflag accordingly
%    
%    validationflag == 1   :   stopping criterion reached
%
%    validationflag == 0   :   stopping criterion not reached

%(1): Produces a prediction using values( ... , validation_sample, ...)
    
%values(ell,sensdim,SS,CC,DD,xsvec,ndim,~,...
                %phi,whatplot,outclass,theout,SVT,test,testing,kstar)
partial_prediction = testroi(w, c, b, r, U, Valmat, Valoutmat, outclass, nout, ndim);
val_prediction = prediction + partial_prediction;
                 %t = 0:validating;
validation_sample = Valoutmat;
DIFF=Valoutmat-val_prediction';
absDiff = abs(DIFF);
%maxDiff = max( absDiff );
p = [0, 0];
%(2):

if method == 1
%%  (1) Direct error (MSE) cross-validation     %%

    validation_error = immse(val_prediction', validation_sample);


elseif method == 2
%%  (2) Fourier Error (DTFT) cross-validation  %%    

    prediction_fourier = fft(val_prediction);

    validation_fourier = fft(validation_sample);

    validation_error = immse(prediction_fourier', validation_fourier);
end

%(3): Update the validation error vector, validation_vector

%%  Add Validation Error to the Validation Error Vector. This will be used to determine the early stopping point.  %%

validation_vector = [validation_vector, validation_error];

%%
%(4): Stopping criterion for validation loop. (training loops are inside
%     validation loop)

%%  If Validation Error has increased more than v times in a row, stop closed_loop_SFAX  %%
if length(validation_vector) >= v+1
    
    bases_used = bases((end-v + 1):end);
    
    validation_used = validation_vector((end-v + 1):end);
    
    p = polyfit(bases_used, validation_used, 1);
    
    f = p(1)*bases_used + p(2);
    
    error = v*immse(f, validation_used);
    
    R_squared = 1 - (error/(sum(sqrt(validation_used.^2))));
    if R_squared >= 0.995 && p(1) >= 0.001
 
        validationflag = 1;%stop training
    else
        
        validationflag = 0;%continue training
    end
else 
    
    validationflag = 0;%continue training
end

end