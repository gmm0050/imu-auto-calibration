# IMU-to-Vehicle Auto-Calibration

## Overview

1. [Introduction](Introduction)
2. [How to Get Started](How-to-Get-Started)
3. [Training Procedure](Training-Procedure)
4. [Test Procedure](Test-Procedure)
5. [Papers](Papers)
6. [Future Work](Future-Work)

## Introduction

This repo contains code to perform online IMU-to-Vehicle extrinsic calibration. No manual calibration needed!

[comment]: # (Insert Gif of colored inital estimates converging simultaneously to the true sensor pose on a wire-frame sketch of a car)

Extrinsic sensor calibration is an important step in obtaining valid information in the vehicle frame, without which an autonomous vehicle cannot function properly. Traditionally, a manual calibration routine must be performed by a set of trained experts to a high degree of precision before the vehicle can be safely operated. This procedure costs time and money and limits the design of the sensor suite. An online and autonomous calibration eliminates this constraint, saving time, and allowing for dynamic reconfiguration of the sensor suite. 

Traditionally, extrinsic sensor calibration needs to be performed relative to another sensor. However, if none of the sensors have yet been calibrated to the vehicle frame, the information they provide remains useless. Direct IMU-to-Vehicle extrinsic calibration allows a transformation relative to the vehicle frame to be established. Once the IMU has been calibrated to the vehicle frame, *all of the other sensors can be calibrated relative to the IMU*. 

[comment]: # (Insert illustration of the IMU-to-Vehicle calibration )

There are 2 stages in the calibration procedure: First, a gaussian radial basis function neural network (GRBFNN) is used to emulate the output of a virtual IMU in the vehicle frame. Then, a maximum likelihood search algorithm estimates the extrinsic calibration parameters by performing an IMU-to-IMU calibration between the IMU on the body of the vehicle, and the virtual IMU. 

This IMU-to-Vehicle extrinsic calibration method has been tested extensively on a real-world platform (2017 Lincoln MKZ). For a 45 minute calibration routine, we obtained 2-3 cm of error in longitudinal and lateral position, 0.2 degrees of error in pitch and roll, and less than 0.1 degrees of error in yaw. The vertical position could not be accurately estimated, but this was expected because the calibration routine was performed entirely on flat ground. 



## How to Get Started

1. Install Matlab and the following Toolboxes
    * Image Processing Toolbox
    * Signal Processing Toolbox
    * Matlab is available [here](https://www.mathworks.com/products/matlab.html)


2. Download Matlab files to a workspace

3. Verify that you have the necessary sensors available 
    * Ground Truth System (High-grade IMU + optional fusion with GPS from receiver)
    * GPS Receiver
    * Wheel Odometry System
    * Steering Wheel Encoder

4. Install ROS on your vehicle
    * ROS is available [here](https://www.ros.org/blog/getting-started/)

5. Run the training procedure

6. Test it out!

## Training Procedure

1. Mount the ground truth system at a chosen control point on the vehicle in line with the vehicle frame

2. Mount a GPS antenna on the vehicle and connect to a receiver

3. Ensure that all of the sensors are connected to the vehicle's computer

4. Launch all sensor drivers in separate terminals, e.g.

5. Record a ROS bag

6. Drive a series of maneuvers designed to maximize the observability of the vehicle's nonlinear dynamics
    * Figure-8s, Squares, and Double Lane Changes work well
    * Vary the forward velocity and steer profiles to ensure that the dynamics will be learned for any maneuver encountered during normal operation

7. Load ROS bag data into Matlab and Parse

8. Train the IMU emulation model for your vehicle


## Test Procedure

1. Mount the IMU and GPS antenna at arbitrary locations on the body of the vehicle

2. Verify that all sensors are connected to the vehicle's computer

3. Initiate the calibration procedure. Wait in a standstill phase for a short period of time (15-20 seconds)

4. Drive the calibration maneuver. Ensure reasonable observability of the lateral and longitudinal vehicle dynamics.

5. Once the calibration maneuver has been driven, generate the extrinsic calibration parameter estimate for the IMU using the collected data.

## Papers

Initial Paper on online IMU-to-Vehicle extrinsic calibration

- [Autonomous Direct Calibration of an Inertial Measurement Unit](https://www.ion.org/publications/abstract.cfm?articleID=17909)

Improvement of Calibration Method by fusing information from a network of multiple IMUs

- [Fused Multi-IMU Direct Sensor-to-Vehicle Extrinsic Calibration](https://www.ion.org/publications/abstract.cfm?articleID=18819)

A more detailed description of the background and methods used in this work can be found in the thesis "Online Autonomous IMU-to-Vehicle Extrinsic Calibration using Gaussian Radial Basis Function Neural Networks" available through the Auburn University Graduate School's online portal.

- [Thesis](https://etd.auburn.edu/handle/10415/8945)


## Future Work

1. The IMU-to-Vehicle extrinsic calibration method is currently being extended to networks of multiple IMUs. The information from each IMU can be fused togther, creating a set of geometric constraints that improve the accuracy of the combined pose estimate. With a network of 5+ commercial grade IMUs, the combined pose error approaches zero. This has been demonstrated in simulation, but will soon be validated on a real-world platform as well.

2. We are also working on a C++ implementation of the code. This should enable it to be run smoothly in real-time, even for large networks of IMUs.

3. The fused extrinsic calibration method isn't limited to networks of IMUs. Any sensor could hypothetically be used as a geometric constraint on the combined sensor pose. The method could be easily extended to any sensor suite in which each pair of sensors can be extrinsically calibrated relative to one another.

4. The GRBFNN dynamic model currently needs to be trained individually for each platform it will be deployed on. A vehicle agnostic model could be developed that accurately estimates the dynamics for any ground vehicle given parameters such as mass, moment of inertia, etc... This model could be transfer learned from a base model learned for a single vehicle or a small number of exemplar vehicles. 


